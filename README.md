

APISENSE utilities
==================

- [Deployment Module](#pysense-deployer)
  - [read logs]()
  - [war deployment]()
  - [docs deployment]()
  - [start tomcat]()
  - [stop tomcat]()  
- [Scripting Documentation generation Module](#Generate documentations)

## Deployment Module

Description of `pysense-deployer` which is a simple python script enable to deploy war, documentation and APK application on
a remote server.

##### global parameters 
- `-t`    set default tomcat path on remote server 
- `-ssh`  set default ssh address of remote server 
- `-html` set default html path on remote server   

##### Usage

- __Read Logs__ 
  - *exec tail -f on catalina.out file in tomcat directory* 
  - Example : `pysense--deployer --read-logs`

- __War deployment__
  - *deploy war passed in parameter in tomcat of remote server*
  - Example : ` pysense-deployer --deploy-war /path/to/war `

- __Documentation deployment__ 
  - *deploy documention in html folder of remote folder. Please see [documentation](#Generate documentations) section before the deployment*
  - Example : ` pysense-deployer --deploy-docs `

- __Start TOMCAT__
  - *start tomcat on remote server* 
  - Example : ` pysense-deployer --start-tomcat `

- __Stop TOMCAT__
  - *stop tomcat on remote server*
  - Example : ` pysense-deployer --stop-tomcat `



## Scripting Documentation generation Module

Module Java to generate scripting documentation. This module use [javaparser][javaparser] to extract
comments of source code and generate documentation using [jsdoc][jsdoc] ToolKit.

  - **Helper : Generate mobile scripting documentation**
> `generate-mobile-task-jsdoc`

  - **Helper : Generate server scripting documentation**
> `generate-server-task-jsdoc`



[javaparser]: http://code.google.com/p/javaparser
[jsdoc]: http://code.google.com/p/jsdoc-toolkit/
