enyo.depends(
	"$lib/layout",
	"$lib/onyx",	// To theme Onyx using Theme.less, change this line to $lib/onyx/source,
	"$lib/g11n",
	"$lib/canvas",
	"$lib/apisense/web/editor"
);
