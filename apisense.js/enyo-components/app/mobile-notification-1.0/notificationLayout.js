

enyo.kind({

	name : "App",

	kind : enyo.Scroller,

	components : [{ kind : "A.mobile.NotificationManager" }]
})

enyo.kind({

        name: "A.mobile.NotificationManager",

        kind : enyo.Scroller,

        components : [
                { layoutKind: "com.MasonryLayout", name : "thumb" }
        ],

        create : function(object){  this.inherited(arguments); 

        	var notifications = JSON.parse($notification.get());

        	for (var i in notifications){

        		this.addThumb(notifications[i]);
        	}

        },

        addThumb : function(json){ this.$.thumb.createComponent({ kind : "A.mobile.Notification" , notif : json}); }

})

enyo.kind({

	name: "A.mobile.Notification",

	components : [

		{  kind : "FittableColumns",components : [

			{name : "icon", kind:"Image", src:"assets/survey-icon.png", classes:"thumb-icon", components: []},
			
			{  kind : "FittableRows",components : [
				{ name : "title", classes : "thumb-title"},
				{ name : "subtitle", classes : "thumb-subtitle", content : "From Toto"}
			]}

		]},


		{ name : "description" , classes : "thumb-description" },

		{  kind : "FittableRows", style:"position:absolute;bottom:0;", components : [
			{ name : "timein", classes  : " thumb-subtitle thumb-timein"},
			{ name : "timeout", classes : "thumb-subtitle thumb-timeout"}
		]}
	],

	handlers: {
    	ontap: "anythingTap"
	},

	anythingTap: function(inSender, inEvent) { $notification.show(this.notificationId) },

	create : function(object){ this.inherited(arguments);

		var notification = object.notif;

		this.notificationId = notification.id;

		if (notification.isRead > 0){
			this.addClass("thumb-read")
		}

		if (notification.type != "Survey"){
			this.$.icon.setSrc("assets/view-icon.png")
		}



		this.$.title.setContent(notification.title)
		this.$.subtitle.setContent("From "+notification.provider)
		this.$.description.setContent(notification.subtitle)

		//this.$.timein.setContent("Published : "+new Date().toISOString());
		this.$.timeout.setContent("Expire : "+new Date().toISOString());
		
	}
});

