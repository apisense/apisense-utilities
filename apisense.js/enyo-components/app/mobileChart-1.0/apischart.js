
enyo.kind({
	name : "apisense.FeedzData",
	create : function(object){  this.inherited(arguments);
	

	}
})

var $db = {};
$db.experiment = function(){  return '{ "data" : [[0,50]] }' }


enyo.kind({
	name : "apisense.Feedz",

	components : [

		{ name : "content" }

	],

	create : function(object){  this.inherited(arguments);
	
	
		this.createComponents([{ kind : "apisense.Chart"}]);
	
		

	}

})

enyo.kind({
	name : "apisense.Chart",
	
	components : [ {name : "content"} ],

	create : function(object){  this.inherited(arguments);
	
		
		this.databaseName = object.database;
		this.query = object.query || "";

		var queryResult = $db.experiment(this.databaseName,this.query);
		queryResult = JSON.parse(queryResult)
		if (queryResult.error){
    
  			alert(queryResult.error);
  			return;
  		}


  		
  		new Highcharts.Chart({
         chart: {
             renderTo: 'lol',
             type: 'bar'
         },
         title: {
             text: 'Fruit Consumption'
         },
         xAxis: {
             categories: ['Apples', 'Bananas', 'Oranges']
         },
         yAxis: {
             title: {
                 text: 'Fruit eaten'
             }
         },
         series: [{
             name: 'Jane',
             data: [1, 0, 4]
         }, {
             name: 'John',
             data: [5, 7, 3]
         }]
     });
  	 this.$.content.render();
  		
	}

})


