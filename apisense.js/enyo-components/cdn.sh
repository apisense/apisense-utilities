#!/bin/bash

if [ $# -ne 1 ]
then
  echo "Usage: Please enter app name"
  exit 
fi

APP=$1
echo $APP

if [ ! -d "archives" ]; then
  # Control will enter here if $DIRECTORY exists.
  mkdir archives
fi

ls | grep ar


zip -r ./archives/$APP.zip ./apisense.js/$APP



echo "deploy archive to apisense server"
scp ./archives/$APP.zip metronet1:/tmp


ssh metronet1 -t " sudo unzip /tmp/$APP.zip -d  /var/www/static.apisense.tld/g/js"

ssh metronet1 -t "rm /tmp/$APP.zip"