enyo.kind({

	name : "apisense.core.Hive",

	kind : "apisense.core.NodeService",
	

	create : function(object){   
		
		this.nodeUrl = object.nodeUrl || "http://metronet-vm2.inrialpes.fr/hive";
		this.sConnect = "/user/get/token";
		this.sDisconnect = "/user/update/disconnect";
		this.tokenCookieName = "apisense.hiveToken";
		this.inherited(arguments);

		this.crowd = new apisense.core.HiveModuleCrowd({ nodeService : this });
		this.adminQuery = new apisense.core.HiveModuleAdminQuery({ nodeService : this });

	}
})