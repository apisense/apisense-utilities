enyo.kind({

	name : "apisense.core.HiveModuleAdminQuery",

	create : function(object){   

		// REST service definition
		this.queryService = "/admin/query/mprocess";

		// HIVE Service Object
		this.hive = object.nodeService;
	},

	query : function(query){ 

		var options = {}
		options.callback = new apisense.core.BasicCallback();
		options.handleAs = "text";

		return this.hive.call(this.queryService,{ query : Base64.encode(query), params : [] },options); }
})