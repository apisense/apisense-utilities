enyo.kind({

	name : "apisense.core.HiveModuleCrowd",

	create : function(object){   

		this.sPushExperimentUsers = "/crowd/push/experiment";
		this.sPushBroadcast = "/crowd/push/broadcast";
		this.sPushUser = "/crowd/push/user";

		this.hive = object.nodeService;

	},

	/**
	 *
	 *
	 */
	experiment : function(experiment, message){


		var option = message.option || {};
		option.collapseKey = option.collapseKey || "PushToExperimentUser";


		var gcmMessage = {};
		gcmMessage.name = experiment;
		gcmMessage.message = Base64.encode(JSON.stringify(message));
		gcmMessage.configuration = JSON.stringify(option);

		return this.hive.call(this.sPushExperimentUsers,gcmMessage);
	},

	experimentEvent : function(experiment, event){

		return this.experiment(experiment,{

			"$event" : event
		});
	},

	experimentToast : function(experiment,message){

		return this.experiment(experiment,{

			"$toast" : message
		});
	},

	experimentNotification : function(experiment,message){

		return this.experiment(experiment,{

			"$notification" : message
		});
	},

	experimentStart : function(experiment){

		return this.experiment(experiment,{

			"$command" : "start"
		});
	},

	experimentStop : function(experiment){

		return this.experiment(experiment,{

			"$command" : "stop"
		});
	},

	experimentUpdate : function(experiment){

		return this.experiment(experiment,{

			"$command" : "update"
		});
	},

	experimentUninstall : function(experiment){

		return this.experiment(experiment,{

			"$command" : "uninstall"
		});
	},


	broadcast : function(message){

		var option = message.option || {};
		option.collapseKey = option.collapseKey || "PushToAllUser";


		var gcmMessage = {};
		gcmMessage.message = Base64.encode(JSON.stringify(message));
		gcmMessage.configuration = option;

		return this.hive.call(this.sPushBroadcast,gcmMessage);
	},


	broadcastToast : function(message){

		return this.broadcast(experiment,{

			"$toast" : message
		});

	},

	broadcastNotification : function(message){

		return this.broadcast(experiment,{

			"$notification" : message
		});

	},

	user : function(userId,message){


		var option = message.option || {};
		option.collapseKey = option.collapseKey || "PushToAllUser";


		var gcmMessage = {};
		gcmMessage.userId = userId;
		gcmMessage.message = Base64.encode(JSON.stringify(message));
		gcmMessage.configuration = option;

		return this.hive.call(this.sPushUser,gcmMessage);
	}

})