enyo.kind({
	
	name : "apisense.core.HoneyComb",

	kind : "apisense.core.NodeService",
	
	create : function(object){  
		
		this.nodeUrl = object.nodeUrl || "http://metronet-vm2.inrialpes.fr/adam";
		this.sConnect = "/user-manager/get/token";
		this.sDisconnect = "/user-manager/update/disconnect";
		this.tokenCookieName = "fr.apisense.token";
		this.inherited(arguments); 
	},

	experiment : function(experimentName){ return new honeyComb.ExperimentModule( { honey : this, experimentName : experimentName } ) },

	experimentManager : function(){ return new honeyComb.ExperimentManager({ honey : this }); }
});


enyo.kind({

	name : "honeyComb.ExperimentManager",

	create : function(object){  this.inherited(arguments); 

		this.honey = object.honey;

	},

	processChannel : function(callback){ var self = this;

		var request =  this.honey.call("/experiment-manager/get/process/channel",{},{method : "GET"});
		request.onSuccess = function(data){

			var url = window.location.hostname;

			var u = self.honey.nodeUrl.split("/")
			
			if ((u[u.length-1]) != "..") url = url+"/"+u[u.length-1];

			if (url == "") url = "localhost:8888" 
			data = data.replace("localhost:8888",url);
			callback(data);
		}


	},

	getPendingProcess : function(){ return this.honey.call("/experiment-manager/get/process/status/pending",{},{method : "GET"})},
	getErrorProcess : function(){ return this.honey.call("/experiment-manager/get/process/status/error",{},{method : "GET"})},
	getDoneProcess : function(){ return this.honey.call("/experiment-manager/get/process/status/done",{},{method : "GET"})},

	users : function(experimentName){ return this.honey.call("/experiment-manager/get/regsitered",{name : experimentName}); },

	remoteStart : function(experimentName){ return this.honey.call("/experiment-manager/lifeCycle/remoteStart",{name : experimentName}); },
	remoteStop : function(experimentName){ return this.honey.call("/experiment-manager/lifeCycle/remoteStop",{name : experimentName}); },
	start : function(experimentName){ return this.honey.call("/experiment-manager/lifeCycle/start",{name : experimentName}); },
	stop : function(experimentName){ return this.honey.call("/experiment-manager/lifeCycle/stop",{name : experimentName}); },
	//finish : function(experimentName){ return this.honey.call("/experiment-manager/lifeCycle/finish",{name : experimentName}); },
	finish : function(experimentName){ return this.honey.call("/experiment-manager/delete/xp",{name : experimentName}); },
	experiments : function(){ return this.honey.call("/experiment-manager/get/xps") },

	experiment : function(experimentName){ return this.honey.synchronizeCall("/experiment-manager/info",{name : experimentName}) },

	configuration : function(experimentName){ return this.honey.call("/experiment-manager/get/configuration",{name : experimentName}) },

	defaultConfiguration : function(experimentName){ return this.honey.call("/experiment-manager/get/default/configuration",{name : experimentName}) },

	updateConfiguration : function(experimentName,selectedFeatures){ return this.honey.call("/experiment-manager/update/configuration",{name : experimentName, spl : JSON.stringify(selectedFeatures)}) },

	updateDescription : function(experimentName, niceName, description, copyright){

		return this.honey.call("/experiment-manager/update/xp/description",{
			name 	 	: experimentName, 
			niceName 	: niceName,
			description : Base64.toBase64(description),
			copyright 	: Base64.toBase64(copyright)});
	},

	editPage : function(experimentName){

		this.honey.connector.redirect("/website/"+experimentName);
	},
})

enyo.kind({

	name : "honeyComb.ExperimentModule",

	create : function(object){  this.inherited(arguments); 

		this.honey = object.honey;
		this.experimentName = object.experimentName;
	},

	collector : function(){ return new honeyComb.CollectorModule({ honey : this.honey, experimentName : this.experimentName }) },

	service   : function(){ return new honeyComb.QueryModule({ honey : this.honey, experimentName : this.experimentName }) }
})

enyo.kind({

	name : "honeyComb.QueryModule",

	create : function(object){  this.inherited(arguments); 

		this.honey = object.honey;
		this.experimentName = object.experimentName;

		this.service_process = "/"+this.experimentName+"/query/process/file";

		this.service_mprocess = "/"+this.experimentName+"/query/mprocess/file";
	},

	query : function(queryName,queryParams){ return this.honey.call(this.service_process,{filename : queryName,params : Base64.encode( JSON.stringify(queryParams))})},

	mquery : function(queryName,queryParams){ return this.honey.call(this.service_mprocess,{filename : queryName,params : Base64.encode(JSON.stringify(queryParams))}) }
})

enyo.kind({

	name : "honeyComb.CollectorModule",

	create : function(object){  this.inherited(arguments); 

		this.honey = object.honey;
		this.experimentName = object.experimentName;

		this.service_get_collections = "/"+this.experimentName+"/upload/export/get/collections";

		this.service_export_collections = "/"+this.experimentName+"/upload/export/collections";

		this.service_export_pending = "/"+this.experimentName+"/upload/export/get/process/pending";

		this.service_export_resources = "/"+this.experimentName+"/upload/export/get/resources";

		this.service_remove_resource = "/"+this.experimentName+"/upload/export/delete/resource";

		this.service_get_stats= "/"+this.experimentName+"/upload/get/stats";

		this.service_export_resources = "/"+this.experimentName+"/upload/export/get/resources";

		this.service_export_resource = "/"+this.experimentName+"/upload/export/get/resource";

		this.service_import_resource = this.honey.connector.nodeUrl+"/"+this.experimentName+"/upload/import/collections";
		
		//lazy val URL_CREATE_MAP = xp.service("/upload/data")
  		//lazy val URL_GET_MAPS = xp.service("/delete")
		//lazy val URL_GET_MEDIA = xp.service("/upload/get/media")
  		//lazy val URL_GET_USER_METRIC = xp.service("/upload/metrics/user")
  		//lazy val URL_GET_USER_METRIC_BETWEEN = xp.service("/upload/metrics/user")
  		//lazy val URL_GET_ALL_METRIC = xp.service("/upload/metrics/all")
  		//lazy val URL_GET_ALL_METRIC_BETWEEN = xp.service("/upload/metrics/all")


  		//lazy val URL_EXPORT_GET_RESOURCE = xp.service("/upload/export/get/resource")
  		//lazy val URL_EXPORT_GET_RESOURCES = xp.service("/upload/export/get/resources")
  		//lazy val URL_EXPORT_GET_PENDING = xp.service("/upload/export/get/process")
  		//lazy val URL_EXPORT_COLLECTION = xp.service("/upload/export/collection")
  		//lazy val URL_EXPORT_ALL = xp.service("/upload/export/all")

	},

	/**
	 * Return all data collections name
	 */
	collections : function(){ return this.honey.call(this.service_get_collections,{},{method : "GET"})},


	/**
	 * Return all data collections name
	 */
	stats : function(){ return this.honey.call(this.service_get_stats,{},{method : "GET"})},
	
	/**
	 * Run a new export process
	 *
	 * @param collections JSON Array of collection name to export
	 */
	exportCollections : function(collections,exportName,format){  return this.honey.call(this.service_export_collections,
		{
			name : exportName,
			collections : JSON.stringify(collections),
			format : format
		}
	)},

	/**
	 * Get all export resources name available for download
	 * 
	 */
	getExportResources : function(){  return this.honey.call(this.service_export_resources,{},{method : "GET"}) },


	resource : function(name){ 

		
		window.downloadFile(this.honey.connector.nodeUrl+this.service_export_resource+"/"+name) 


	},

	/**
	 * Delete resource
	 *
	 */
	deleteResource : function(name){  return this.honey.call(this.service_remove_resource,{name : name}) },
	
})

window.downloadFile = function(sUrl) {

	console.log("start downalod "+sUrl);

    //If in Chrome or Safari - download via virtual link click
    if (window.downloadFile.isChrome || window.downloadFile.isSafari) {
        //Creating new link node.
        var link = document.createElement('a');
        link.href = sUrl;

        if (link.download !== undefined){
            //Set HTML5 download attribute. This will prevent file from opening if supported.
            var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
            link.download = fileName;
        }

        //Dispatching click event.
        if (document.createEvent) {
            var e = document.createEvent('MouseEvents');
            e.initEvent('click' ,true ,true);
            link.dispatchEvent(e);
            return true;
        }
    }

    // Force file download (whether supported by server).
    var query = '?download';
    console.log("oalala "+sUrl)
    window.open(sUrl + query, '_self');
}

window.downloadFile.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
window.downloadFile.isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;



