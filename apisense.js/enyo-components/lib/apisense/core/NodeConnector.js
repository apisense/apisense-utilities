enyo.kind({
	
	name : "apisense.core.NodeConnector",
	
	create : function(object){  this.inherited(arguments);
	
		this.nodeUrl = object.nodeUrl || "http://localhost:18000";
		this.sConnect = object.sConnect ;
		this.sDisconnect = object.sDisconnect;
		this.tokenCookieName = object.tokenCookieName || "TOKEN";


		this.TOKEN = Cookies.get(this.tokenCookieName);


		this.requestBuffer = [];

	},

	service : function(url){ return this.nodeUrl + url },


	_createRequest : function(url,_arg,options){

		var options = options || {};

		var callback = options.callback || new apisense.core.JSONCallback();
		var handleAs = options.handleAs || "json";
		var method = options.method || "POST";

		var _arg = _arg || {};

		var headers = options.headers || {};
		
		var token   = options.token || "true"; 

		if (token == "true"){
			
			headers.TOKEN = this.TOKEN;
		}

		
		//header
		var request = new enyo.Ajax({
			url : this.service(url),
			headers : headers,
			method : method,
			datatype:"text/html",
			handleAs : handleAs , //options are "json", "text", or "xml"
			postBody : _arg
		})

		
		request.response(this, function(request,response){ 

			callback.onFinished(); 
			callback.processResponse(request,response)
		});

		request.error(this, function(request,response){
			
			callback.onFinished(); 
			callback.processErrorResponse(request,response)
		});

		return [request,callback];
	},

	synchronizeCall : function(url,_arg,options){ var self = this;

		// generate a simple request id
		var requestId = new Date().getTime();

		options = options || {};
		var callback = options.callback || new apisense.core.JSONCallback();
		callback.onFinished = function(){
			
			
			delete self.requestBuffer[this.requestId];

			var keys = Object.keys(self.requestBuffer);
			if (keys.length != 0){
				
				self.requestBuffer[keys[0]][0].go();
			}

		};
		callback.requestId = requestId;

		options.callback = callback;

		var ajaxRequest = this._createRequest(url,_arg,options);

		self.requestBuffer[requestId] = ajaxRequest;

		if (Object.keys(self.requestBuffer).length == 1){

			self.requestBuffer[requestId][0].go();			
		}

		return callback;
	},

	call : function(url,_arg,options){

		var request = this._createRequest(url,_arg,options);
		
		request[0].go();
		return request[1];
	},

	isConnected : function(){  

		

		return (this.TOKEN != undefined) 
	},

	connect : function(username,password){ var self = this;

		this.username = username;
		this.password = password;
		
		var request = this.synchronizeCall(this.sConnect,{
			username : username,
			password : md5(password)
		},{token : "false"});

		request.on = function(){};
		request.onSuccess = function(token){
			console.log("connected with service "+self.nodeUrl)
			self.TOKEN = token;
			Cookies.set(self.tokenCookieName,self.TOKEN);
			this.on(token);
		};

		return request;
	},

	setToken : function(token){

		self.TOKEN = token;
		Cookies.set(this.tokenCookieName,self.TOKEN);
	},

	disconnect : function(){ var self = this;
		
	
		var request = this.synchronizeCall(this.sDisconnect);
		request.onDisconnected = function(){};
		request.onSuccess = function(req,res){

			this.on(req);
		}

		self.TOKEN = undefined;
		console.log("delete cookie "+self.tokenCookieName)
		Cookies.expire(self.tokenCookieName);

		return request;
	},

	redirect : function(path, params, method) {
    	
    	method = method || "post"; // Set method to post by default, if not specified.

    	// The rest of this code assumes you are not using a library.
    	// It can be made less wordy if you use one.
    	var form = document.createElement("form");
    	form.setAttribute("method", method);
    	form.setAttribute("action",this.service(path));

    	for(var key in params) {
        	var hiddenField = document.createElement("input");
        	hiddenField.setAttribute("type", "hidden");
        	hiddenField.setAttribute("name", key);
        	hiddenField.setAttribute("value", params[key]);

        	form.appendChild(hiddenField);
    	}

    	document.body.appendChild(form);
    	form.submit();
    }


});

enyo.kind({
	name : "apisense.core.BasicCallback",
	processResponse : function(inRequest,inResponse){},
	processErrorResponse : function(inRequest,inResponse){},
	onSuccess : function(){},
	onError : function(){},
	onFinished : function(){}
})



enyo.kind({
	name : "apisense.core.JSONCallback",

	processResponse : function(inRequest,inResponse){

		if ((inResponse.success)||(inResponse.success == "")){
			
			this.onSuccess(inResponse.success)
		}
		else{
			
			this.onError(inResponse.error);
		}
	},

	processErrorResponse : function(inRequest,inResponse){

		this.onError(inResponse,inRequest);
	},

	onFinished : function(){},

	onSuccess : function(){},
	
	onError   : function(){}
}); 

