enyo.kind({
	
	name : "apisense.core.NodeService",
	
	create : function(object){  this.inherited(arguments); var self = this;
	
		this.connector = new apisense.core.NodeConnector({
  			nodeUrl : object.nodeUrl || "http://metronet-vm2.inrialpes.fr/adam",
			sConnect : this.sConnect,
			sDisconnect : this.sDisconnect,
			tokenCookieName : this.tokenCookieName || "Token"
		})


	},

	isConnected : function(){  return this.connector.isConnected(); },

	setToken : function(token){ this.connector.setToken(token); },

	connect : function(username,password,callback){


		if (!this.isConnected()){
			var request = this.connector.connect( username,password );
			request.on = callback;
			request.onError = function(error){
				console.log("Cannot process connection with node ");
				console.log("Cause : "+JSON.stringify(error));
			};

		} else{
		   
		   if (callback) callback(this);
		}
	},

	call : function(url,_arg,options){ 

		var options = options || {};
		options.headers = options.headers || {};
		

		return this.connector.call(url,_arg,options);
	},

	synchronizeCall : function(url,_arg,options){ 

		var options = options || {};
		options.headers = options.headers || {};
		
		
		return this.connector.synchronizeCall(url,_arg,options);
	},

	ready : function(){},
	
	disconnect : function(){  return this.connector.disconnect(); }
});