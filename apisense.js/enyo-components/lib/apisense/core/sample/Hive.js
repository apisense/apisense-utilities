enyo.kind({
	name : "apisense.core.Hive",
	

	create : function(object){  this.inherited(arguments); var self = this;
	
		this.connector = new apisense.core.NodeConnector({
  			nodeUrl : object.url || "http://metronet-vm2.inrialpes.fr/hive",
			sConnect : "/user/get/token",
			sDisconnect : "/user/update/disconnect"
		})

		var request = this.connector.connect( object.username,object.password );
		request.onSuccess = function(token){
			enyo.log("connected with token "+token)
			self.ready(self);
		}

		request.onError = function(error){
			enyo.log("Cannot process connection with node "+object.url);
			enyo.log("Cause : "+error);
		}

	},

	ready : function(){


	},
	
	disconnect : function(){ this.connector.disconnect(); }
})