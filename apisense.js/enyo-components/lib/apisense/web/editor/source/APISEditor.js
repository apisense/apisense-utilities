

enyo.kind({
	name: "APISEditor",
	kind: "FittableRows",
	fit: true,
	classes: "enyo-unselectable",
    style : "background:#F5F5F5;width:100%;position:relative",
	components:[

		{name : "toolbar", kind: "apisense.ToolBar", classes:"toolbar", cssButton : "toolbar-button"},

		{name : "mainPanel", kind : "Panels", arrangerKind: "CollapsingArranger",fit:true, realtimeFit: true,draggable : false, narrowFit: true,  classes:"enyo-border-box",components: [

			{ name : "editorTree", kind:"apisense.editorTree"},


			{ kind : "FittableRows", classes:"apisense-container", components : [

				// Tab Bar
				{ name : "editorTab", kind: "onyx.TabBar",onTabRemoved : "onTabRemoved", onTabChanged:"_updatePaneIndex", classes:"apisense-container-bar"},

				// Tab Content
				{ name : "editorTabPanel", draggable : false, kind : "Panels", classes : "apisense-container-panel"}

			]}
			
		]},

		// bottom part
		{name : "ConsolePane"}		
	],


    
    create : function(){

        var self = this;
        

        this.inherited(arguments);
        this.openFile = {};

        this.$.editorTree.setOnSelectedFile(function(filename){ self.onFileSelected(self,filename) });

        this.$.toolbar.init(this);

        this.consoleToolBar = this.$.ConsolePane.createComponent({
            name : "consoleToolbar", 
            kind : "apisense.ToolBar",
            style:"height:40px;padding:0;"
        })

        this.expandTreeMenu = this.consoleToolBar.menu("","icon icon-chevron-left icon-white",function(){
            
            self._expandTree();
        })

        this.expandConsoleMenu = this.consoleToolBar.menu("","icon icon-chevron-down icon-white",function(){
            
            self._expandConsole();
        })

        this.consoleToolBar.menu("","icon icon-fullscreen icon-white",function(){
            
            self.fullScreen();
        })

        this.consoleToolBar.menu("","icon icon-th-large icon-white",function(){
            
            self.configModal();
        });

        this.configuratorMain = new apisense.editorConfig({editorType:"main"});
        this.configuratorConsole = new apisense.editorConfig({editorType:"console"});

       
        


    },

    /**
     *
     *  Open modal dialog for ace editor configuration
     *  All preferences are set in cookie
     *  
     *  @TODO Complete configuration 
     *
     */
    configModal : function(){ var self = this;

       // main panel editor configurator
       var _main = this.configuratorMain.get();

       var config = new apisense.FormModal({title:"ACE Configurator",style:"width:400px;"});

       config.label("Main Configuration")
       config.list("theme","Theme",this.configuratorMain.themes,_main.theme)
       config.list("fontSize","Font Size",this.configuratorMain.sizes,_main.fontSize)
       config.toggle("showLine","Show Line",_main.showLine)

       config.onChange = function(data){

            self.configuratorMain.set(data.getResult())
            var ace = self.getAceEditor();
            if (ace){
                ace.data.refresh();
            }
       }

       config.renderInto(this);
       config.show();
    },

    constructor : function(object){
        
        var self = this;
        
        this.inherited(arguments);

        this.configuration = object || {};

        this.editorTabPanelCount = 0;

        window.onresize = function(event) {
            
           self.applyStyle("height",$(window).height()-60+"px")
           var ace = self.getAceEditor();
           if (ace){
             ace.data.refresh();
           }

          self.aceConsole.refresh();
        }

        self.applyStyle("height",$(window).height()-60+"px")

    },

    fullScreen : function(){
        
        if (!this.isFullScreen){  this.isFullScreen = false;  }

        if (this.isFullScreen == false){

            var o = this
            $("body").addClass("fullScreen")
            o.applyStyle("position","absolute!IMPORTANT")
            o.applyStyle("height",$(window).height()+"%!IMPORTANT")
            this.isFullScreen = true;

        }
        else{

            var o = this
            o.applyStyle("position","relative!IMPORTANT")
            o.applyStyle("height",$(window).height()-60+"px")
            this.isFullScreen = false;
        } 
    },


	onTabChanged : function(inSender,inEvent){ this.$.editorTabPanel.setIndex(inEvent.index); },

	onTabRemoved : function(inSender,inEvent){

		var deletedCaption = inEvent.caption;
		

		var pane = this.openFile[deletedCaption];
		pane.data.destroy();
		this.$.editorTabPanel.reflow();
		this.render();

		delete this.openFile[deletedCaption];

		this._updatePaneIndex();
	},

    _updatePaneIndex : function(inSender,inEvent){
    	
    	var selected = this.$.editorTab.selectedId;
    	if (this.$.editorTabPanel.getActive()){
    		this.$.editorTabPanel.setIndex(selected);
    		this.$.editorTabPanel.getActive().refresh();

    		if(this.aceConsole){
    			this.aceConsole.refresh();
    		}
    	}
    },

    open : function(filename,content){

    	if (this.openFile[filename]){

    		this.$.editorTab.activate({caption : filename})
			return;
    	}

    	var aceEditor = this.$.editorTabPanel.createComponent({kind:"aceEditor", caption : filename});

		this.$.editorTab.addTab({'caption' : filename});

        this.render();

        this.openFile[filename] = {
        	caption   : filename,
        	data	  : aceEditor,
        	paneIndex : this.$.editorTab.selectedId
        }

        aceEditor.init(this,this.configuratorMain,filename,content);

        this._updatePaneIndex();
    },

    

    initConsole : function(mode){

        var mode = mode || "xml"

        this.$.ConsolePane.createComponent({
        	name : "consoleEditor", 
        	draggable : false,
        	kind : "Panels", 
        	style:"width:100%;height:100%",
        	components : [
        		{kind:"aceEditor", isConsole : true}
        	]
        });

        this.render();
        this.$.ConsolePane.render();

        this.aceConsole = this.$.ConsolePane.$.aceEditor;
        this.aceConsole.init(this,this.configuratorConsole,"."+mode,"");
        this.isConsoleExpanded = false;
       	this.aceConsole.refresh();
        this._expandConsole();

    },

    _expandTree : function(){
    
    	
    	var btn = this.expandTreeMenu.controls[0];
    	if (this.isTreeExpanded){
    		
    		this.$.mainPanel.setIndex(0);
    		this.isTreeExpanded = false;
    		btn.setContent("<i class='icon icon-chevron-left icon-white'></i>");
			
    	}else{

    		this.$.mainPanel.setIndex(1);
    		this.isTreeExpanded = true;
    		btn.setContent("<i class=' icon icon-chevron-right icon-white'></i>");
    	}

    	var ace = this.getAceEditor();
    	if (ace){
    		ace.data.refresh();
    	}

    	this.aceConsole.refresh();
    },

    _expandConsole : function(){

    	var pane = this.$.ConsolePane;
    	var btn = this.expandConsoleMenu.controls[0];
		
    	if (!this.isConsoleExpanded){  
			
			pane.removeClass("apisense-console-up")
            pane.addClass("apisense-console-down")
			this.isConsoleExpanded = true;
			btn.setContent("<i class='icon icon-chevron-up icon-white'></i>");

    		
    	}else{

    		
            pane.removeClass("apisense-console-down")
            pane.addClass("apisense-console-up")
    		this.isConsoleExpanded = false;
    		btn.setContent("<i class='icon icon-chevron-down icon-white'></i>");
    	}

    	this.render();

    	var ace = this.getAceEditor();
    	if (ace){
    		ace.data.refresh();
    	}

    	this.aceConsole.refresh();
    },

    addConsoleValue : function(text){ this.aceConsole.addValue(text); },
    setConsoleValue : function(text){ this.aceConsole.setValue(text); },
    updateTreeModel : function(tree){ this.$.editorTree.update(tree); },

    getAceEditor : function(){ 
    	
    	var panel = this.$.editorTabPanel.getActive();
    	if(panel){
    		var caption = panel.caption;
    		return this.openFile[caption];
    	}
    },

    toolbar : function(){ return this.$.toolbar; },
    tree    : function(){ return this.$.editorTree; },
   
    onSave : function(filename,content){}
});


enyo.kind({
	name : "aceEditor",
	kind : "FittableRows",
	fit : true,
	constructor : function(object, apisEditor){ 
		this.inherited(arguments);
    },

    create : function(object){
    	
    	this.inherited(arguments);
	
		this.isConsole = object.isConsole || false; 
		this.caption = object.caption;
		this.theme = object.theme || "ace/theme/chrome";

    	this.aceId = "ace-"+new Date().getTime();
    	this.createComponent({ name:"editor", tag : "div", style : "position: absolute;top: 0;  right: 0;bottom: 40px;left: 0;font-size:16px;'", id : this.aceId })
    	this.render();

    
    },

	_config : function(){

		var self = this;
		ace.config.loadModule('ace/ext/language_tools', function() {

            
            var _conf = self.configurator.get();

            self.$.editor.applyStyle("font-size",_conf.fontSize);

            self.ed.setOptions({
				enableBasicAutocompletion: _conf.auto,
				enableSnippets: _conf.snippet
			})
		
			self.ed.commands.addCommand({
    			name: 'saveCommand',
    			bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
    			exec: function(editor) {
        			self.save();
    			},
    			readOnly: false 
			});

           

			if (self.isConsole){

				self.ed.setShowPrintMargin(_conf.printMargin);
                self.ed.setShowFoldWidgets(_conf.foldWidgets);
                self.ed.renderer.setShowGutter(_conf.showLine); 
                self.ed.setReadOnly(true);
                self.ed.getSession().setUseSoftTabs(true);
				self.ed.setTheme("ace/theme/ambiance");
			
            }else{

                self.ed.setShowPrintMargin(false);
                self.ed.setShowFoldWidgets(false);
                self.ed.setBehavioursEnabled(false)
                self.ed.renderer.setShowGutter(true); 
                self.ed.setTheme("ace/theme/"+_conf.theme);
            }
		})
	},

    init : function(apisEditor,configurator,filename,text){

    	var self = this;

        ace.config.loadModule("ace/lib/dom", function(dom) { self.dom = dom })
    	
    	var mode = "html";
		var ext = filename.substring((filename.lastIndexOf('.')),filename.lenght)
		if (ext == ".js") mode = "javascript"
		else if (ext == ".xq") mode = "xquery"
        else if (ext == ".txt") mode = "text"
        else if (ext == ".xml") mode = "xml"
		else if (ext == ".py") mode = "python"
		else if (ext == ".css") mode = "css"
		else if (ext == ".html") mode = "html"
        else if (ext == ".json") mode = "json"

    	self.apisEditor = apisEditor;
        self.configurator = configurator;

		self.ed = ace.edit(self.aceId);
		
        self._config();
		
        self.ed.setValue(text);
		self.ed.getSession().setUseWorker(true);
        self.ed.getSession().setMode("ace/mode/"+mode)
    	

    	if (!this.isConsole){
			self.ed.getSession().on('change', function(e) {

				var tab = self.apisEditor.$.editorTab.resolveTab({caption : self.caption})
				var title = tab.getContent();
				if (title.indexOf('*') != (title.length -1))	{
					tab.$.button.setContent(title +" *");
					self.apisEditor.$.editorTab.resetWidth();
				}
			})
		}


        self.ed.resize();
    },
    
    setValue : function(text){  this.ed.setValue(text); },

    addValue : function(text){ 

    	this.ed.insert(text+"\n");
        //this.ed.insert("\n"); 
    	this.ed.scrollPageDown();
    },

    save : function(){

    	this.apisEditor.onSave(this.caption,this.ed.getValue());

    	// remove '*' at the end of the caption bar
    	var tab = this.apisEditor.$.editorTab.resolveTab({caption : this.caption})
		var title = tab.getContent();
		if (title.indexOf('*') != (title.length -1))	{
		   	tab.$.button.setContent(this.caption);
			this.apisEditor.$.editorTab.resetWidth();
		}
    },

    refresh : function(){

    	if (this.ed){
			var session = this.ed.getSession();
    		this.ed = ace.edit(this.aceId);
    		this.ed.setSession(session);
    		this._config();
    	}
    }

})




