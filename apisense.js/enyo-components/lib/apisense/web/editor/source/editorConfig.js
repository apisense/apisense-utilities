enyo.kind({
	name : "apisense.editorConfig",
	create : function(object){  this.inherited(arguments);
	
		this.themes = [
			"chrome","ambiance","chaos","eclipse","cobalt","github",
			"clouds","xcode","twilight","vibrant_ink",
			"tomorrow_night","tomorrow","textmate","terminal","solarized_light","solarized_dark"
		]

		this.sizes = ["10px","11px","12px","13px","14px","15px","16px","17px","18px","19px","20px"]
		this.editorType = this.editorType || "basic"	

		this.update();
	},

	_get : function(name){  return enyo.getCookie("apisense.editor."+this.editorType+"."+name);  },

	_set : function(name,value){  
		
		this.conf[name]	 = value;
		return enyo.setCookie("apisense.editor."+this.editorType+"."+name,value);  
	},


	update : function(){

		var _conf = {}

		_conf.theme    		= this._get("theme") || this.themes[0];
		_conf.fontSize 		= this._get("fontSize") || "16px";
		_conf.auto     		= this._get("auto") || true;
		_conf.snippet  		= this._get("snippet") || true;
		_conf.printMargin	= this._get("printMargin") || true;
		_conf.readOnly		= this._get("readOnly") || false;
		_conf.foldWidgets	= this._get("foldWidgets") || true;
		_conf.showLine		= this._get("showLine") || true;

	

		this.conf = _conf;

	},

	get : function(){ return this.conf;},

	set : function(conf){

		this._set("theme",conf.theme || this.themes[0]);
		this._set("fontSize",conf.fontSize || "16px");
		this._set("showLine",conf.showLine);
	}


})