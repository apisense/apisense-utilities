enyo.kind({
	name: "apisense.ToolBar",
	kind: "onyx.Toolbar",
	classes: "enyo-unselectable",

	constructor : function(object){

        this.inherited(arguments);
    },

    create : function(object){

    	this.inherited(arguments);

    	this.cssButton = object.cssButton || undefined
    },

    init : function(apiseditor){

    	this.apiseditor = apiseditor;

    },


    buttonTapped : function(inSender,inEvent){
    	
    	if (this.buttonHandler[inSender.id]){
    		this.buttonHandler[inSender.id]();
    	}
    },

    menu : function(name,classes,callback){

    	if (!this.buttonHandler) this.buttonHandler = {};

    	//{name:"ConsolePaneBtn", kind: "onyx.Button", style:"background:transparent", ontap:"_expandConsole",classes: "glyphicon glyphicon-chevron-up"},
    	var menu = this.createComponent({
    		kind: "onyx.Button",
    		ontap:"buttonTapped",
    		classes: this.cssButton,
    		components : [ {allowHtml : "true", content : " <div class='toolbar-icon-text'>"+name+"</div> <i class='"+classes+"'></i> " } ]
    		
    	})

    	this.render();

    	this.buttonHandler[menu.id] = callback;
    	
    	return menu;
    },

    separator : function(){

    	var item = this.createComponent({kind: "onyx.Grabber"});
    	this.render();

    	return item;
    },

    newfile : function(callback, _ext ){

    	var ext = _ext || [".js",".html",".css"]
    	var self = this;

    	return this.menu(" New File","icon-file",function(){ 

    		var popup = new apisense.InputModal({
    			name : "filePopup",
    			title : "Create a new File",
    			onValid : function(value){
    			
    				callback(value[1]+"/"+value.input+value[0]);
    			},
    			lists : [ 
    				{name : "File extension", data : ext},
    				{name : "In Folder", data : self.apiseditor.tree().getFolders()}
    			]
    		});
    		popup.renderInto(this);

    		popup.show(); 
    	})
	},

    newfolder : function(callback){

    	var self = this;
    	
    	return this.menu(" New Folder"," icon-folder-close",function(){ 

    		var popup = new apisense.InputModal({
    			name : "folderPopup",
    			title : "Create a new Folder",
    			onValid : function(value){
    			
    				callback(value[0]+"/"+value.input);
    			},
    			lists : [ {name : "In Folder", data : self.apiseditor.tree().getFolders()} ]
    		});
    		
    		popup.renderInto(this);

    		popup.show();

    	})
    },

    deletefile : function(callback){

    	var self = this;
    	

    	return this.menu(" delete","icon-remove",function(){

    		
    		var tree     = self.apiseditor.tree();
    		var rootName = tree.rootName;
    		var selected = tree.selectedFile;

    		if (selected != rootName){
    			var popup = new apisense.BaseModal({
    				name : "deletePopup",
    				title   : "Are you sure ?",
    				onValid : function(data){ callback(data); }
    			})
	    		popup.renderInto(this);
    			popup.setMessage(selected);
    			popup.setData(selected);
				popup.show();
    		}
    	})
    },

    savefile : function(onSave){

    	this.apiseditor.onSave = onSave;

    	var self = this;
    	return this.menu(" save","icon-hdd",function(){

    		var ace = self.apiseditor.getAceEditor()
    		if (ace){
    			ace.data.save();
    		}
    	})
    },

    input : function(label){

    	var id = "input-"+new Date().getTime()

    	var item =  this.createComponents([
    	 	{content : label, style:"color:black" },
    	 	{kind: "onyx.InputDecorator", components: [
				{name : id, kind: "onyx.Input",onchange:"inputChanged"}
			]}
    	])

    	this.render();

    	return this.$[id]
    },

     inputpwd : function(label){

    	var id = "input-"+new Date().getTime()

    	var item =  this.createComponents([
    	 	{content : label, style:"color:black" },
    	 	{kind: "onyx.InputDecorator", components: [
				{name : id, kind: "onyx.Input", type:"password",onchange:"inputChanged"}
			]}
    	])

    	this.render();

    	return this.$[id]
    }


})