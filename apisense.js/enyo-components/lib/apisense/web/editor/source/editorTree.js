
if (!apisense) apisense = {};
if (!apisense.editor) apisense.editor = {};
apisense.editor.assets = function(ima){ return  enyo.path.paths.lib+"/apisense/web/editor/assets/"+ima }

enyo.kind({
	name: "apisense.editorTree",
	classes: "enyo-unselectable enyo-fit apisense-tree",
	kind: "FittableRows",
	fit: true,
	components: [

		{ name : "mainContainer", components : []}

		
	],

	constructor : function(object){

        this.inherited(arguments);

        if (!enyo.assetPath){
        	enyo.assetPath = ""
        }

   		
    },

	nodeExpand: function(inSender, inEvent) {
		
		var icon = inSender.expanded ? apisense.editor.assets("folder-open.png") : apisense.editor.assets("folder.png")

		inSender.setIcon(icon);
	},
	nodeTap: function(inSender, inEvent) {
		var node = inEvent.originator;
		this.$.selection.select(node.id, node);
	},


	select: function(inSender, inEvent) {

		this.selectedFile = inEvent.data.file;

		
		if (inEvent.data){
			inEvent.data.$.caption.applyStyle("background-color", "lightblue");
		}

		// if selected file is not a folder
		if (this.folders.indexOf(this.selectedFile) < 0 ){
			if (this.onSelectedFile) this.onSelectedFile(this.selectedFile);
		}

	},
	deselect: function(inSender, inEvent) {

		if (inEvent.data.$.caption){
			inEvent.data.$.caption.applyStyle("background-color", null);
		}
	},

	setOnSelectedFile : function(callback){

		this.onSelectedFile = callback;

	},

	getFolders : function(){ return this.folders; },

	update : function(tree){
		
		this.folders = [tree.nodeName];
		this.files = [];

		this.$.mainContainer.destroy();

		this.rootName = tree.nodeName;

		var treeContainer = {
			name : "treeContainer",
			kind: "Node",
			content: tree.nodeName,
			expandable: true,
			expanded: true,
			file : tree.nodeName,
			icon : apisense.editor.assets("folder-open.png"),
			onExpand: "nodeExpand",
			onNodeTap: "nodeTap",
			components: []}
			
		
		if (tree.childs){

			for (var childs in tree.childs){

				this._update(tree.nodeName,treeContainer,tree.childs[childs]);
			}
		}

		this.createComponents([{ name : "mainContainer", components : [
			{kind: "Selection", onSelect: "select", onDeselect: "deselect"},
			{name : "tree", kind: "Scroller", fit: true, components: [treeContainer]}
		]}]);

		this.render();
	},

	_update : function(basePath, treeContainer, tree){

		if (tree.childs){

			this.folders.push(basePath+"/"+tree.nodeName);

			var component = {icon: apisense.editor.assets("folder-open.png"), file : basePath+"/"+tree.nodeName, content: tree.nodeName, expandable: true, expanded: true, components: []};
			for (var childs in tree.childs){

				this._update(basePath+"/"+tree.nodeName,component,tree.childs[childs])
			}
			treeContainer.components.push(component);

		}else{
			
			var icon = "$assets/file.png";
			var filename = tree.nodeName;

			
			if (filename.match(".js$")) icon = apisense.editor.assets("file-js.png")
			else if (filename.match(".xq$")) icon = apisense.editor.assets("file-xq.png")
			else if (filename.match(".py$")) icon = apisense.editor.assets("file-py.png")
			else if (filename.match(".css$")) icon = apisense.editor.assets("file-css.png")
			else if (filename.match(".html$")) icon = apisense.editor.assets("file-html.png")
			
			treeContainer.components.push({icon: icon, content: filename, file : basePath+"/"+tree.nodeName});
		    this.files.push(basePath+"/"+tree.nodeName);
		}
	}
});

