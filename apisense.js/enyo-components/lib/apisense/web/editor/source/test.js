
enyo.kind({
	name : "apisense.ExperimentConfiguration",
	kind : "apisense.BaseModal",

	create : function(object){ this.inherited(arguments);

		this.items = [];

		
		this.items.push(this.input("version","Script Version","version-1"))
		this.items.push(this.input("url","Service URL  ","http://"))
		this.items.push(this.list("main","Main File",["value.js","main.js"],"main.js"));
		this.items.push(this.list("visible","Private",["True","False"],"True"));

		this.onValid  = object.onValid  || function(){};
		this.onCancel = object.onCancel || function(){};

		this.$.modalContent.itemSelected = this.itemSelected;

		this.formResult = {}

	},

	valid : function(){ 

		var results = {};
		for (var i in this.items){

			var r = this.items[i]();
			results[r.name] = r.value;
		}
		
		this.onValid(results);
		this.hide();this.destroy(); 
	},

	input : function(name,label,defaultValue){

		defaultValue = defaultValue || ""

		this.$.modalContent.createComponent({
			 kind : "FittableColumns", style:"padding-bottom:30px;",components : [
			     {content : label, style : "font-weight:bold;padding-top:8px;width : 150px;"},
			     {kind: "onyx.InputDecorator", components: [ {kind: "onyx.Input", style:"width:300px;", name : name, value : defaultValue} ]}
			]}
		)

		var self = this;
		return function(){

			return {
				name  : name,
				value : self.$.modalContent.$[name].getValue()
			}
		}
	},

	label : function(name){ this.$.modalContent.createComponent({classes: "onyx-sample-divider", content: name});},

		
	radio : function(){


		this.$.modalContent.createComponent({kind: "Group", classes: "onyx-sample-tools group", onActivate:"groupActivated", highlander: true, components: [
			{kind:"onyx.Checkbox", checked: true},
			{kind:"onyx.Checkbox"},
			{kind:"onyx.Checkbox"}
		]})

	},

	list : function(name,label,_items, defaultValue){

		defaultValue = defaultValue || ""

		var items = [];
		for (var i in _items){
			items.push({content: _items[i], index : name});
		}

		var p = this.$.modalContent.createComponents([
			{content : label, style : "font-weight:bold;padding-top:8px;padding-bottom:10px;width:200px;"},
			{kind: "onyx.MenuDecorator", onSelect: "itemSelected",
				components: [
					{name : name, content: defaultValue },
					{kind: "onyx.Menu", components: items}
			]}
		])

		var self = this;
		return function(){
			
			return { 
				name  : name, 
				value : self.$.modalContent.$[name].getContent()
			}
		}
	},

	itemSelected: function(inSender, inEvent) {
		var el = inEvent.originator;
		this.$[el.index].setContent(el.content)
	},
})





