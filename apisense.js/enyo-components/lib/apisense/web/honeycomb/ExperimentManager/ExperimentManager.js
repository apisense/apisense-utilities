// https://github.com/joewalnes/reconnecting-websocket/
function ReconnectingWebSocket(a){function f(g){c=new WebSocket(a);if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","attempt-connect",a)}var h=c;var i=setTimeout(function(){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","connection-timeout",a)}e=true;h.close();e=false},b.timeoutInterval);c.onopen=function(c){clearTimeout(i);if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","onopen",a)}b.readyState=WebSocket.OPEN;g=false;b.onopen(c)};c.onclose=function(h){clearTimeout(i);c=null;if(d){b.readyState=WebSocket.CLOSED;b.onclose(h)}else{b.readyState=WebSocket.CONNECTING;if(!g&&!e){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","onclose",a)}b.onclose(h)}setTimeout(function(){f(true)},b.reconnectInterval)}};c.onmessage=function(c){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","onmessage",a,c.data)}b.onmessage(c)};c.onerror=function(c){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","onerror",a,c)}b.onerror(c)}}this.debug=false;this.reconnectInterval=1e3;this.timeoutInterval=2e3;var b=this;var c;var d=false;var e=false;this.url=a;this.readyState=WebSocket.CONNECTING;this.URL=a;this.onopen=function(a){};this.onclose=function(a){};this.onmessage=function(a){};this.onerror=function(a){};f(a);this.send=function(d){if(c){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","send",a,d)}return c.send(d)}else{throw"INVALID_STATE_ERR : Pausing to reconnect websocket"}};this.close=function(){if(c){d=true;c.close()}};this.refresh=function(){if(c){c.close()}}}ReconnectingWebSocket.debugAll=false

enyo.kind({
	
	name: "A.honey.ExperimentManager",
	
	kind : enyo.Scroller,

	components : [

		{ layoutKind: "com.MasonryLayout", name : "thumb" },

		{kind: "Notification", name: "notif",style:"z-index:10000;"},

		{kind: "apisense.web.utils.TaskManager", name: "notifTask"}
	],

	create : function(object){  this.inherited(arguments); 

		var self = this;

		this.honey = object.honey;
		this.experimentManager = object.honey.experimentManager();

		// get all experiments and create a thumbnail
		// object for each experiments
		var request = this.experimentManager.experiments();
		request.onSuccess = function(response){

			response.forEach(function(xp){

				self.addThumb(xp.experiment)
			})
					
			self.render();
		}

		// get websocket url for process notification
		this.experimentManager.processChannel(function(ws){
			
			self.initWebSocket(ws)
			var f = self.experimentManager.getPendingProcess();
			f.onSuccess = function(data){
				for (i in data){ self.notifTask(data[i]); }
			};

		});
		
		self.renderInto(object.into)		
	},

	initWebSocket : function(channel){ var self = this
		
		if (this.channel == undefined){
			
			if (!window.WebSocket) { window.WebSocket = window.MozWebSocket; }
			if (window.WebSocket){

				this.channel = new ReconnectingWebSocket("ws://"+channel);
				this.channel.onmessage = function(event) {
					
					var data = JSON.parse(event.data);
					self.notifTask(data);

				}
				this.channel.onopen = function(event){ }
				this.channel.onclose = function(event){}
			}
		}

	},

	/**
	 * Forward task notification 
	 * @param task JSON Object of current task description
	 			   JSON Structure
	 			   {
						id      	: task id
						name    	: task name
						message 	: message
						status  	: task status
						progress    : current task progress
						progressMax : maximum task progress
	 			   }
	 */
	notifTask : function(task){ 

		this.$.notifTask.put(task);
		this.$.notifTask.animateToMin();
	},

	notif : function(title,message,stay){

		this.sendNotif(title,message,"notification.PageCurl", undefined,1);
	},

	sendNotif: function(title,message, theme, stay, duration) {
		return this.$.notif.sendNotification({
			title: title,
			message: message,
			icon : "http://static.apisense.fr/g/logo/sting.png",
			theme: theme,
			stay: stay,
			duration: duration
		});
	},

	addThumb : function(json){ 

		this.$.thumb.createComponent({
			kind : "A.honey.ExperimentThumb" , 
			experiment : json, 
			thumbManager : this , 
			honey : this.honey, 
			experimentManager : this.experimentManager
		}); 
	}
});

enyo.kind({

	name: "A.honey.ExperimentThumb",

	//classes : "thumb-stopped",

	components : [

		{  kind : "FittableColumns", classes : "thumb-header",components : [
			
			{ name : "ThumbName", classes : "thumb-name" },
			
			// menu 
			{ name : "ThumMenu" , classes : "thumb-menu",components : [
				{  name: "ThumbMenuContent", kind : "FittableColumns" , components : [
					
					{kind:"onyx.Button", classes: "icon-refresh thumb-button", ontap:"update"},

					{name : "experimentControl", kind:"onyx.Button", classes: "icon-play thumb-button"},

					{name : "experimentEdit", kind:"onyx.Button", classes: "icon-edit thumb-button", ontap:"edit"},
				
					{kind: "onyx.MenuDecorator", onSelect: "itemSelected", components: [
						{classes : "icon-collapse thumb-button"},
						{kind: "onyx.Menu", floating: true, components: [
							
							{name : "remoteControl", content : "remote start"},

							{name : "menuConfiguration", content : "configuration", ontap : "serviceConfiguration"},
							
							{classes: "onyx-menu-divider"},
							{name : "menuImport", content : "import", ontap : "importCollections"},
							{name : "menuExport", content : "export",  ontap : "exportCollections"},
							{name : "menuDownload", content : "download export", ontap : "downloadCollection"},
							{classes: "onyx-menu-divider"},
							{ content : "finish" , ontap : "remoteFinish"},
						]}
					]}
				]}
			]}
		]},

		{ name : "spinner", kind:"onyx.Spinner" , style:"position:absolute;left : 45%"},

		{ name : "ThumbContent", components : [

			{ name : "ThumbChart", classes : "thumb-chart" },
			
			{ name : "ThumbBottom" , classes : "thumb-bottom", components : [

				{ name : "ThumbState" },

				{ name : "ThumbParticipant" , classes : "icon-user "}

			]}
		]}
	],

	create : function(object){ this.inherited(arguments); var self = this;


		this.xp = object.experiment;
		this.xpName = this.xp.name;
		this.xpNice = this.xp.niceName || this.xp.name;

		
		this.collector = object.honey.experiment(this.xpName).collector();
		
		this.experimentManager = object.experimentManager;
		this.thumbManager = object.thumbManager;

		this.$.ThumbName.setContent(this.xpNice);

		this.updateUserRegistered();
		this.update();
	},

	edit : function(){

		this.thumbManager.experimentManager.editPage(this.xpName);
	},

	updateUserRegistered : function(){ var self = this;

		var regiteredUserRequest = self.experimentManager.users(self.xpName);
		regiteredUserRequest.onSuccess = function(data){

			var userNumber = 0;
			if (data.registration.user){

				if (data.registration.user.length){

					userNumber = data.registration.user.length;
				}
				else userNumber = 1;
			}

			self.$.ThumbParticipant.setContent(" "+userNumber);
		};
	},

	update : function(){ var self = this;

		this.$.spinner.applyStyle("display","block");



		if (this.xp.status == "true"){

			var request = self.collector.stats() 
			request.onSuccess = function(stats){ self._update(self.xp,stats); }
			request.onError = function(){ self._update(self.xp,[]); }

		}else{

			self._update(this.xp,[]);
		}
	},

	_update : function(experiment,stats){

		this.$.spinner.applyStyle("display","none");

		var xpName = experiment.name;
		var remoteState = experiment.remoteState || "stopped";

		// remove old state class if exist
		if (this.stateClass){  this.removeClass(this.stateClass);  }
		
		// experiment service are not started
		if (experiment.status == "false"){
			
			this.stateClass = "thumb-remoteStopped"; 

			this.$.remoteControl.setContent("start services")
			this.$.remoteControl.ontap = "remoteStart";

			this.$.experimentControl.setDisabled(true);
			this.$.experimentControl.ontap = "";

			this.$.experimentEdit.setDisabled(true);
			this.$.experimentEdit.ontap = "";

			// hide import/export menu
			this.$.menuImport.hide();
			this.$.menuExport.hide();
		}

		// experiment service is started
		else{
			
			// show import/export menu
			this.$.menuImport.show();
			this.$.menuExport.show();
						
			// experiment is in running mode
			if (remoteState == "started"){
				
				this.stateClass = "thumb-started"; 
				
				this.$.remoteControl.setContent("stop services")
			    this.$.remoteControl.ontap = "remoteStop";
			    
			    this.$.experimentControl.setDisabled(false);
			    this.$.experimentControl.ontap = "stop";
			    this.$.experimentControl.removeClass("icon-play");
			    this.$.experimentControl.addClass("icon-stop");
			    
			    this.$.experimentEdit.setDisabled(false);
			    this.$.experimentEdit.ontap = "edit";

			}
			// experiment is in pause mode
			else{
				
				this.stateClass = "thumb-stopped";
				this.$.remoteControl.setContent("stop services")
			    this.$.remoteControl.ontap = "remoteStop";
			   
			    this.$.experimentControl.setDisabled(false);
			    this.$.experimentControl.ontap = "start";
			    
			    this.$.experimentControl.removeClass("icon-stop");
			    this.$.experimentControl.addClass("icon-play");
			    
			    this.$.experimentEdit.setDisabled(false);
			    this.$.experimentEdit.ontap = "edit";
			}	
		}
		
		this.$.ThumbState.setContent(" state : "+experiment.remoteState);
		this.$.ThumbParticipant.setContent(" "+experiment.participant);
		 
		this.addClass("masonry-brick "+this.stateClass);

		this.drawChart(stats);
	},

	_requestSuccess : function(mess){ var self = this;

		if (mess == "") mess =  "success"

		if (mess instanceof Object){
			if (mess.success) mess = mess.success;
			else if (mess.error) mess = mess.error;
			else mess = JSON.stringify(mess);
		}
		
		this.experimentManager.experiment(this.xpName).onSuccess = function(xp){
			//
			self.experiment.description = xp[self.xpName].description;
			self.experiment.status = xp[self.xpName].state;
			self.experiment.remoteState = xp[self.xpName].experiment.remoteState;
			self.thumbManager.notif("Experiment Notification",mess);
			self.update();
		}
	},

	_requestError : function(error){ var self = this;

		var message = error.message || new date();
		this.thumbManager.notif("Error",JSON.stringify(message));

		this.experimentManager.experiment(this.xpName).onSuccess = function(xp){
			self.experiment.status = xp[self.xpName].state;
			self.experiment.remoteState = xp[self.xpName].experiment.remoteState;
			self.update();
		}
	},

	remoteStart : function(){ var self = this;

		this.$.spinner.applyStyle("display","block");

		var request = this.experimentManager.remoteStart(this.xpName);
		request.onSuccess = function(m){self._requestSuccess(m);}
		request.onError = function(m){self._requestError(m);}
	},

	remoteStop : function(){ var self = this;

		this.$.spinner.applyStyle("display","block");

		var request = this.experimentManager.remoteStop(this.xpName);
		request.onSuccess = function(m){self._requestSuccess(m);}
		request.onError = function(m){self._requestError(m);}	
	},

	remoteFinish : function(){ var self = this;

		var request = this.experimentManager.finish(this.xpName);
		request.onSuccess = function(m){self._requestSuccess(m);}
		request.onError = function(m){self._requestError(m);}
	},

	start : function(){ var self = this;

		this.$.spinner.applyStyle("display","block");

		alert(this.xpName);

		var request = this.experimentManager.start(this.xpName);
		request.onSuccess = function(m){self._requestSuccess(m);}
		request.onError = function(m){self._requestError(m);}	
	},

	stop : function(){ var self = this;

		this.$.spinner.applyStyle("display","block");

		var request = this.experimentManager.stop(this.xpName);
		request.onSuccess = function(m){self._requestSuccess(m);}
		request.onError = function(m){self._requestError(m);}
	},

	importCollections : function(){

		var modal = new apisense.modal.ImportDownload({ title : "Import data", url : this.collector.service_import_resource })
		

	},

	serviceConfiguration : function(){

		new apisense.modal.ConfigurationModal({ title : "Service Configuration", thumb : this ,manager : this.experimentManager, xp : this.xp })

	},

	downloadCollection : function(){ var self = this;

		var future = this.collector.getExportResources();
		future.onSuccess = function(data){

			var modal = new apisense.modal.ExportDownload({ title : "Download Export" })
		
			for (var i in data){

				modal.addDownload( data[i]);
			}
			
		
			modal.onDownload = function(file){  self.collector.resource(file); }

			modal.onDelete = function(file){ 

				self.collector.deleteResource(file).onSuccess = function(data){
					self.thumbManager.notif(data,data);
					self.downloadCollection();

				};
				modal.cancel();
				
			}
			modal.show();
		};

	},

	exportCollections : function(){ var self = this;

		var future = this.collector.collections();
		future.onSuccess = function(collections){

			var modal = new apisense.modal.Form({title : "MyTitiel", style:"width:700px;"});
			modal.divider("margin-top:20px;");
			modal.input("exportName","Export Name : ",self.xpName+"-"+new Date().toISOString().substring(0,10))
			modal.toggle("selectAll","selectAll",false);
			modal.divider("margin-top:20px;");

			for (var index in collections){

				modal.checkbox(collections[index],collections[index],false);
			}
			modal.onChange = function(modal,name){

				if (name == "selectAll"){

					var selectAll = modal.$.modalContent.$.selectAll.getValue();
					for (i in collections){
						modal.$.modalContent.$[collections[i]].setActive(selectAll);
					}
				}
			}
			modal.onValid = function(data){

				var selectedCollections = [];
				for (var i in data){
					
					if (i == "exportName"){

						filename = data[i];
					}
					else if(i != "selectAll"){

						if (data[i]){ selectedCollections.push(i)}
					}
				}

				
				if (selectedCollections.length > 0){

					 var future = self.collector.exportCollections(selectedCollections,filename,"json")
					 future.onSuccess = function(data){

					 	self.thumbManager.notifTask(data);
					 }

					 future.onError = function(request,error){
					 	console.log(request);
					 	console.log(error);
					 }
				}
			}

			modal.show();
		}
			
	},

	drawChart : function(dbs){ var self = this;

		var id = this.$.ThumbChart.id

		if (dbs.length == 0){
			this.$.ThumbChart.allowHtml = true;
			this.$.ThumbChart.setContent("<div class='thumb-nochart' >no data</div>")
			return; 
		}
	  
		var datas = [];
		dbs.forEach(function(stat){

			if (stat.name.indexOf("20") == -1) return;

			// get database name
			var name = stat.name.substring(stat.name.indexOf("20")).split("-");
				time = Date.UTC(name[name.length - 3],name[name.length - 2]-1,name[name.length - 1]);
			    sizeArray = stat.size.split(" ")
				size = parseInt(sizeArray[0])
			
			// size is in KB
			if (sizeArray[1].indexOf(0) == "K"){

				size = size * 1024
			}

			// size if in Mega byte
			else if (sizeArray[1].indexOf(0) == "M"){

				size = size * 1048576
			}

			// size if in Giga byte
			else if (sizeArray[1].indexOf(0) == "G"){

				size = size * 1073741824
			}
			
			datas.push([ time,size ])
		})

	  	this.thumbChart = new Highcharts.Chart({
        		chart: {
            		renderTo: id,
            		type: 'column',
            		backgroundColor: null,
            		zoomType: 'x'
        		},

        		title: {
            		text: 'Collected Data : '+this.xpName
        		},
        		xAxis: {
        			type: 'datetime'
            	},
        		yAxis: {
            		title: { text: 'Bytes' }
        		},
        		series: [{
        			showInLegend: false,       
        			data: datas}
			    ]
    	});
		
	}

})

/**
 *
 * Modal popup
 *
 * Experiment Configuration
 *
 */
enyo.kind({
	
	name : "apisense.modal.ConfigurationModal",

	kind : "apisense.modal.Modal",

	style : "width:1000px;",

	create : function(object){ this.inherited(arguments); var self = this; 

		this.$.modalContent.createComponents([

			{name:"bar",kind: "onyx.TabBar", onTabChanged: "switchTab"},
			
			{ components: [	{ 
				name : "tabpannel", 
				draggable : false,
				kind : "Panels",
				arrangerKind: "CardArranger",
				style : "min-width : 800px;min-height:450px;" ,
				components : [ 
					
					{ 
						kind:"A.honey.ConfigurationGlobalView", 
						name : "panelGlobal",
						manager : object.manager, 
						xp : object.xp
					},
					{ 
						kind:"A.honey.ConfigurationView", 
						name : "panelConfService",
						manager : object.manager, 
						xp : object.xp
					}
				]}
			]}
		]);

		this.tabbar = this.$.modalContent.$.bar;
		this.tabpannel = this.$.modalContent.$.tabpannel
		
		this.panelService = this.$.modalContent.$.panelConfService
		this.panelGlobal = this.$.modalContent.$.panelConfGlobal

		this.tabbar.addTab({caption : 'Global Configuration',   data : { index : 0 }});
		this.tabbar.addTab({caption : 'Service Configuration',  data : { index : 1 }});
		

		this.tabpannel.setIndex(0);
		this.thumb = object.thumb;

		this.show();

	},

	valid : function(){ var self = this;

		this.tabpannel.getActive().valid(function(status, mess){
			self.hide();self.destroy();
			self.thumb._requestSuccess(mess);
		});
	},

	handlers: { onTabChanged: "switchTab" },

	switchTab : function(inSender,inEvent){  this.tabpannel.setIndex(inEvent.data.index); }
});





enyo.kind({
	
	name : "apisense.modal.ImportDownload",

	kind : "apisense.modal.Modal",

	create : function(object){ this.inherited(arguments); var self = this; 

		this.$.modalBottom.destroy();
		var zone = this.createComponent({ 

			 allowHtml: true,
			 style : "background:#d6d6d6;height:200px;color:black;",
			 tag : "div",
			 content : " <center><strong> Drop file to upload </strong><br/> <i>(or click)</i></center> "
		})

		this.show();
		
		new Dropzone("#"+zone.id, {
				url : object.url,
  				init: function() {
    				this.on("success", function() { });
    				this.on("error", function()   { });
  				}
		});
		document.querySelector("#"+zone.id).classList.add("dropzone");
	}
})

enyo.kind({
	name : "apisense.modal.ExportDownload",
	kind : "apisense.modal.Modal",

	style : "min-width : 50%",

	create : function(object){  var self = this;

		this.inherited(arguments);
		this.$.modalBottom.destroy();

		this.$.modalContent._onDownload = function(inSender){ self.onDownload(inSender.file) }
		this.$.modalContent._onDelete   = function(inSender){ self.onDelete(inSender.file) }
	},

	onDownload : function(filename){},
	onDelete   : function(filename){},	

	addDownload : function(name, onDownload, onDelete){

		this.$.modalContent.createComponent( { kind : "FittableColumns", style : "margin-top:10px;", components : [

			{ content : name, style : "font-weight:bold;padding-top:8px;width : 70%;" },

			{kind:"onyx.Button", file : name, content: "Download", classes: "onyx-blue", style : "margin-right:20px;", ontap: "_onDownload"},

			{kind:"onyx.Button", file : name, content: "Delete", classes: "onyx-negative", ontap: "_onDelete"},

		]});

		
	}
})

