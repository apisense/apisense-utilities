
enyo.kind({

	name: "A.honey.ConfigurationGlobalView",

	style : "padding : 30px;background:#777;",

	kind : "enyo.Scroller",

	components : [

		// experiment name input
		{ kind : "FittableColumns", 
		  style:"padding-bottom:30px;",
		  components : [
			     
			     {content : "Name : ", style : "font-weight:bold;padding-top:8px;width : 200px;"},
			     
			     {kind: "onyx.InputDecorator", components: [ {kind: "onyx.Input", style:"width:400px;", name : "xpName", value : "???" } ]}
			]
		},

		// experiment nice name input
		{ kind : "FittableColumns", 
		  style:"padding-bottom:30px;",
		  components : [
			     
			     {content : "Nice Name : ", style : "font-weight:bold;padding-top:8px;width : 200px;"},
			     
			     {kind: "onyx.InputDecorator", components: [ {kind: "onyx.Input", style:"width:400px;", name : "xpNiceName", value : "???" } ]}
			]
		},

		// experiment description
		{ kind : "FittableColumns", 
		  style:"padding-bottom:30px;",
		  components : [
			     
			     {content : "Description Name : ", style : "font-weight:bold;padding-top:8px;width : 200px;"},
			     
			     {kind: "onyx.InputDecorator", components: [ {kind: "onyx.RichText", style:"color:black;", name : "xpDescription", value : "<b>coucou</b> : Hello" }]}
			]
		},

		// experiment description
		{ kind : "FittableColumns", 
		  style:"padding-bottom:30px;",
		  components : [
			     
			     {content : "Description Name : ", style : "font-weight:bold;padding-top:8px;width : 200px;"},
			     
			     {kind: "onyx.InputDecorator", components: [ {kind: "onyx.RichText", style:"color:black;", name : "xpCopyright", value : "<b>coucou</b> : Hello" }]}
			]
		}

	],

	create : function(object){  this.inherited(arguments); var self = this;

	
		this.$.xpName.setValue(object.xp.name)
		this.$.xpNiceName.setValue(object.xp.niceName)
		this.$.xpDescription.setValue(Base64.fromBase64(object.xp.description));
		this.$.xpCopyright.setValue(Base64.fromBase64(object.xp.copyright));

		this.valid = function(callback){

			var request = object.manager.updateDescription(
				this.$.xpName.getValue(),
				this.$.xpNiceName.getValue(),
				this.$.xpDescription.getValue(),
				this.$.xpCopyright.getValue()
			);
			request.onSuccess = function(r){ callback("success",""); }
			request.onError   = function(r){ callback("error",r); }
		}
	}
})


enyo.kind({
	
	name: "A.honey.ConfigurationView",
	
	style : "width:100%;height:100%;background:#777",

	components : [

		{ name : "graph", tag : "div", style : "width: 100%; height: 100%;"}
	
	],

	create : function(object){  this.inherited(arguments); var self = this;


		this.manager = object.manager;

		var request = this.manager.configuration(object.xp.name);
		request.onSuccess = function(result){

			
			var selectedFeatures = self.getPreSelectedFeatures(result["slice"]["variants"],{})

			self.manager.defaultConfiguration(object.xp.name).onSuccess = function(r){


				self.fdViewer = new FDViewver({
					id : self.$.graph.id,
					color1 : "#EEEEEE",
					color2 : "#FFA500",
					nodeWidth:150,
					nodeHeight:20,
					textSize : "13px"
				})
				
				self.fdViewer.load(r["slice"]["variants"],selectedFeatures);
			};
		};

		this.valid = function(callback){

			var request = object.manager.updateConfiguration(
				self.xp.name,
				self.fdViewer.selected);
			request.onSuccess = function(r){ callback("success","please restart experiment services"); }
			request.onError   = function(r){ callback("error",r); }

		}
	},

	switchTabs: function (inSender, inResponse) {
        this.$.AppViews.setIndex(inResponse.originator.index);
    },

    getSelectedFeatures : function(){ return  this.fdViewer.selected},

    getPreSelectedFeatures : function(fd, result){ var self = this;

    	if (fd.feature.cardinality >= 0 ){

    		result[fd.feature.name] = fd.feature.cardinality;
    	}
    	
    	if (fd.feature.variants){
    		fd.feature.variants.forEach(function(vr){

    			self.getPreSelectedFeatures(vr,result);
    		})
    	}

    	return result;
    }

});
