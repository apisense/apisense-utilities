//******************************************************************
//*** Author : Haderer Nicolas
//*** 
//***
//******************************************************************


A.apisurvey.form.plugins = {

	build : function(args){ 
		var returnObject = {};
		if ((typeof args[0]) == "string"){
			returnObject.title = args[0];
		}
		else returnObject = args[0];
		return returnObject;
	}
}


/**
 *
 * @class Abstract Form element
 *
 */
enyo.kind({
	
	name : "EnyoSurveyElement",
	
	components : [  {kind: "FittableRows", name : "elementContent", components : [] } ],
		
	constructor : function(object){

		this.inherited(arguments);
		this.indexSurvey = 0;
		this.label = object.label || undefined;

		this.title = object.title || "";
	},

	create : function(){	

		this.inherited(arguments);
		this.$.elementContent.createComponent({
		   
		   components : [
		   		
		   		{content : this.indexSurvey+". "+this.title, classes : "survey-form-element-label",name : "labelElement"},
				{kind: "onyx.Popup",classes : "popup-error", components: [
     				 {name : "popupContent", content: "Hello World!"}
  				]}
			]
		})
	},

	error : function(message){

		var pop = this.$.elementContent.$.popup;
		this.$.elementContent.$.popupContent.setContent(message);
		pop.show();

	},

	setIndexSurvey : function(index){
		this.indexSurvey = index;
		var labelElement = this.$.elementContent.$.labelElement;
		labelElement.content = this.indexSurvey+". "+this.title,
		labelElement.render();
	},

	load : function(result){

		console.log("Load function not implement for kind "+this.name)
	},

	resultMedia : function(){ return [{}]; }
})


/**
 *
 * Class: EnyoSurveyForm
 */
enyo.kind({
	name : "EnyoSurveyForm",
	kind : "FittableRows",
	components : [  
		{kind: "Panels", fit: true, draggable: false, classes: "scroller-sample-panels",style: "height: 100%;", components: [
			{kind: "Scroller", classes: "enyo-fit",strategyKind: "TranslateScrollStrategy"}
		]}
	],

	constructor : function(args){

		this.inherited(arguments);

		this.formIndex = args.formIndex;
		
		if (typeof this.label == "number")  this.label = "q"+this.label ;
		
	},

	create : function(){

		this.inherited(arguments);
		
		var self = this;

		for (var i in A.apisurvey.form.plugins){
			(function(i){
				self[i] = function(){
					return self.add(A.apisurvey.form.plugins[i](arguments));
				}
			})(i)
		}

		this.elements = [];
	},

	hasNext : function(){ return this.getNext != undefined},
	hasPrevious : function(){return this.getPrevious != undefined},

	next : function(nextQuestion){

		var self = this;

		if ((typeof nextQuestion) == "function"){

			this.getNext = function(){
				
				var _nextQuestion = nextQuestion(self.result()[self.label]);	
				_nextQuestion.previous(self);
				return _nextQuestion; 
			}

		}else{

			this.getNext = function(){

				nextQuestion.previous(self);
				return nextQuestion;
			}

		}

		if (this.survey){ this.survey.update(); }
	},

	previous : function(previousQuestion){
		this.getPrevious = function(){ return  previousQuestion;}
	},

	text : function(html){

		var scrollers = this.$.panels.getPanels()[0];
		var component = scrollers.createComponent({classes : "survey-form-text", content : html,allowHtml: "true"})
		return this;

	},

	add : function(element){
		
		var index = 0;
		
		if (this.survey){ index = this.survey.createIndex();}
		else{ index = this.elements.length + 1 }

		var scrollers = this.$.panels.getPanels()[0];
		
		var component = scrollers.createComponent(element)
		
		component.setIndexSurvey(index);

		if (!component.label){

			component.label = "elem-"+this.elements.length + 1;	
		}
		

		this.elements.push(component);
		

		this.$.panels.render();
		return this;
	},

	setSurvey : function(survey){
		this.survey = survey;
	},

	valid : function(){

		for (i in this.elements){
			if (this.elements[i].valid){
			  if (!this.elements[i].valid()){
			  	return false;
			  }
			}
		}
		return true;
	},

	load : function(result){
		
		var responses = result.responses[this.label];
		var medias    = result.medias;



		for (i in this.elements){

			var elem = this.elements[i];
			elem.load(responses[elem.label],result.medias[i])
		}
	},


	result : function(){

		var results = {};
		
		for (i in this.elements){
			
			var element = this.elements[i];
			var _label = element.label;
			if (element.result){

				
				results[_label] = { title : element.title, value : element.result()}
			}
		}
		var finalResult = {};
		finalResult[this.label] = results;
		return finalResult;
	},

	resultMedia : function(){

		var results = [];
      	for (i in this.elements){
			
			var element = this.elements[i];

			if (element.resultMedia){
				
				var medias = element.resultMedia();

				for (var j in medias){
					results.push(medias[j])
				}
			}
		}

		return results;
	}

})





