
//******************************************************************
//*** Author : Haderer Nicolas
//*** Form plugin enabling media capture
//***
//******************************************************************

A.apisurvey.media = {}

A.apisurvey.submitMedia = function(mediaId,filePath){
	var callback = this.media[mediaId];
	callback(mediaId,filePath);
}

/** *************************************************************************************************************************  IMAGE MEDIA */

A.apisurvey.mediaPicture = function(format,quality,callback){

	//var mediaId = $survey.picture(format,quality);
	var mediaId = $survey.picture(format);
	this.media[mediaId] = callback;
}

A.apisurvey.form.plugins.image = function(args){
	var element = this.build(args);

	element.kind = "EnyoSurveyPicture";
	element.format  = args[1] || "png";
	element.quality = args[2] || "85";
	return element;
};

enyo.kind({
	
	name : "EnyoSurveyPicture",
	
	kind : "EnyoSurveyElement",
	
	components : [  {kind: "FittableRows", name : "elementContent", components : []}],
	
	constructor : function(object){ 

		this.inherited(arguments);
		this.format = object.format || "png";
		this.quality = object.quality || "85";
	},

	create : function(){	

		this.inherited(arguments);

		this.createComponent({kind: "FittableRows", classes : "survey-picture", components : [
			{kind: "Image", name : "pictureElement", classes:"survey-picture-overview"},
			{tag: "br"},
			{kind: "onyx.Button", classes:"onyx-blue survey-picture-button", name:"Image Button", ontap:"buttonTapped", components: [
				{tag: "img", attributes: {src: "assets/ic_camera.png"}},
				{content: "Click to take a picture"}
			]}
		]})

		this.pictureElement = this.$.pictureElement
		this.pictureElement.hide();
	},

	buttonTapped : function(){ var self = this;

		A.apisurvey.mediaPicture(this.format,this.quality,function(fileId,filePath){

   			self.pictureElement.show();
			self.imageId = fileId;
			self.imagePath = filePath;
			self.pictureElement.setSrc(self.imagePath)
			self.render();			
		})
	},
	
	valid : function(){ 

		if (this.imageId == undefined){
			this.error("No image");
			return false;
		}

		return true;
	},

	result : function(){  return { type : "image/"+this.format, id : this.imageId};  },

	resultMedia : function(){ return [ { uri : this.imagePath, id : this.imageId }] }
})


/** *************************************************************************************************************************  SOUND MEDIA */

A.apisurvey.form.plugins.audio = function(args){
	var element = this.build(args);
	element.kind = "EnyoSurveyAudio";
	return element;
};

A.apisurvey.mediaAudio = function(callback){
	var fileId = $survey.audio();
	this.media[fileId] = callback;
}


enyo.kind({
	
	name : "EnyoSurveyAudio",
	
	kind : "EnyoSurveyElement",

	components : [  {kind: "FittableRows", name : "elementContent", components : []}],
	
	constructor : function(object){ 

		this.inherited(arguments);
	},

	create : function(){	

		this.inherited(arguments);

		this.createComponent({kind: "FittableRows", classes : "survey-sound", components : [
			
			{kind: "FittableRows" , name:"soundLayout", classes:"survey-sound-layout", components : []},
			{kind: "onyx.Button", classes:"onyx-blue survey-sound-button", name:"Image Button", ontap:"buttonTapped", components: [
				{tag: "img", attributes: {src: "assets/ic_audio.png"}},
				{content: "Click to Record"}
			]}
		]})

		this.pictureElement = this.$.pictureElement
	},

	_createAudioComponent : function(src){

		var htmlContent = " <video  width='100%' height='50px' controls> "
		htmlContent = htmlContent + '<source src="'+src+'" type="audio/3gp">'
		htmlContent = htmlContent + '<source src="'+src+'" type="audio/mpeg">'
		htmlContent = htmlContent + '<embed  src="'+src+'">'
 		htmlContent = htmlContent + '</video>'
 		
 		return {
 			tag : "div",
 			name : "audioContent",
 			allowHtml: "true",
 			content : htmlContent
 		}
	},

	buttonTapped : function(){

		var self = this;

		A.apisurvey.mediaAudio(function(mediaId, mediaUri){
			
			var audioElement = self.$.soundLayout.$.audioContent;
		    if (audioElement){
		 	  audioElement.destroy();
		    }
		   
		    self.mediaId  = mediaId;
		    self.mediaUri = mediaUri
			self.$.soundLayout.createComponent(self._createAudioComponent(mediaUri));
			self.render();
		})
	},

	valid : function(){ 

		if (this.mediaId == undefined){
			this.error("No Audio");
			return false;
		}

		return true;
	},

	result : function(){  return {  type : "audio/3gpp", id : this.mediaId }  },

	resultMedia : function(){ return [ { uri : this.mediaUri, id : this.mediaId }] }
})





/** *************************************************************************************************************************  VIDEO MEDIA */

A.apisurvey.form.plugins.video = function(args){
	var element = this.build(args);
	element.kind = "EnyoSurveyVideo";
	return element;
};

A.apisurvey.mediaVideo = function(callback){

	var fileId = $survey.video();
	this.media[fileId] = callback;
}

/**
 *
 * Media Sound
 */
enyo.kind({
	
	name : "EnyoSurveyVideo",
	
	kind : "EnyoSurveyElement",
	
	components : [  {kind: "FittableRows", name : "elementContent", components : []}],
	
	constructor : function(object){ 

		this.inherited(arguments);
	},

	create : function(){	

		this.inherited(arguments);

		this.createComponent({kind: "FittableRows", classes : "survey-video", components : [
			
			{kind: "FittableRows" , name:"videoLayout", classes:"survey-video-layout", components : []},
			{kind: "onyx.Button", classes:"onyx-blue survey-video-button", name:"Image Button", ontap:"buttonTapped", components: [
				{tag: "img", attributes: {src: "assets/ic_video.png"}},
				{content: "Click to take a video "}
			]}
		]})		
	},

	_createVideoComponent : function(src){

	
		var htmlContent = " <video id='video'  width='100%' height='175'  controls autobuffer> "
		htmlContent = htmlContent + '<source src="'+src+'" type="video/mp4">'
		//htmlContent = htmlContent + '<source src="'+src+'" type="video/webm">'
		//htmlContent = htmlContent + '<source src="'+src+'" type="video/ogg">'
		htmlContent = htmlContent + '<source src="'+src+'">'
		htmlContent = htmlContent + '</video>'
 	
 		return {
 			tag : "div",
 			name : "videoContent",
 			allowHtml: "true",
 			content : htmlContent
 		}
	},

	buttonTapped : function(){

		var self = this;

		A.apisurvey.mediaVideo(function(mediaId, mediaUri){
			
			var videoElement = self.$.videoLayout.$.videoContent;
		    if (videoElement){
		 	  videoElement.destroy();
		    }

		    self.mediaId  = mediaId;
		    self.mediaUri = mediaUri
			self.$.videoLayout.createComponent(self._createVideoComponent(mediaUri));
			self.render();
		})
	},

	valid : function(){ 

		if (this.mediaId == undefined){
			this.error("No Video");
			return false;
		}

		return true;
	},

	result : function(){  return {  type : "video/mp4", id : this.mediaId }  },

	resultMedia : function(){ return [ { uri : this.mediaUri, id : this.mediaId }] }
})




