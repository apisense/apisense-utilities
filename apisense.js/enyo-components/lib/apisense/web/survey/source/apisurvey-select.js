
/**
 *
 * Survey Plugin : Close
 * Ease the build of close question
 */

/* build close question with one possible response */
A.apisurvey.plugins.close = function(survey,args){

	var label = args[0] || "..."
	var items = args[1] || ["A","B","C"];
	var options = args[2] || {}
	var component = survey.question();
	component.select(label,items,options);
	return component;
};



/**
 *
 * Form Element Plugin
 * 
 * Create radio button group 
 */
A.apisurvey.form.plugins.select = function(args){
	var element = this.build(args);
	element.kind = "EnyoSurveySelect";
	element.items = args[1] || [];
	if (args[2]){ for (var i in args[2]){element[i] = args[2][i]} }
	return element;
};

enyo.kind({
	name : "EnyoSurveySelect",
	kind : "EnyoSurveyElement",
	components : [  {kind: "FittableRows", name : "elementContent", components : []}],
	constructor : function(object){
		
		this.inherited(arguments);
		this.items = object.items;
	},

	create : function(){	

		this.inherited(arguments);

		var kind = {kind: "Group", name : "radioGroup", highlander: true,onActivate:"groupActivated",classes:"survey-form-element survey-radio", components: []}
		for (var i in this.items){

			var item = this.items[i];
			kind.components.push({ kind:"onyx.Checkbox", name : item, content : item, classes : "survey-radio-element" })
		}

		this.createComponent(kind);
	},

	groupActivated: function(inSender, inEvent) {

		if (inEvent.originator.getActive()) {
			
			var selected = inEvent.originator.indexInContainer();
			this.radioResult = 	this.items[selected];
		}
	},

	valid : function(){

		if (this.radioResult == undefined){
			this.error("Please select one option")
			return false;
		}
		return true;
	},

	result : function(){   return this.radioResult; },


	load : function(response,media){
		
		if (response.value != undefined){
			
			var chexbox = this.$[response.value];
			chexbox.setChecked(true);
			this.radioResult = 	response.value;
		}
	}
})

/**
 *
 * Form Element Plugin
 * 
 * Create radio button group with multiple responses
 */
A.apisurvey.form.plugins.selects = function(args){

	var element = this.build(args);
	element.kind = "EnyoSurveySelects";
	element.items = args[1] || [];
	return element;
};

enyo.kind({
	name : "EnyoSurveySelects",
	kind : "EnyoSurveyElement",
	components : [  {kind: "FittableRows", name : "elementContent", components : []}],
	
	constructor : function(object){
		this.inherited(arguments);
		this.items = object.items;	
		this.itemResults = [];	
	},

	create : function(){	

		this.inherited(arguments);

		var kind = { highlander: true,onActivate:"groupActivated",classes:"survey-form-element survey-radio", components: []}
		for (var i in this.items){

			var item = this.items[i];
			kind.components.push({ kind:"onyx.Checkbox", name : item, content : item, classes : "survey-radio-element" })
		}

		this.createComponent(kind);
	},

	groupActivated: function(inSender, inEvent) {

		// Array Remove - By John Resig (MIT Licensed)
		Array.prototype.remove = function(from, to) {
  			var rest = this.slice((to || from) + 1 || this.length);
  			this.length = from < 0 ? this.length + from : from;
  			return this.push.apply(this, rest);
		};

		var selected = inEvent.originator.indexInContainer();

		if (inEvent.originator.getActive()) this.itemResults.push(this.items[selected])
		else delete this.itemResults.remove(this.itemResults.indexOf(this.items[selected]));


		delete Array.prototype.remove;
	},

	result : function(){ return this.itemResults; },


	load : function(response,media){

		// unset all checkbox
		for (var i in this.items){ this.$[this.items[i]].setChecked(false);}

		// select checkbox from results
		for (var i in response.value){

			this.$[response.value[i]].setChecked(true);
		}

		this.itemResults = response.value;
	}
})