//******************************************************************
//*** Author : Haderer Nicolas
//*** Form plugin enabling slide input
//***
//******************************************************************


A.apisurvey.form.plugins.slider = function(args){

	var element = this.build(args);
	element.kind = "EnyoSurveySlider";
	element.minValue = args[1];
	element.maxValue = args[2];
	return element;
};


enyo.kind({
	name : "EnyoSurveySlider",
	kind : "EnyoSurveyElement",
	components : [  {kind: "FittableRows", name : "elementContent", components : []}],
	constructor : function(object){

		this.inherited(arguments);
		
		this.minValue = object.minValue;
		this.maxValue = object.maxValue;
	
	},
	create : function(){	

		this.inherited(arguments);

		var self = this;

		this.createComponent({kind: "onyx.InputDecorator", classes : "survey-slider-label", components: [ 
			{name : "slideValue"} ,
		]});

		this.createComponent({ name : "Slider", kind: "onyx.Slider", increment : self.calculIncrement(), classes : "survey-slider", onChanging:"sliderChanging"})
	},

	sliderChanging : function(inSender){

		this.slideValue = inSender.getValue();
		this.$.slideValue.setContent(this.calculSlideValue(this.slideValue))
	},
	
	calculSlideValue : function(value){

		var range =  Math.abs(this.maxValue - this.minValue);

		return Math.round((range * value) / 100) + this.minValue; 

	},

	calculIncrement : function(){

		var range =  Math.abs(this.maxValue - this.minValue);
		return 100 / range;

	},

	load : function(response,media){

	
		if (response.value != undefined){
		  this.slideValue = response.value;
		  this.$.Slider.setValue(((response.value-this.minValue) / Math.abs(this.maxValue - this.minValue)) * 100);
		  this.$.slideValue.setContent(this.slideValue);
		}
	},

	result : function(){ return this.calculSlideValue(this.slideValue)}

})