//******************************************************************
//*** Author : Haderer Nicolas
//*** Form plugin enabling text input
//***
//******************************************************************


/**
 *  
 * Form element plugin
 * @example
 *   
 *
 */
A.apisurvey.form.plugins.editText = function(args){
	var element = this.build(args);
	element.kind = "EnyoSurveyEditText";
	return element;
};

A.apisurvey.form.plugins.number = function(args){
	var el = A.apisurvey.form.plugins.editText(args);
		el.editType = "number";
	return el;
}

A.apisurvey.form.plugins.email = function(args){
	var el = A.apisurvey.form.plugins.editText(args);
		el.editType = "email";
	return el;
}

A.apisurvey.form.plugins.url = function(args){
	var el = A.apisurvey.form.plugins.editText(args);
		el.editType = "url";
	return el;
}

A.apisurvey.form.plugins.search = function(args){
	var el = A.apisurvey.form.plugins.editText(args);
		el.editType = "search";
	return el;
}


A.apisurvey.plugins.open = function(survey,args){

	var label = args[0] || "..."

	var component = survey.question();
	component.editText(label);
	return component;
};

enyo.kind({
	name : "EnyoSurveyEditText",
	kind : "EnyoSurveyElement",
	components : [  {kind: "FittableRows", name : "elementContent", components : []}],
	constructor : function(object){

		this.inherited(arguments);
	
	},
	create : function(object){	

		this.inherited(arguments);

		var editType = object.editType || "text";
		
		this.$.elementContent.createComponent(
			{kind: "onyx.InputDecorator", classes : "survey-form-element",components: [
			{kind: "onyx.Input", name : "input",type: editType, classes : "survey-edit", placeholder: "Enter text here", onchange:"inputChanged"}
			]}
		);
	},

	valid : function(){

		var value = this.$.elementContent.$.input.getValue();
		if (value == ""){
			this.error("This field cannot be empty")
			return false;
		}

		return true;
	},

	load : function(response,media){

		this.$.elementContent.$.input.setValue(response.value);
	},

	result : function(){ return this.$.elementContent.$.input.getValue() }
})