

//******************************************************************
//*** Author : Haderer Nicolas
//*** Main Survey Object
//*** Define survey context
//******************************************************************

var Survey = function(){ return new EnyoSurvey(arguments) }

var A = A || {};

A.apisurvey = {};
A.apisurvey.name =  "Not Defined";
A.apisurvey.description = "...";
A.apisurvey.plugins = {}

A.apisurvey.form = {};

A.apisurvey.surveys = {};

// Survey Global Listenner

A.apisurvey.onSubmit = function(survey,responses){ } 

A.apisurvey.onNext = function(survey){ }


//******************************************************************
//*** Author : Haderer Nicolas
//*** Main Enyo Survey Object 
//***
//******************************************************************

enyo.kind({
	name: "EnyoSurvey",
	kind: "FittableRows",
	fit: true,
	touch : false,
	classes : "survey",
	components:[
		{kind: "onyx.Toolbar", name :"SurveyHeader", classes : "survey-head"},
		{kind: "enyo.Scroller", components: [
			
			{name : "surveyDescription", tag : "div", classes : "survey-description"},

			{name: "main", classes: "survey-content",  allowHtml: true, components : [

				{kind: "Panels", name:"formPanel",fit:true, draggable : false, style: "width:100%;height:100%;", components: []}
				
			]},

		    {kind: "FittableRows", classes:"survey-bottom", components : [
		       {kind: "onyx.Button", name : "previousButton", classes : "survey-bottom-button ", content: "Previous", ontap : "previousPanel"},
		       {kind: "onyx.Button", name : "nextButton", style:"margin-left:10px;", classes : "survey-bottom-button onyx-blue", content: "Next", ontap : "nextPanel"},
			   {kind: "onyx.ProgressBar", name:"progessBar", classes:"survey-progess", animateStripes: false, showStripes: false, progress: 0},
			   { allowHtml : true, name : "progessText" }
			]}
		]},
	],

	 constructor : function(){

    	this.inherited(arguments);

    	this.name = A.apisurvey.name;
        this.description = A.apisurvey.description;

        this.forms = [];

    },

    create : function(){

    	this.inherited(arguments);
    	

    	var self = this;

    	for (var i in A.apisurvey.plugins){
			(function(i){
				self[i] = function(){ return A.apisurvey.plugins[i](self,arguments)}
			})(i)
		}

		for (var i in A.apisurvey.form.plugins){
			(function(i){
				self[i] = function(args){
					
					var func = A.apisurvey.form.plugins[i];
					return self.question()[i].apply(self,arguments)
				}	
			})(i)
		}

		

    	this.$.SurveyHeader.content = this.name;
        this.$.surveyDescription.content = this.description;


		this.progessBar = this.$.progessBar;
        this.progessText = this.$.progessText;


        this.renderInto(document.body);  

        A.apisurvey.surveys[this.name] = this;    

    },

	nextPanel: function() {

		var currentForm = this.forms[this.$.formPanel.getIndex()];

		if (!currentForm.valid()){ return; }

		this.progessBar.animateProgressTo(Math.round((currentForm.formIndex + 1) * 100 / this.forms.length))

		if (currentForm.hasNext()){
			
			var id = currentForm.getNext().formIndex;
			this.$.formPanel.setIndex(id);
		}
		else{

			this.submit();
		}

		this.update();

		A.apisurvey.onNext(this);
	},

	previousPanel : function(){
		
		var currentForm = this.forms[this.$.formPanel.getIndex()];

		if (currentForm.hasPrevious()){

			var prevIndex = currentForm.getPrevious().formIndex;
			this.progessBar.animateProgressTo(Math.round((prevIndex ) * 100 / this.forms.length))
			this.$.formPanel.setIndex(prevIndex);
		}

		this.update();
	},

    text : function(html){  return this.question().text(html)},

   /**
	*	
	*	Return current response of the Survey
    *
    */
    saveSurvey : function(){

		// get current form page index
		var currentIndex = this.$.formPanel.getIndex();
		currentResult = [];

		// for all form, get current result
		for (indexForm in this.forms){

			var form = this.forms[indexForm];
			currentResult.push({ 
				responses : form.result(),
				medias    : form.resultMedia()
			});
		}

		var savedSurvey = {  index : currentIndex, results : currentResult };
		
		
		return savedSurvey;
	},

	loadSurvey : function( result ){

		if (!result.index){ return; }

		for (i in result.results){

			var response = result.results[i];
			
			this.forms[i].load(response)
		}

		// go back to the first panel
		var currentForm = this.forms[result.index];
		while( currentForm.hasPrevious()){

			currentForm =  this.forms[this.$.formPanel.getIndex()];
			this.previousPanel();
		}

		// go to index saved result panel
		for (var i=0;i<result.index;i++){
			
			this.nextPanel();
		}
	},
   
    submit : function(){

    	var currentForm = this.forms[this.$.formPanel.getIndex()];
		if (!currentForm.valid()){
			
			return;
		}

		var results = {};
		var resultMedias = [];
		while (currentForm.hasPrevious()){

			// build question result
			var answer = currentForm.result();

			// get question label
			var label = Object.keys(answer);

			// push answer in result
			results[label] = answer[label];

			var medias = currentForm.resultMedia();
			for (var j in medias){
				resultMedias.push(medias[j])
			}

			currentForm = currentForm.getPrevious();
		}

		var answer = currentForm.result();
		var label = Object.keys(answer);
		results[label] = answer[label];
		

		var medias = currentForm.resultMedia();
		for (var z in medias){
			
			resultMedias.push(medias[z])
		}

		A.apisurvey.onSubmit(this,{ survey : results, medias : resultMedias});
    },

    createIndex : function(){
    	
    	this._indexElements = this._indexElements  || 0;
    	this._indexElements = this._indexElements + 1;
    	return this._indexElements;
    },

    question : function(_label){

    	var label = _label || this.forms.length

		var component = this.$.formPanel.createComponent({ kind : "EnyoSurveyForm", formIndex : this.forms.length, label : label });
		component.setSurvey(this);
		
		this.forms.push(component);
		
		this.$.formPanel.render();
    	
    	if (this.forms.length > 1){

			var previousQuestion = this.forms[this.forms.length -2]
			previousQuestion.next(component);
		}

    	this.update();
    	
    	return component;
    },

    first : function(question){

    	this.$.formPanel.setIndex(question.formIndex);
    	this.update();

    },

    update : function(){

    	var currentForm = this.forms[this.$.formPanel.getIndex()];
    	
    	if (currentForm.hasNext()){

    		var button = this.$.nextButton;
    		button.content = "Next";
    		button.addClass("onyx-blue");
    		button.removeClass("onyx-affirmative");
    		button.render();

    	}else{

    		var button = this.$.nextButton;
    		button.content = "Submit";
    		button.removeClass("onyx-blue");
    		button.addClass("onyx-affirmative");
    		button.render();
    	}

    	if (currentForm.hasPrevious()){


    		var button = this.$.previousButton;
    		button.show();


    	}else{

    		var button = this.$.previousButton;
    		button.hide();
    		
    	}

    	this.progessText.content = this.$.formPanel.getIndex()+" / "+this.forms.length
    	this.progessText.render();
    }
});




