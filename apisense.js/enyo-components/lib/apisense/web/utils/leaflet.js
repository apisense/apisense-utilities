enyo.kind({
	name: "apisense.web.utils.leaflet",
	kind: "FittableRows",
	fit: true,
	classes: "enyo-unselectable",

	create : function(object){ this.inherited(arguments);

		this.renderInto(this.id)

		// create leaflet map
		this.leaflet = L.map(this.id).setView([51.505, -0.09], 13);

		// configuration of map provider
		this.mainLayer = L.tileLayer( object.url || 'http://{s}.tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png', {

			maxZoom:  object.maxZoom || 18,
			
			attribution:  this.atribution || ""
		
		})
	},

	show : function(object){


		this.mainLayer.addTo(map);

	},


	drawRect : function(callback){ var self = this;

		var points = [];
		var markers = [];



		var drawCallback = function(event){
			
			points.push(event.latlng);

			var mk = L.marker(event.latlng);
			markers.push(mk);
			mk.addTo(self.leaflet)

			enyo.log("ola")
			if (points.length == 2){

				for (i in markers){
					self.leaflet.removeLayer(markers[i])
				}
				delete markers;


				self.leaflet.off('click',drawCallback);

				var rect = L.rectangle([points[0],points[1]], {color: "#ff7800", weight: 1});
				rect.addTo(self.leaflet);

				if (callback) callback(rect);
			}			
		}

		self.leaflet.on('click',drawCallback);
	},



})