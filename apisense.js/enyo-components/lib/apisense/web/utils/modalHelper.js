enyo.kind({
	name : "apisense.modal.Modal",
    kind: "onyx.Popup", 
    classes: "onyx-sample-popup apisense-modal ", 
    centered: true, modal: true, floating: true, 
    autoDismiss: false,
    components: [
    	
    	{name : "modalHeader", kind : "FittableRows", classes : "apisense-modal-header",components : []},

    	{name : "modalContent", kind : "enyo.Scroller", style:"min-width:400px;", maxHeight:"500px;", classes : "apisense-modal-content",components : []},

	  	{kind : "FittableRows", name : "modalBottom", classes : "apisense-modal-bottom", style:"text-align:center", components : [
	  		{kind: "onyx.Button", content: "Validate",ontap:"valid",style:"margin:5px;"},
	  		{kind: "onyx.Button", content: "Cancel",ontap:"cancel", style:"margin:5px;"}
	  	]}
	],

	constructor : function(object){

		 this.inherited(arguments);

		 this.title = object.title || "Title"
		 this.onValid = object.onValid || function(){};
		 this.onCancel = object.onCancel || function(){};
	},

	create : function(object){  this.inherited(arguments);
		 
		 this.$.modalHeader.createComponent({content : this.title })

		 if (object.message){
		 	
		 	this.$.modalContent.createComponent({ content : object.message, allowHtml : true });
		 }
	},

	setMessage : function(msg){

		this.$.modalContent.setContent(msg)
	},

	setHtml : function(content){

		content["allowHtml"] = true

		this.$.modalContent.createComponent(content)	
	},

	setData : function(data){

		this.data = data;
	},

	valid : function(){ 

		this.onValid(this.data);
		this.hide();this.destroy(); 
	},

	cancel : function(inSender,inEvent){ this.hide();this.destroy(); }
})



enyo.kind({
	name : "apisense.modal.Input",
	kind : "apisense.modal.Modal",

	create : function(object){  

		this.inherited(arguments);

		this.lists = object.lists || [];

		this.input = this.$.modalContent.createComponents([
			{kind: "onyx.InputDecorator", components: [
				{name : "firstInput", kind: "onyx.Input",onchange:"inputChanged"}
			]}
		]);

		this.result = {};

		for (var i in this.lists){

			var content = this.lists[i].name;
			var data = this.lists[i].data;

			var cps = []

			for (var j in data){

				cps.push({content: data[j], index : i})
			}

			this.$.modalContent.createComponent(

				{ kind : "FittableColumns", style : "margin-top:10px;", components : [
					{ content : content, style:"margin-right:5px;padding-top:8px;width:100px;font-size:13px;" },
			 		{kind: "onyx.MenuDecorator", onSelect: "itemSelected", components: [
						{name : "list-"+i,content: data[0]},
						{kind: "onyx.Menu", components:  cps }
					]}
				]}
			)
			this.$.modalContent.render();
			this.result[i] = data[0];
		}

		var self = this;
		this.$.modalContent.itemSelected = function(inSender,inEvent){ 

			var content = inEvent.originator.content;
			var index = inEvent.originator.index
			self.$.modalContent.$["list-"+index].setContent(content);
			
			self.result[index] = content;

		}
	},

	valid : function(){

		this.result["input"] = this.$.modalContent.$.firstInput.getValue();
		this.onValid(this.result);
		this.hide();this.destroy(); 
	}
})


enyo.kind({
	name : "apisense.modal.Form",
	kind : "apisense.modal.Modal",

	create : function(object){ this.inherited(arguments);

		this.items = [];
		this.modalItems = {};
		this.onValid  = object.onValid  || function(){};
		this.onCancel = object.onCancel || function(){};

		this.$.modalContent.itemSelected = this.itemSelected;
	},

	onChange : function(){},

	valid : function(){

		this.onValid(this.getResult());
		this.hide();this.destroy();
	},


	getResult : function(){

		var results = {};
		for (var i in this.items){

			var r = this.items[i]();
			results[r.name] = r.value;
		}
		return results;
	},

	input : function(name,label,defaultValue){

		defaultValue = defaultValue || ""

		this.$.modalContent.createComponent({
			kind : "FittableColumns", style:"padding-bottom:30px;",components : [
			     {content : label, style : "font-weight:bold;padding-top:8px;width : 200px;"},
			     {kind: "onyx.InputDecorator", components: [ {kind: "onyx.Input", style:"width:400px;", name : name, value : defaultValue} ]}
			]}
		)

		var self = this;
		this.items.push(function(){

			return {
				name  : name,
				value : self.$.modalContent.$[name].getValue()
			}
		})
	},

	label : function(name){ this.$.modalContent.createComponent({classes: "onyx-sample-divider", content: name});},

	divider : function(style){ this.$.modalContent.createComponent({ style:style })  } ,

	toggle : function(name,label,defaultValue){ var self = this;

		defaultValue = defaultValue || false;
		var p = this.$.modalContent.createComponents([
			{ kind : "FittableColumns", components : [
				{content : label, style : "font-weight:bold;padding-top:8px;padding-bottom:10px;width:200px;"},
				{kind:"onyx.ToggleButton",name : name, onChange:"toggleChanged", value: defaultValue}
			]}
		])
		
		this.$.modalContent.toggleChanged = function(){

			self.onChange(self,name);
		}



		this.items.push(function(){

			return {
				name  : name,
				value : self.$.modalContent.$[name].getValue()
			}
		})
	},

	checkbox : function(name,label,defaultvalue){ var self = this;

		var p = this.$.modalContent.createComponents([
			{ kind : "FittableColumns", components : [
				{content : label, style : "width:70%;"},
				{kind: "onyx.MenuDecorator", onSelect: "itemSelected",
					components: [
						{kind:"enyo.Checkbox",style:"margin-top:7px;",name : name, checked : defaultvalue, onActivate : "checkboxSelected"}
					]
				}
			]}
		])

		this.$.modalContent.checkboxSelected = function(inSender,inEvent){

			var el = inEvent.originator;
			self.onChange(self,name);
		}

		
		this.items.push(function(){

			return{
				name  : name,
				value : self.$.modalContent.$[name].active
			}
		})
	},

	list : function(name,label,_items, defaultValue){ var self = this;

		defaultValue = defaultValue || ""

		var items = [];
		for (var i in _items){
			items.push({content: _items[i], index : name, style:"min-width:100px;"});
		}

		var p = this.$.modalContent.createComponents([
			{ kind : "FittableColumns", components : [
				{content : label, style : "font-weight:bold;padding-top:8px;padding-bottom:10px;width:100px;"},
				{kind: "onyx.MenuDecorator", onSelect: "itemSelected",
				components: [
					{name : name, style:"width:200px;", content: defaultValue },
					{kind: "onyx.Menu", floating: true, maxHeight: 500,scrolling: true, components: items}
				]}
			]}
		])

		this.$.modalContent.itemSelected = function(inSender,inEvent){

			var el = inEvent.originator;
			self.$.modalContent.$[el.index].setContent(el.content)
			self.onChange(self);
		}

		
		this.items.push(function(){

			return {
				name  : name,
				value : self.$.modalContent.$[name].getContent()
			}
		})
	}
})

