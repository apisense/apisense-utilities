enyo.kind({
	
	name : "apisense.web.utils.Task",

	classes : "notif-task",

	components : [

		{ kind : "FittableColumns",  components : [

			{ name : "taskName", content : "Task Name", classes : "task name"},

				
			{ name : "taskProgression", kind: "onyx.ProgressBar", progress: 25, barClasses: "onyx-green", classes : "task progress"},
			

			{ name : "taskStatus", content : "Status", classes:"task status"},

		]},
		{ name : "taskMessage", content : "message", classes:"task message" }
	],

	create : function(object){ this.inherited(arguments);

	},

	set : function(task){
		
		this.$.taskName.setContent(task.name);
		
		this.$.taskMessage.setContent(task.message);
		this.$.taskStatus.setContent(task.status);
		
		var progress = ((task.progress/task.progressMax)*100)
		if (isNaN(progress)) progress = 0;

		progress = Math.round(progress);

		this.$.taskProgression.animateProgressTo(progress);
		
		if (task.status == "done") this.$.taskProgression.$.bar.applyStyle("background", "green");
		else if (task.status == "error") this.$.taskProgression.$.bar.applyStyle("background", "red");

	}
})