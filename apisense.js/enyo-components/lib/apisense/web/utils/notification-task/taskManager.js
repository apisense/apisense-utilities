enyo.kind({
	
	name : "apisense.web.utils.TaskManager",

	kind: "enyo.Slideable",

	// slide configuration
	value: 0,
	min:  0,
	max : 95, 
	unit: "%",
	axis: "h",
	
	classes : "enyo-fit slideable right",

	components : [
	
		{  kind : "FittableColumns", components : [

			{ kind: "onyx.Grabber", classes : "grabber right", ontap:"toggle" },
			
			{ name : "layout" , classes : "notif-task-layout", components : [ ]}

		] }
	],

	toggle : function(){ this.toggleMinMax() },
	
	create : function(object){ this.inherited(arguments);

		this.taskMap = {};
	},

	put : function(task){


		if (!this.taskMap[task.id]){
			// create new task
			this.taskMap[task.id] = this.$.layout.createComponent({  kind : "apisense.web.utils.Task"})
			this.render();
		}
		
		this.taskMap[task.id].set(task);
	}
});

		
	
