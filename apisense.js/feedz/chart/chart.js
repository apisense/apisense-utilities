var charts = {};

var subs = function(e){
    e.data[0] = parseInt(e.data[0]);
    e.data[1] = parseInt(e.data[1]);
    charts["chart-"+e.chartId].series[e.serieId].addPoint(e.data);
}


var createChart = function(id, options, height){

    
  $('body').append('<div id="'+id+'" style="height:'+height+'%" ></div>');
  
  
  options = options || {};
  options.title   = options.title || "Chart Title"
  options.ytitle  = options.ytitle || "Y Axis"
  options.ymax    = options.ymax || 100
  options.ymin    = options.ymin || 0
  options.series  = options.series;
  
  options.series.forEach(function(serie){
            
      var dataSeries = [];
      var d = JSON.parse($db.experimentMemory(serie.tableId,"")); 
      d.data.forEach(function(value){
         
          serie.data.push([value.x, value.y]);
      });
      
  })
   
  charts[id] = new Highcharts.Chart({
         chart: { renderTo: id,zoomType: 'x',spacingRight: 20 },   
         plotOptions: {  marker: { enabled: false } },
         title: { text: options.title},
         xAxis: { type: 'datetime',title: {text: null}},
         yAxis: {title: {text: options.ytitle}, min : options.ymin, max : options.ymax},
         legend: { enabled: true },
         series: options.series
  });
}

$(function(){
   
    console.log("load chart")

    $('body').append("<center><h4> "+options.title+" </h4></center>");
    
    
    var height = Math.round(90 / options.charts.length);
    options.charts.forEach(function(option){
        console.log("create chart");
        console.log(JSON.stringify(option));
        createChart("chart-"+option.id,option,height);
    }); 
    
    $apisense.subscribe("chartData","subs");

    
    //var datas = $db.experimentMemory("chartFeedz","");
    //datas = JSON.parse(datas);
    
    //datas.data.forEach(function(data){ 
    //  var entry = JSON.parse(data.data);
    //  subs(entry);
    //})
    
    
});