F={create:function(){function e(){}return function(t){e.prototype=t;return new e}}(),extend:function(e){var t=Array.prototype.slice.call(arguments,1),n,r,i,s;for(r=0,i=t.length;r<i;r++){s=t[r];for(n in s){e[n]=s[n]}}return e},Class:function(){}};F.Class.extend=function(e){var t=function(){if(this.initialize){this.initialize.apply(this,arguments)}if(this._initHooks.length){this.callInitHooks()}};var n=t.__super__=this.prototype;var r=F.create(n);r.constructor=t;t.prototype=r;for(var i in this){if(this.hasOwnProperty(i)&&i!=="prototype"){t[i]=this[i]}}if(e.statics){F.extend(t,e.statics);delete e.statics}if(e.includes){F.extend.apply(null,[r].concat(e.includes));delete e.includes}if(r.options){e.options=F.extend(F.create(r.options),e.options)}F.extend(r,e);r._initHooks=[];r.callInitHooks=function(){if(this._initHooksCalled){return}if(n.callInitHooks){n.callInitHooks.call(this)}this._initHooksCalled=true;for(var e=0,t=r._initHooks.length;e<t;e++){r._initHooks[e].call(this)}};return t}


var FeedzChart = function(title,description){ return new F.ChartFeedz(title,description); }

F.ChartFeedz = F.Class.extend({
    
    initialize : function(title,description){
        
        this.title = title;
        this.description = description;
        
        this.db = $memorydb;
        this.db.init();
        
        this.options = [];
    },
    
    create : function(chartOptions){
        
         this.chartOptions = chartOptions || {};
         this.chartOptions.series = [];
         this.chartOptions.zoomType = 'x';
         
         this.chartId = this.options.length;
         this.chartOptions.id = this.chartId;

         this.options.push(this.chartOptions);
         
         return new F.SerieFactory(this.chartOptions,this.db);
    },
    
    publish : function(){
        
        var jquery     = "http://codeorigin.jquery.com/jquery-2.0.3.min.js";
        var hightchart = "http://code.highcharts.com/highcharts.js";   
      
        $feedz.publishFeedz(this.title,this.description,{
        index : "chart/chart.html",
        js_local : ["chart/chart.js"],
        js : [jquery,hightchart],
        options : {  
            title  : this.title,
            charts :  this.options 
        }
     });
    }
});

F.SerieFactory = F.Class.extend({
    
    initialize : function(chartOptions,db){
        this.db = db;
        this.chartOptions = chartOptions;
    },
    
    _create : function(serieOptions,clazz){
        
        serieOptions.serieId = this.chartOptions.series.length
        serieOptions.chartId = this.chartOptions.id
        
        var serie = new clazz(serieOptions,this.db);
        this.chartOptions.series.push(serieOptions);
        return serie;
    },
    
    serie : function(serieOptions){ return this._create(serieOptions,F.Serie) },
    area  : function(serieOptions){ return this._create(serieOptions,F.SerieArea) }
});


F.Serie = F.Class.extend({
    
    initialize : function(serieOption, db){
            
        this.db = db;
        serieOption = serieOption || {};
        serieOption.name = serieOption.name || "name";
        serieOption.data = serieOption.data || [];
        
        this.serieId = serieOption.serieId;
        this.chartId = serieOption.chartId;
        
        this.serieTableName = "table"+this.chartId+this.serieId;
        this.serieTable     = this._createTable(this.serieTableName);
    
        this._configureSerieOption(serieOption);
    
        serieOption.tableId = String(this.serieTableName);
    },
    
    _configureSerieOption : function(serieOption){
        
        //serieOption.type = "area";
        
    },
    
    _createTable : function(tableName){
        var duration = 30*60*1000;
        return this.db.create({
            name : String(tableName),
            version : 4,
            type : "time",
            time : duration,
            schema : {
                x : "number",
                y : "number"
            }
        });
    },
    
    add : function(datas){
        
        var e = {
            data    : datas,
            chartId : this.chartId,
            serieId : this.serieId,
            tableSerie : this.serieTableName,
            name : "chartData"
        };

        var _d = { x : datas[0], y : datas[1] };
        
        $eventStream.publishJSON(e);
        this.serieTable.insert(JSON.stringify(_d));
    }
});

F.SerieArea = F.Serie.extend({
    _configureSerieOption : function(serieOption){
        serieOption.type = "area";
    }
});








