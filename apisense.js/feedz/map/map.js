
pingCirlce = L.CircleMarker.extend({
   options: { 
      ping: '',
      signal: ''
   }
});

var map,networks,colors, init = undefined;
$(function(){

  colors = generateGradiantArray("#98ff00","#cc0000",70);
  init = false;
  
  networks = {};
  var queryResult = JSON.parse($db.experiment("PingTable","","group by network ")).data;
  for (var i in queryResult){
  
    var network = queryResult[i].network
    networks[i] = network;
    
    $("#network").append(' <div><input class="entry" type="radio" name="group" value="'+i+'"> '+network+'</input> <br/></div>')
    $("#network input[type='radio']").filter('[value='+i+']').prop('checked', true);
   
  }
  
  $("#network input[type='radio']").click(function(){update()})
  
  $('#network input:radio').filter('[value=0]').prop('checked', true);
   
  // create a map in the "map" div, set the view to a given place and zoom
  map = L.map('map').setView([50.6214, 3.112], 13);
  
  // add an OpenStreetMap tile layer
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
   	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);
 
  update();
})

var markers = [];
var markersLayer = undefined;
var network = {}


var update = function(){
  
  $("#stat").empty();
  
  var network = networks[$("#network input[type='radio']:checked").val()];

  if (markersLayer){
  	console.log("remove layer")
    map.removeLayer(markersLayer);
    delete markersLayer;
    delete markers;
    markers = [];
  }
  
  var data = JSON.parse($db.experiment("PingTable"," network = \""+network+"\" "))
  
  markersLayer = new L.MarkerClusterGroup({
    
    spiderfyOnMaxZoom: true, 
    
    showCoverageOnHover: true,
    
    zoomToBoundsOnClick: false,
  	
  	iconCreateFunction: function(cluster) {
      
      
      var markers = cluster.getAllChildMarkers();
      var signal = 0;
      var ping   = 0;
      
      for (var i in markers){
          var marker = markers[i];
        	
          signal = signal + marker.options.signal;
          ping   = ping + marker.options.ping;
          
      }
      
      signal = Math.floor(signal / markers.length);
      ping = Math.floor(ping / markers.length);
      
      var color = getColor(ping);
      var size = signal * 5;
      
      var popup =  "<div class='scluster-pop'>"
      popup = popup + "<strong>Childs number : </strong>"+cluster.getChildCount()+"<br/>"
      popup = popup + "<strong>Signal average : </strong>"+signal+" db<br/>"
      popup = popup + "<strong>Latency Average : </strong>"+ping+" ms<br/>"
      popup = popup + "</div>"
      
      cluster.on("mouseover",function(event){ $("#map").append(popup); })
      cluster.on("mouseout",function(event){ $("#map").append(popup).find(".scluster-pop").remove() })
      
      return new L.DivIcon({
        html : "<div  class='scluster' style='background:"+color+"'>"+markers.length+"</div>",
        className : "",
        iconSize  : new L.Point(size,size) 
      });
    }
  })
 
  
  data = data.data
  for (var i in data){
      
    var entry = data[i];
    var lat = entry.lat;
    var lon = entry.lon;
    var signal = entry.signal;     
    var ping = entry.ping;   
    var color = getColor(Math.floor(entry.ping));
    
   
    var circle = L.circle([lat,lon],signal*2, {
    	color: ""+color+"",
        fillColor: ""+color+"",
        signal : signal,
        ping   : ping  
    });
    circle.bindPopup("signal : "+signal+" <br/> latency : "+entry.ping);
    
    //var circle = L.marker(L.latLng([lat,lon]), { title: "hello" });
    
    markers.push(circle);
    markersLayer.addLayer(circle);
  }
  $("#stat").append("<strong>Data : </strong>"+data.length);
  
  //markersLayer = L.featureGroup(markers);
  markersLayer.addTo(map);
  
  if (init == false){
  
  	map.fitBounds(markersLayer.getBounds());
    init=true;
  }
}


var generateGradiantArray = function( color1, color2, state ){
                
   var c1 = new RGBColor(color1)
   var c2 = new RGBColor(color2)
   state = state || 4
                
   var red = ((c2.r - c1.r) / (state))
   var green = ((c2.g - c1.g) / (state))
   var blue = ((c2.b - c1.b) / (state))
                
   var colors = []
   for (var i = 0;i< state;i++){
                        
     var nr = Math.round(c1.r + red   * i) ;
     var ng = Math.round(c1.g + green * i) ;
     var nb = Math.round(c1.b + blue  * i) ;
                        
     colors[i] =  "rgba("+nr+","+ng+","+nb+", 0.8)"
   }
   return colors;
}


var getColor = function(level){
                
     if (level >= colors.length) 
         return colors[this.colors.length -1]
     else{
         return colors[level];
     }
}
