var jquery     = "http://codeorigin.jquery.com/jquery-2.0.3.min.js";
var hightchart = "http://code.highcharts.com/highcharts.js";
var leaflet    = "http://cdn.leafletjs.com/leaflet-0.5/leaflet.js";
var leafletcss = "http://cdn.leafletjs.com/leaflet-0.5/leaflet.css";
var leafletCluster = "http://metronet1.inrialpes.fr/js/leaflet/leaflet.markercluster-src-v2.js"
var leafletClusterCss = "http://www.apisense.fr/js/leaflet/MarkerCluster-v2.css"
var colorUtil  = "http://static.apisense.fr/g/js/rgb-color.js";

$view.declare("SignalMap","signal map",{
  index : "view/map/map_signal.html",
  js_local : ["view/map/map_signal.js"],
  js : [jquery,leaflet,colorUtil,leafletCluster],
  css : [leafletcss,leafletClusterCss],
  css_local : ["view/map/map_signal.css"]
})

