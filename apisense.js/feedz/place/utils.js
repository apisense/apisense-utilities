/**
 * Helper which add rectangle from a virtual sensor definition
 */
var drawRect = function(vs){
      var base    =  [vs.slon,vs.slat];
		  current =  [vs.slon,vs.slat];
		  end     =  [vs.elon,vs.elat];
          result  = [];
          
        var global =  L.rectangle([ [current[1],current[0]] , [end[1],end[0]]], {color: "	black", weight: 10});
			  global.addTo(map);
        result.push(global);
        
		while( current[0] < end[0] ){

			while( current[1] - vs.cellH > end[1] ){

				var p =  L.rectangle([ [current[1],current[0]], [current[1] - vs.cellH,current[0]+vs.cellW]], 
					{color: "#ff7800", weight: 1});
                
                result.push(p);
                
          		current[1] = current[1] - vs.cellH;
			}

		    current[1] = base[1];
		    current[0] = current[0] + vs.cellW;
	    }
	    
	    return result;
}


