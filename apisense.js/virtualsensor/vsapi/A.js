A={create:function(){function e(){}return function(t){e.prototype=t;return new e}}(),extend:function(e){var t=Array.prototype.slice.call(arguments,1),n,r,i,s;for(r=0,i=t.length;r<i;r++){s=t[r];for(n in s){e[n]=s[n]}}return e},Class:function(){}};A.Class.extend=function(e){var t=function(){if(this.initialize){this.initialize.apply(this,arguments)}if(this._initHooks.length){this.callInitHooks()}};var n=t.__super__=this.prototype;var r=A.create(n);r.constructor=t;t.prototype=r;for(var i in this){if(this.hasOwnProperty(i)&&i!=="prototype"){t[i]=this[i]}}if(e.statics){A.extend(t,e.statics);delete e.statics}if(e.includes){A.extend.apply(null,[r].concat(e.includes));delete e.includes}if(r.options){e.options=A.extend(A.create(r.options),e.options)}A.extend(r,e);r._initHooks=[];r.callInitHooks=function(){if(this._initHooksCalled){return}if(n.callInitHooks){n.callInitHooks.call(this)}this._initHooksCalled=true;for(var e=0,t=r._initHooks.length;e<t;e++){r._initHooks[e].call(this)}};return t}


A.distance2 = function (lat1, lon1, lat2, lon2) {

        var R = 6371; // km
        var dLat = (lat2-lat1).toRad();
        var dLon = (lon2-lon1).toRad();
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c;
};

A.distance = function (lat1, lon1, lat2, lon2) {

      var radlat1 = Math.PI * lat1/180
      var radlat2 = Math.PI * lat2/180
      var radlon1 = Math.PI * lon1/180
      var radlon2 = Math.PI * lon2/180

      var theta = lon1-lon2
      var radtheta = Math.PI * theta/180

      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

      dist = Math.acos(dist)
      dist = dist * 180/Math.PI
      dist = dist * 60 * 1.1515
      // transform distance in kilometer
      dist = dist * 1.609344 

      //if (unit=="N") { dist = dist * 0.8684 }

      return dist
}

A.rectangle =  function(bound){ return {   
        x       : bound.x,
        y       : bound.y,
        width   : bound.width,
        height  : bound.height,
        inBound : function(x,y){
          return(
            (x > this.x)                         &
            (x < this.x + this.width)    &
            (y < this.y)                             &
            (y > this.y - this.height))
    
        },
        distN : function(point){return A.distance(point.y,point.x,this.y,point.x)},
        distE : function(point){return A.distance(point.y,point.x,point.y,this.x + this.width)},
        distS : function(point){return A.distance(point.y,point.x,this.y - this.height,point.x)},
        distW : function(point){return A.distance(point.y,point.x,point.y,this.x)},
        distSeg : function(point){
          var dist = this.distN(point);
          tmp = this.distE(point);
          if (tmp <= dist){ dist = tmp }
          tmp = this.distW(point);
          if (tmp <= dist){ dist = tmp }
      
          tmp = this.distS(point);
          if (tmp <= dist){ dist = tmp }
      
          return dist;
        }
      }
}

A.uuid = function(){
    
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}



A.stringify = function(obj){
        return JSON.stringify(obj,function(key, value){
          return (typeof value === 'function' ) ? value.toString() : value;
        });
}

A.parse = function(str){
        return JSON.parse(str,function(key, value){
          if(typeof value != 'string') return value;
          if (value[0] == "\n") value = value.substring(1)
          if (value[0] == " ") value = value.substring(1)
          return ( value.substring(0,8) == 'function') ? eval('('+value+')') : value;
})}

A.binding = function(userIds,events,property){ crowd.user(userIds,events,property); }

A.sync = function(funk){

  return new Packages.org.mozilla.javascript.Synchronizer(funk)
}

module.exports = A;




