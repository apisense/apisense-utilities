require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
A={create:function(){function e(){}return function(t){e.prototype=t;return new e}}(),extend:function(e){var t=Array.prototype.slice.call(arguments,1),n,r,i,s;for(r=0,i=t.length;r<i;r++){s=t[r];for(n in s){e[n]=s[n]}}return e},Class:function(){}};A.Class.extend=function(e){var t=function(){if(this.initialize){this.initialize.apply(this,arguments)}if(this._initHooks.length){this.callInitHooks()}};var n=t.__super__=this.prototype;var r=A.create(n);r.constructor=t;t.prototype=r;for(var i in this){if(this.hasOwnProperty(i)&&i!=="prototype"){t[i]=this[i]}}if(e.statics){A.extend(t,e.statics);delete e.statics}if(e.includes){A.extend.apply(null,[r].concat(e.includes));delete e.includes}if(r.options){e.options=A.extend(A.create(r.options),e.options)}A.extend(r,e);r._initHooks=[];r.callInitHooks=function(){if(this._initHooksCalled){return}if(n.callInitHooks){n.callInitHooks.call(this)}this._initHooksCalled=true;for(var e=0,t=r._initHooks.length;e<t;e++){r._initHooks[e].call(this)}};return t}


A.distance2 = function (lat1, lon1, lat2, lon2) {

        var R = 6371; // km
        var dLat = (lat2-lat1).toRad();
        var dLon = (lon2-lon1).toRad();
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c;
};

A.distance = function (lat1, lon1, lat2, lon2) {

      var radlat1 = Math.PI * lat1/180
      var radlat2 = Math.PI * lat2/180
      var radlon1 = Math.PI * lon1/180
      var radlon2 = Math.PI * lon2/180

      var theta = lon1-lon2
      var radtheta = Math.PI * theta/180

      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

      dist = Math.acos(dist)
      dist = dist * 180/Math.PI
      dist = dist * 60 * 1.1515
      // transform distance in kilometer
      dist = dist * 1.609344 

      //if (unit=="N") { dist = dist * 0.8684 }

      return dist
}

A.rectangle =  function(bound){ return {   
        x       : bound.x,
        y       : bound.y,
        width   : bound.width,
        height  : bound.height,
        inBound : function(x,y){
          return(
            (x > this.x)                         &
            (x < this.x + this.width)    &
            (y < this.y)                             &
            (y > this.y - this.height))
    
        },
        distN : function(point){return A.distance(point.y,point.x,this.y,point.x)},
        distE : function(point){return A.distance(point.y,point.x,point.y,this.x + this.width)},
        distS : function(point){return A.distance(point.y,point.x,this.y - this.height,point.x)},
        distW : function(point){return A.distance(point.y,point.x,point.y,this.x)},
        distSeg : function(point){
          var dist = this.distN(point);
          tmp = this.distE(point);
          if (tmp <= dist){ dist = tmp }
          tmp = this.distW(point);
          if (tmp <= dist){ dist = tmp }
      
          tmp = this.distS(point);
          if (tmp <= dist){ dist = tmp }
      
          return dist;
        }
      }
}

A.uuid = function(){
    
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}



A.stringify = function(obj){
        return JSON.stringify(obj,function(key, value){
          return (typeof value === 'function' ) ? value.toString() : value;
        });
}

A.parse = function(str){
        return JSON.parse(str,function(key, value){
          if(typeof value != 'string') return value;
          if (value[0] == "\n") value = value.substring(1)
          if (value[0] == " ") value = value.substring(1)
          return ( value.substring(0,8) == 'function') ? eval('('+value+')') : value;
})}

A.binding = function(userIds,events,property){ crowd.user(userIds,events,property); }

A.sync = function(funk){

  return new Packages.org.mozilla.javascript.Synchronizer(funk)
}

module.exports = A;





},{}],"GYRR2z":[function(require,module,exports){
var A = require("../A.js");



var device = A.Class.extend({


	initialize : function(host){ var self = this;

		this.nbTask = 0;

		this.score = 0;

		String.prototype.endsWith = function(str){
    		var lastIndex = this.lastIndexOf(str);
    		return (lastIndex != -1) && (lastIndex + str.length == this.length);
		}

		$context.onPreFinish(function(){

			self._handlerOutCell();
		});


		this.service = $apis.nodeUrl().substring(0,$apis.nodeUrl().indexOf('/'));
		if (host){
			//log.toast("has host "+host);
			this.service = host;
		}
		this.service = this.service + "/socko/" +  $apis.experimentName();

		this.task 		= undefined;
		this.virtualNode = undefined;
		this._subsTimout = undefined;
	},

	_handleAlertRequest : function(){

		if (!(typeof(virtualNode) === 'undefined')){

			this.onCellChanged(virtualNode);
		}
	},

	/**
	 * Handler called when virtual sensor try to assign
	 * a sensing task to device.
	 */
	_handlerTaskRequest : function(request){

		var _recruit = A.parse(request.recruit)
    	var taskAccepted = false;
    	var properties = undefined;



		if ((this.task == undefined)){

			properties = _recruit.recruit(this);
			if (properties != undefined){

				taskAccepted = true;
				properties.score = this.score;	
			}
    	}

    	this.virtualNode.aggregate_id = request.aggregate_id;
    	
      	$apis.publishJSON({ 
          	eventName : "RequestTaskResponse",
          	requestId : request.requestId,
          	user : {  
            	id : $apis.userId(),
            	taskAccepted  : taskAccepted,
            	properties : properties
            }
        });
	},

	_handlerEndTask : function(){

		if (typeof(this.task) =='undefined'){
			log.d("no running task");
			return;
		} 


		if (typeof(this._subsTimout) !='undefined'){
			this._subsTimout.cancel();	
			this._subsTimout = undefined;
		}

		var _score = this.task.score(this.task,this.score);
		if (_score != undefined){

			this.score = _score;
		}

		$apis.publishJSON({ 
          	name   		 : "RequestTaskResult",
          	aggregate_id : this.virtualNode.aggregate_id,
          	taskId       : this.task.id,
          	isCompleted  : this.task.isCompleted,
          	data		 : this.task.data
        });

		this.task.postTask();
        $asl.stopTask("sensingTask");

		this.task = undefined;
	},

	/**
	 * Handler called when virtual sensor
	 * assign the sensing task to this mobile device
	 *
	 */
	_handlerStartTaskExecution : function(event){ var self = this;

		this.task 			   		 = A.parse(event.task);
		this.task.startExecutionAt   = android.time();
		this.task.cellAddr	   		 = this.virtualNode.cellAddr;
		this.task.data		   		 = [];
		this.task.isCompleted 		 = false;

		this.nbTask = this.nbTask + 1;

		this.task.preTask();

		$asl.startTask("sensingTask","task",{

			complete : function(data){ 
				
				self.task.isCompleted = true;
				if (data) self.task.data.push(data);
				self._handlerEndTask(); 
			},

			add : function(data){
				self.task.data.push(data);
			},

			number : this.nbTask
		});

		this._subsTimout = $schedule.at(this.task.stopAt,function(){
			
			self._subsTimout = undefined;
			self.task.isCompleted = true;
         	self._handlerEndTask();
      	});
	},

	start : function(){ var self = this;

		$feedz.load("http://static.apisense.fr/g/feedz/place",function(){

			var place = new PlaceMap();
			place.publish();
		})

		
		// download virtual sensor distribution
		http.post(self.service+"/container/resolver",{},
			function(result){    

				
				var resolver = A.parse(result);

				var distribution = { 
					name : $apis.experimentName(), 
					lat1 : resolver.bound.lat1, 
					lon1 : resolver.bound.lon1, 
					lat2 : resolver.bound.lat2, 
					lon2 : resolver.bound.lon2, 
					split: resolver.cellScale
				}

				$place.register(distribution,function(e){self.onCellChanged(e);});
			},
			function(err){

    			log.e(err);
    			log.toast(err);
    	});


    	$eventStream.subscribe(function(e){self._handlerTaskRequest(e)},"eventName = TaskRequest");
    	$eventStream.subscribe(function(e){self._handlerStartTaskExecution(e)},"eventName = TaskExecutionRequest");
    	$eventStream.subscribe(function(e){self._handleAlertRequest(e)},"eventName = NotificationRequest");
	},

	//"time":1395427973044,
	//"distance":13.84814453125,
	//"cellSize":871.7986,
	//"name":"VirtualSensorEvent",
	//"state":"enter",
	//"vsName":"SanFran",
	//"probability":0.830347,
	//"cellName":"SanFran-3_3",
	//"accuracy":20.96,
	//"cellAddr":["3","3"]}
	onCellChanged : function(event){ var self = this;

		if ((event.state == "enter") || (event.state == "stay")) {

			if (typeof(this.virtualNode) == 'undefined'){
			
				this.virtualNode =  event;

				$apis.publishJSON({
					name   : "Registration",
					vs     : event.cellAddr,
					id : $apis.userId(),
					properties : {}
				});
			}

		}
		else if (event.state == "exit"){

			this._handlerOutCell();
		}
	},

	_handlerOutCell : function(){

		this._handlerEndTask();

		$apis.publishJSON({
			name   : "Unregistration",
			vs     : this.virtualNode.cellAddr,
			id : $apis.userId(),
			properties : {}
		})

		this.virtualNode = undefined;
	}

});

module.exports.device = function(host){ return new device(host); }
module.exports.A = A;


},{"../A.js":1}],"publicsensing":[function(require,module,exports){
module.exports=require('GYRR2z');
},{}]},{},[])