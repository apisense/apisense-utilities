var A = require("../A.js");



var device = A.Class.extend({


	initialize : function(host){ var self = this;

		this.nbTask = 0;

		this.score = 0;

		String.prototype.endsWith = function(str){
    		var lastIndex = this.lastIndexOf(str);
    		return (lastIndex != -1) && (lastIndex + str.length == this.length);
		}

		$context.onPreFinish(function(){

			self._handlerOutCell();
		});


		this.service = $apis.nodeUrl().substring(0,$apis.nodeUrl().indexOf('/'));
		if (host){
			//log.toast("has host "+host);
			this.service = host;
		}
		this.service = this.service + "/socko/" +  $apis.experimentName();

		this.task 		= undefined;
		this.virtualNode = undefined;
		this._subsTimout = undefined;
	},

	_handleAlertRequest : function(){

		if (!(typeof(virtualNode) === 'undefined')){

			this.onCellChanged(virtualNode);
		}
	},

	/**
	 * Handler called when virtual sensor try to assign
	 * a sensing task to device.
	 */
	_handlerTaskRequest : function(request){

		var _recruit = A.parse(request.recruit)
    	var taskAccepted = false;
    	var properties = undefined;



		if ((this.task == undefined)){

			properties = _recruit.recruit(this);
			if (properties != undefined){

				taskAccepted = true;
				properties.score = this.score;	
			}
    	}

    	this.virtualNode.aggregate_id = request.aggregate_id;
    	
      	$apis.publishJSON({ 
          	eventName : "RequestTaskResponse",
          	requestId : request.requestId,
          	user : {  
            	id : $apis.userId(),
            	taskAccepted  : taskAccepted,
            	properties : properties
            }
        });
	},

	_handlerEndTask : function(){

		if (typeof(this.task) =='undefined'){
			log.d("no running task");
			return;
		} 


		if (typeof(this._subsTimout) !='undefined'){
			this._subsTimout.cancel();	
			this._subsTimout = undefined;
		}

		var _score = this.task.score(this.task,this.score);
		if (_score != undefined){

			this.score = _score;
		}

		$apis.publishJSON({ 
          	name   		 : "RequestTaskResult",
          	aggregate_id : this.virtualNode.aggregate_id,
          	taskId       : this.task.id,
          	isCompleted  : this.task.isCompleted,
          	data		 : this.task.data
        });

		this.task.postTask();
        $asl.stopTask("sensingTask");

		this.task = undefined;
	},

	/**
	 * Handler called when virtual sensor
	 * assign the sensing task to this mobile device
	 *
	 */
	_handlerStartTaskExecution : function(event){ var self = this;

		this.task 			   		 = A.parse(event.task);
		this.task.startExecutionAt   = android.time();
		this.task.cellAddr	   		 = this.virtualNode.cellAddr;
		this.task.data		   		 = [];
		this.task.isCompleted 		 = false;

		this.nbTask = this.nbTask + 1;

		this.task.preTask();

		$asl.startTask("sensingTask","task",{

			complete : function(data){ 
				
				self.task.isCompleted = true;
				if (data) self.task.data.push(data);
				self._handlerEndTask(); 
			},

			add : function(data){
				self.task.data.push(data);
			},

			number : this.nbTask
		});

		this._subsTimout = $schedule.at(this.task.stopAt,function(){
			
			self._subsTimout = undefined;
			self.task.isCompleted = true;
         	self._handlerEndTask();
      	});
	},

	start : function(){ var self = this;

		$feedz.load("http://static.apisense.fr/g/feedz/place",function(){

			var place = new PlaceMap();
			place.publish();
		})

		
		// download virtual sensor distribution
		http.post(self.service+"/container/resolver",{},
			function(result){    

				
				var resolver = A.parse(result);

				var distribution = { 
					name : $apis.experimentName(), 
					lat1 : resolver.bound.lat1, 
					lon1 : resolver.bound.lon1, 
					lat2 : resolver.bound.lat2, 
					lon2 : resolver.bound.lon2, 
					split: resolver.cellScale
				}

				$place.register(distribution,function(e){self.onCellChanged(e);});
			},
			function(err){

    			log.e(err);
    			log.toast(err);
    	});


    	$eventStream.subscribe(function(e){self._handlerTaskRequest(e)},"eventName = TaskRequest");
    	$eventStream.subscribe(function(e){self._handlerStartTaskExecution(e)},"eventName = TaskExecutionRequest");
    	$eventStream.subscribe(function(e){self._handleAlertRequest(e)},"eventName = NotificationRequest");
	},

	//"time":1395427973044,
	//"distance":13.84814453125,
	//"cellSize":871.7986,
	//"name":"VirtualSensorEvent",
	//"state":"enter",
	//"vsName":"SanFran",
	//"probability":0.830347,
	//"cellName":"SanFran-3_3",
	//"accuracy":20.96,
	//"cellAddr":["3","3"]}
	onCellChanged : function(event){ var self = this;

		if ((event.state == "enter") || (event.state == "stay")) {

			if (typeof(this.virtualNode) == 'undefined'){
			
				this.virtualNode =  event;

				$apis.publishJSON({
					name   : "Registration",
					vs     : event.cellAddr,
					id : $apis.userId(),
					properties : {}
				});
			}

		}
		else if (event.state == "exit"){

			this._handlerOutCell();
		}
	},

	_handlerOutCell : function(){

		this._handlerEndTask();

		$apis.publishJSON({
			name   : "Unregistration",
			vs     : this.virtualNode.cellAddr,
			id : $apis.userId(),
			properties : {}
		})

		this.virtualNode = undefined;
	}

});

module.exports.device = function(host){ return new device(host); }
module.exports.A = A;

