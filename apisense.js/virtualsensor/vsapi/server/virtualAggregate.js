var A = require("../A.js");
var jlinq = require("../jlinq.js");

// 

Aggregate = A.Class.extend({

	initialize : function(id,config){ var self = this;

		this.id = id || A.uuid();

		config = config || {};
		this.onCompleted = config.onCompleted || function(){ log.d("task is completed"); }
		this.onAggregate = config.onAggregate || function(info,data){ 
			log.d("task aggregate");
			log.d(data);
			log.d(info);
			log.d("---");
			return data;  
		}

		this.completedTasks = {};


		$eventStream.subscribe(function(event){ self._handleData(event); }," &(name = RequestTaskResult)( aggregate_id = "+this.id+" )");

		$db.remove("__info__",{ aggregate_id : this.id });
	},

	_handleData : function(data){
		
		this.add({
			id: data.taskId,
			isCompleted : data.isCompleted,
			data : data.data
		});
	},


	/**
	 * Regsiter a task to aggregator service
	 *
	 * @param {JSON}     task 			Task to register
	 * @param {String}   task.id 			Unique Identifier of sensing task
	 * @param {Number}   task.duplicate 	Number of devices recruited in the task
	 * @param {JSON} 	 task.attributes 	Attributes of task aggregation
	 */
	registerTask : function(task){


		$db.add( "__info__",{

			aggregate_id 	 : this.id,
			task_id		 	 : task.id,
			attributes	 	 : task.attributes,
			task_duplicate	 : task.duplicate,
			nodes			 : 0,
			remaining		 : task.duplicate,
			isCompleted		 : false,
			data  			 : []
		});
	},

	/**
	 * Add data 
	 * @param {JSON} 	task				Task description to associate data
	 * @param {String} 	task.id					Unique Identifier of sensing task
	 * @param {Boolean} task.completed			True if task is completed
	 * @param {JSON[]}	task.data				Array of JSON value to insert
	 */
	add : function(task){ 

		var self = this;
		var task = task;

		var selectQuery = { aggregate_id : this.id, task_id : task.id };
		var updateQuery = { $pushAll : { data :  task.data }, $inc : { nodes : 1} }

		if (task.isCompleted == true){

			updateQuery["$inc"] = { remaining : -1 };
		}

		$db.update("__info__",selectQuery,updateQuery,false,false,function(){
			
			self.checkAggregate(task.id);   
		})
	},

	checkAggregate : function(taskId){ var self = this;
		
		var selectQuery = { aggregate_id : this.id, task_id : taskId };

		$db.getA("__info__",selectQuery, {remaining:1,isCompleted:1},function(data){

			if (data.length == 0){ 
				log.d("Task not registered "+selectQuery); 
				return; 
			}

			data = data[0];

			// task is not finished
			if (data.remaining > 0) return;

			$context.sync(function(){

				if ((data.isCompleted == false)			&&
					(!(self.completedTasks[taskId]))	&&
					   self.completedTasks[taskId] != data.remaining) {

					self.completedTasks[taskId] = data.remaining;

					self.onCompleted(taskId);
			
					$db.update("__info__",selectQuery,{ $set : {isCompleted : true}},false,false,function(){});

					self.aggregate(taskId,true);
				}else{

					if (self.completedTasks[taskId] != data.remaining){
						self.completedTasks[taskId] = data.remaining;
					
						self.aggregate(taskId,false);
					}
				}
			});
		});
	},

	end : function(taskid){ var self = this;

		var selectQuery = { aggregate_id : this.id, task_id : taskid };
		$db.update("__info__",selectQuery,{ $set : {completed : true} },false,false,function(){

			//self.onCompleted();
			self.aggregate(taskid,true);

		});
	},

	aggregate : function(taskid,first){ var self = this;

		var selectQuery = { aggregate_id : this.id, task_id : taskid };
		$db.getA( "__info__",selectQuery,{},function(data){
			
			if (data.length != 0){

				self.onAggregate({
					taskId     : taskid,
					first      : first,
					numberNode : data[0].nodes,
					remaining  : data[0].remaining
				},data[0].data);
			}
		});
	}



});


module.exports = Aggregate;