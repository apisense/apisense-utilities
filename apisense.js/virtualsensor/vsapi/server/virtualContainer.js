var A = require("../A.js");
var VN = require("./virtualSensor.js");
var VirtualTask = require("./virtualTask.js");

/** **/
VirtualNodeContainer = A.Class.extend({

  // initialize the virtual container
  initialize : function(){ var self = this;
    
    //
    this.taskId = new Date().getTime();
    
    //
    this.websocket = server.websocket("cellEvent");
    
 
    $eventStream.subscribe(function(device){

      var node = self._virtualNodes[device.vs[0]][device.vs[1]];
      node.registerDevice(device);
    
    }," name = Registration ");

    $eventStream.subscribe(function(device){

      var node = self._virtualNodes[device.vs[0]][device.vs[1]];
      node.unregisterDevice(device);
    
    }," name = Unregistration ");

  },
  
  /**
   * 
   * Publish a new sensing task in all virtual sensors
   * deployed in the virtual container 
   * 
   */
  publishTask : function(task){ var self = this;
    
    // create a new virtual task
    var _newtask = new VirtualTask(task);
    
    
    for (_ivn in this._virtualNodes){
      for (_jvn in this._virtualNodes[_ivn]){
        this._virtualNodes[_ivn][_jvn].publishTask(_newtask);
      }
    }

  },

  publishTaskToNode : function(task,cell){

    var _newtask = new VirtualTask(task);
    this._virtualNodes[cell[0]][cell[1]].publishTask(_newtask);

  },
  
  /**
   * Build and deploy a set of virtual sensors
   * covering the virtual container bounding box
   */
  distribute : function(bound, zoneLength, aggregateCallback ){
  

    if (this.virtualNodes){
        // TODO clean all virtual nodes
        // in the container
    }
    
    this._virtualNodes = [];
    this._address = [];
    this._bound = bound;
    this._zoneLength = zoneLength;
     
    for (i=0; i < zoneLength ;i++){
      
      this._virtualNodes[i] = [];
      
      for (j=0;j<zoneLength;j++){
      
        var node = VN.node(
          "cell/"+i+"-"+j,
          this.websocket,
          { 
            id : "cell/"+i+"-"+j,
            attributes : { bound : this._bound },
            aggregate  : aggregateCallback 
          } 
        );

        this._virtualNodes[i][j] = node;
      }
    }
    
    log.d((this._virtualNodes.length * this._virtualNodes[0].length)+" virtual sensors created")
  },
  

  /**
   * Return virtual container resolver
   * enabling to find a virtual sensor address
   * from a location
   */
  getResolver : function(){  
    return {
      bound : this._bound,
      cellScale : this.cellScale
    }
  },
  
  /**
   * 
   * Return bounding box of the container
   *
   */ 
  getGeometry     : function(){ return this._bound; },
   
  /**
   *
   * Return all virtual sensors deployed
   * in the container
   * 
   */
  getVirtualSensors : function(){ return this._virtualNodes; }
});

module.exports = function(){ return new VirtualNodeContainer(); }