var A = require("../A.js");
var jlinq = require("../jlinq.js");
var VSAggregate = require("./virtualAggregate.js")

 /**
  *
  * Virtual Sensor
  *
  */
 VirtualNode = A.Class.extend({
   
  initialize : function(nodeAddress,websocket,config){ var self = this;
    
    config = config || {};

    /* virtual sensor unique id */
    this.id = config.id || A.uuid();

    this.CollectionName = "__vs-"+this.id+"__"

    this.attributes = config.attributes || {};

    this.aggregateCallback = config.aggregate || function(info,data){ log.d("aggregate");log.d(data); }


    this.aggregator = new VSAggregate(this.id,{

      onAggregate : function(info,data){

        self.aggregateCallback(self,info,data);

      },

      onCompleted : function(taskId){ 

        self.stopTask(taskId); 
      }

    });

    /* virtual sensor http address */
    this.nodeAddress = nodeAddress;
     
    /* websocket notification */
    this.websocket = websocket;
    
    /* configuration :  */
    this.timeToWaitDevices = "15 s"
   
    /* map of physical devices executing a sensing task */
    this.devices = [];

    this.devices = $db.get(this.CollectionName,{},{});
    
  
    /* task deployed in the virtual sensor */
    this.currentTask = undefined;
    
    /*   */
    this.updateActivated = false;
  },
   
  getDevices : function(){ return this.devices; },
  
  /**
   * Register a new mobile device 
   * @param {JSON} user
   * @param {String} user.id Anonymous user id
   * @param {JSON}   user.property static properties of devices
   */
  registerDevice : function(user){

     var _user = jlinq.from(this.devices).equals('id',user.id).select()[0];
     if (!_user){

        log.d("register new user "+user.id+" in cell "+this.nodeAddress);

        user._activated = 0;
        
        this.devices.push(user);
        $db.add(this.CollectionName,user);

        this.update();

        this.websocket.broadcast({ "UpdateCell" : this.nodeAddress })
     }
  },
  
  /**
   * Unregistered mobile device 
   * @param {JSON} user
   * @param {String} user.id Anonymous user id
   */ 
  unregisterDevice : function(user, update){
 
     var update = update || true;

     var _user = jlinq.from(this.devices).equals('id',user.id).select()[0];
     if (_user){

        log.d("unregister user "+user.id+" from cell "+this.nodeAddress);

        $db.remove(this.CollectionName,{ id : user.id });

        delete this.devices[this.devices.indexOf(_user)];
        if (_user._activated == 1){

          if (update == true)
            this.update();
        }

        this.websocket.broadcast({ "UpdateCell" : this.nodeAddress })
     }
  },

  /** 
   *
   * Publish a new task in virtual sensor
   *
   **/
  publishTask : function(task){ var self = this;

    if (this.currentTask == undefined){

      // publish a task in aggregator
      this.aggregator.registerTask({
        id : task._taskId,
        duplicate : task._duplicate,
        attributes : this.attributes
      });

      // set current task
      // and try to deploy it to mobile device
      this.currentTask = task;
      this.update();

      // regsiter a callback to the task
      // to be notified to the end of this task
      task.timeout(function(t){ 
       
        self.stopTask(t._taskId); 
        self.aggregator.end(t._taskId);
      });


      this.websocket.broadcast({ "UpdateCell" : this.nodeAddress });

    } 
    else{ 

      log.d("A Task already running in the virtual sensor ") 
    }
  },
   
  
  stopTask : function(taskId){

    if (this.currentTask == undefined) return;
    
    if (taskId != this.currentTask._taskId){

      return;
    }
    
    delete this.currentTask;
    
    log.d("stop task "+taskId);

    var activeDevices = jlinq.from(this.devices).equals("_activated",1).select();
    activeDevices.forEach(function(device){

      device['_activated'] = 0;
    });

    
    this.websocket.broadcast({ "UpdateCell" : this.nodeAddress });
  },
   
  update : function(){ var self = this;
    
    // test if a task is deployed
    // in current sensing node
    if (this.currentTask == undefined){
      log.d("No task is deployed in cell "+this.nodeAddress);
      return;
    }
    
    if (this.updateActivated) {
      log.d(this.nodeAddress+"Cell updating already ...")
      return;
    }

    var remainingTime = this.currentTask._stopAt -  new Date().getTime();
   
    if (remainingTime <= 1000){
      log.d("time remaining is too short "+(remainingTime/1000)+" seconds");
      return;
    }
    

    this.updateActivated = true;


    // search devices not affected to a task
    var freeDevices = jlinq.from(this.devices).equals("_activated",0).select(function(u){ return u.id; });
    var activeDevices = jlinq.from(this.devices).equals("_activated",1).select();

    // no free device found
    if (freeDevices.length == 0){
      this.updateActivated = false;
      return;
    }

    // task is already assigned to devices
    if (activeDevices.length >= this.currentTask.duplicate()){
      log.d("Task is already assigned");
      this.updateActivated = false;
      return;
    }

    // calcul number of device to recruit for task assignement
    var recruitNumber = this.currentTask.duplicate() - activeDevices.length;
    requestId = this.nodeAddress.substring(5)+"-"+this.currentTask._taskId;


    taskRequest = new TaskRequestrator({
      aggregate_id  : this.id,
      timeToWait : this.timeToWaitDevices,
      requestId  : requestId,
      task       : this.currentTask,
      devices    : freeDevices,
      number     : recruitNumber,
      callback   : function(selectedUsers,noRespondingUsers){

        self.updateActivated = false;

        self.websocket.broadcast({ "UpdateCell" : self.nodeAddress });

        // update information about user
        selectedUsers.forEach(function(_u){

            var user = jlinq.from(self.devices).equals('id',_u.id).select()[0];
            user['_activated'] = 1;
            user['properties'] = _u.properties;
        });
        
        A.binding(noRespondingUsers,{
            '$process' : server.experimentName(),
            eventName : "NotificationRequest"
          },{
            timeToLive : 5*60*1000,
            collapseKey : "NotificationRequest"
        });


        noRespondingUsers.forEach(function(id){

          self.unregisterDevice({ id : id },false);
        });
              
      }
    });

  }
});

module.exports.node = function(arg1,arg2,arg3){ return new VirtualNode(arg1,arg2,arg3); };

 
var TaskRequestrator = A.Class.extend({
 
  /**
   * Task Assignement algorithm 
   * {String}   vsId              : unique id of virtual sensor
   * {Integer}  timeToWaitDevices : Time to wait a response from physical devices
   * {String}   requestId         : Uniq ID of the task for assignement
   * {Object}   task              : Task to assign
   * {Array}    userIds           : Array of users candidate for task assignement
   * {Integer}  recruitNumber   : Number of user to recruit for task assignement
   * {Function} callback          : callback method triggered when task assignement is done
   */
  initialize : function(config){

    var timeToWaitDevices  = config.timeToWait;
         requestId         = config.requestId;
         aggregate_id      = config.aggregate_id;
         task              = config.task;
         userIds           = config.devices;
         recruitNumber     = config.number;
         callback          = config.callback
         _subsTaskResponse = undefined;
         acceptedDevices   = undefined;
         noConnectedDevices= undefined;
   
    log.d(" Need to recruit "+recruitNumber+" from users ");
    
    // Clone array of devices
    noConnectedDevices = JSON.parse(JSON.stringify(userIds));
    acceptedDevices     = [];

    // push a task request to mobile devices    
    A.binding(userIds,
        { 
          '$process'  : server.experimentName(), 
          'eventName' : "TaskRequest", 
          'recruit'   : task.buildAuthorizationTask(), 
          'requestId' : requestId,
          'aggregate_id'     : aggregate_id
        },
        {
          timeToLive : 60*1000,
          collapseKey : "TaskRequest"
    });
    
        
    // waiting a response from physical device available

    _subsTaskResponse = $eventStream.subscribe(this._handlerTaskRequest, 
      "&( eventName = RequestTaskResponse )( requestId = "+requestId+" )")
    
    log.d(_subsTaskResponse);
    log.d("waiting user ... "+timeToWaitDevices)
    
    // waiting a threshold value to trigger
    // assignement of the task to device availables
    timer.scheduleTask(timeToWaitDevices,this._handlerTimeOut);

  },

  _handlerTaskRequest : function(event){

      var user = event.user;
                  
      // if user has accepted the sensing task
      if (user.taskAccepted == true){
         acceptedDevices.push(user);
      }
      
      var userIndex = noConnectedDevices.indexOf(user.id);
      if (userIndex != -1){
        delete noConnectedDevices[userIndex];
      }

      log.d("response of user "+JSON.stringify(user));
  },

  _handlerTimeOut : function(){

      _subsTaskResponse.cancel();
      
      var recruitedUsers = task.ranking(jlinq.from(acceptedDevices));
      var selectedUser   = [];
          userIds        = [];


      var _index = recruitNumber;
      recruitedUsers.forEach(function(user){

        if (_index > 0){
          userIds.push(user.id);
          selectedUser.push(user);
          _index -= 1;
        }

      });

      callback(selectedUser, noConnectedDevices);

      if (recruitedUsers.length == 0){

         log.d("cannot find user"); 
         return;
       }
       
      log.d("forward execution to "+JSON.stringify(userIds)); 
      A.binding(userIds,
        { 
          '$process'  : server.experimentName(),
          'eventName' : "TaskExecutionRequest",
          'task' : task.buildTask(),
          'requestId' : requestId 
        },{
          'timeToLive'  : 60*1000,
          'collapseKey' : "TaskExecutionRequest"
      });
  }

});
 
 
