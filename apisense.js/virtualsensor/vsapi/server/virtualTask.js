var A = require("../A.js");

/**
 *
 * Virtual Task
 *
 * Abstraction of a sensing task properties
 */
VirtualTask = A.Class.extend({

  /**
   * Initialisation of a sensing task properties
   *
   */
  initialize : function(task){ var self = this;
    
    // task id
    this._taskId = A.uuid();
    this.timeoutCallback = [];

    // number of mobile devices to duplicate
    // the sensing task
    this._duplicate = task.duplicate || 51;

    // duration of the sensing
    // default 1 Hour
    this._duration = task.duration || 60 * 60 * 1000
    
    // time start of sensing task
    this._startAt = new Date().getTime();
    
    // calcul timeout of sensing task from duration
    this._stopAt = this._startAt + this._duration
    
    // function executed to select best mobile device 
    // to execute the sensing task 
    this.ranking = task.ranking || function(users){ return users; };
 
    if (task.recruit) this.deviceCode.recruit = task.recruit;

    if (task.preTask) this.deviceCode.preTask = task.preTask;

    if (task.postTask) this.deviceCode.postTask = task.postTask;

    if (task.score) this.deviceCode.score = task.score;
  
    this._virtualNode = undefined;


    this._scheduleEnd();
     
  },

  timeout : function(callback){  this.timeoutCallback.push(callback); },
  
  _scheduleEnd : function(){ var self = this;
    
    timer.scheduleTask(this._duration,function(subs){
      subs.cancel();
     
      self.timeoutCallback.forEach(function(callback){

        callback(self);
      })
    });

  },

  getVirtualNode : function(){return this.__virtualNode;},
  
  setVirtualNode : function(virtualNode){this.__virtualNode = virtualNode;},
  
  duplicate : function(){return this._duplicate},
  
 
  deviceCode : {
  
    score : function(){  return (android.time() - this.startExecutionAt)*100/(this.stopAt - this.startAt); },

    preTask : function(){},

    postTask : function(){},

    recruit : function(){ return {}; }
  },  
  
  buildAuthorizationTask : function(){
    var a = { 
      recruit:this.deviceCode.recruit, 
      id : this._taskId
    };
    return A.stringify(a);
  },
  
  buildTask : function(){
    var a = {
      
      // sensing task id
      id: this._taskId, 

      score : this.deviceCode.score, 
      
      startAt: this._startAt,

      stopAt: this._stopAt,
      
      preTask: this.deviceCode.preTask,
      
      postTask: this.deviceCode.postTask
    };
    
    return A.stringify(a);
  }
  
});

module.exports = VirtualTask