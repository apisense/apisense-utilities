var A = require("../A.js")
var VSContainer = require("./virtualContainer.js")
var jlinq = require("../jlinq.js");
var VSDevice = require("../mobile/VirtualDevice.js")
var VSAggregate = require("./virtualAggregate.js")


var sensingAPI = A.Class.extend({
    
  initialize : function(host){ var self = this;

    this.host =  host || undefined;

    this.aggregateCallback = undefined;

    server.post("container/resolver",function(){ return A.stringify(self.virtual.getResolver());})

    server.post("container/geo",function(){ return self.virtual.getGeometry();})

    server.post("container/cell/info",function(args){ 
      //var cell = getCell("cell",args);
      var node = self.virtual._virtualNodes[args['cell[]'][0]][args['cell[]'][1]];
      var object = {};
      object.devices = node.devices;
      
      if (node.currentTask){
        object.task = {
          id     : node.currentTask._taskId,
          stopAt : node.currentTask._stopAt,
          startAt : node.currentTask._startAt,
          duplicate : node.currentTask._duplicate
        }
      }
      return object;
    });

    server.post("container/publish/cell",function(args){ 

      var cell = [args['cell[]'][0],args['cell[]'][1]];
      self.virtual.publishTaskToNode(self._sensingTask,cell);
    });



  },

  libs : function(libs){ this.libsPath = libs+"/vs-mobile-1.0.js"; },
  
  sense : function(sensingTaskProperties){this._sensingTask = sensingTaskProperties;},
   
  coverage : function(bound, meter){

    this.virtual = VSContainer();
    this.virtual.cellScale = meter;
    
    
    this.bound = bound;
    this.cellScale = meter;

  },

  ranking : function(_funk){ this.rankingFunk = _funk; },

  recruit : function(_funk){ this.recruitFunk = _funk; },

  aggregate : function(aggregateCallback){ this.aggregateCallback = aggregateCallback },

  every : function(period){ this._period = period; },

  publish : function(){


     this.virtual.publishTask(this._sensingTask);
  },

  start : function(){ var self = this;

    log.d("start experiment");

    if (!this.libsPath) this.libsPath = "http://static.apisense.fr/g/vs/vs-mobile-1.0.js"

    if (!this.virtual){
      log.d("please define experiment coverage ");
      $context.exit();
    }

    if (!this._sensingTask){
      log.d("please define a sensing task ");
      $context.exit();
    }

    this._generate(this._sensingTask);

    // create a set of virtual around the coverage area
    this.virtual.distribute(this.bound,this.cellScale, this.aggregateCallback);

    if (this.recruitFunk) this._sensingTask.recruit = this.recruitFunk;

    if (this.rankingFunk) this._sensingTask.ranking = this.rankingFunk;

    // publish sensing task to all created virtual sensors
    this.virtual.publishTask(this._sensingTask)

    //if (this._period){

    //  timer.schedulePeriod(this._period,function(){
              
    //      self.virtual.publishTask(self._sensingTask)
    //  })

    //}

    //crowd.experiment({ "$command" : "forceUpdate" },{},function(response){
    //  log.d(response);
    //});

 },

  /**
   * Generate mobile code to publish in central server
   *
   */
 _generate : function(sensingTask){

     // generate main js
     var mainJs = "";
     mainJs = mainJs  + " $context.load('device.js');\n ";
     mainJs = mainJs  + " var vs = require('publicsensing'); \n ";
     mainJs = mainJs  + " $asl.addTask('sensingTask','mainTask.js'); \n ";
     
  
    // load sensing task definition
    $store.addScript("mainTask.js",$context.read(sensingTask.task));

    // load sensing task libraries
    if (sensingTask.taskLibs){
        sensingTask.taskLibs.forEach(function(libs){

          if (libs.indexOf('/') >= 0){
            
            mainJs = mainJs  + " $asl.addScript('sensingTask','"+libs.substring(libs.lastIndexOf('/')+1)+"'); \n ";
          }
          else{

            mainJs = mainJs  + " $asl.addScript('sensingTask','"+libs+"'); \n ";
          }
          
         
          $store.addScript(libs);

        });
    }

    if (!this.host){
       mainJs = mainJs  + " var virtualDevice = vs.device(); \n ";
    }
    else{

      mainJs = mainJs  + " var virtualDevice = vs.device('"+this.host+"'); \n ";
    }
     
    mainJs = mainJs  + " virtualDevice.start(); \n ";

    // publish code in sensing node
    $store.addScript("main.js",mainJs);
    $store.addScript("device.js",$context.read(this.libsPath));

    // publish code to central server
    $store.publish("2.0","small description");
 }



});

module.exports.jlinq =  jlinq;
module.exports.experiment = function(host){ return new sensingAPI(host); }
module.exports.aggregate  =  function(id){    return new VSAggregate(id); }
module.exports.jlinq  =  jlinq;


