
$(function(){
	var currentCell = undefined;

	//var WebSocketHOST = "ws://apisense-thesis.cloudapp.net/websocket/PublicSensing/cellEvent";
	var WebSocketHOST = "ws://"+IP+"/websocket/"+XP+"/cellEvent";

	var web = new APIS.WebSocket({
		path : WebSocketHOST,
		onOpen    : function(){},
		onClose   : function(er){},
		onMessage : function(mess){

			var _mess = APIS.parse(mess)
			if (_mess.hasOwnProperty("UpdateCell")){
				var cell = _mess["UpdateCell"].split("/")[1]
				updateCell([ cell.split("-")[0],cell.split("-")[1]])
			}
		}
	})


    var objectToHtml = function(property){

    	var content = "";
    	for (var k in property){

    		var data = property[k]
    		var type = typeof data
    		
    		if (type == "object"){
    			content += objectToHtml(data);
    		}
    		else{
    		  content += "<strong>"+k +"</strong> : "+data
    		  content += "<br/>";
    		}
    	}
    	return content;
    } 

	var updateCell = function(cellAddress){

		//var _p = properties[cellAddress[0]][cellAddress[1]]
		console.log(cellAddress);

		Http.post(HTTPHOST+"container/cell/info",{cell : cellAddress}).onSuccess = function(mess){
			
			
			var vs 		= APIS.parse(mess)
			var devices = vs.devices;
			
			var taskContent = "<center><strong>Task</strong></center><br/>";
			if (vs.task){
				var task = vs.task
				taskContent += "<strong>duplicate</strong> "+task["duplicate"];taskContent +="<br/>"
				taskContent += "<strong>ID</strong> "+task["id"];taskContent +="<br/>"
				taskContent += "<strong>start</strong> "+new Date(task["startAt"]).toISOString();taskContent +="<br/>"
				taskContent += "<strong>stop</strong>  "+new Date(task["stopAt"]).toISOString();taskContent +="<br/>"
			}

			var props = properties[cellAddress[0]][cellAddress[1]];
			
			var point = props.centerPoint;
			
			var nbActif = 0;

			taskContent += "<center><strong>User</strong></center><br/>";
			
			for (i=0;i<props.markers.length;i++){ map.removeLayer(props.markers[i]); }
			properties[cellAddress[0]][cellAddress[1]].markers = [];

			devices.forEach(function(device){
				console.log(device)
				taskContent += "User : "+device.id+"<br/>";
				device.properties.active = device._activated;
				nbActif += device._activated;
				taskContent += objectToHtml(device.properties);

			});

			var label  = "<div class='marker-label'>";
			    label += "Connected : "+devices.length;
			    label += '<br/>';
				label += "Running	: "+nbActif
				label += "<div/>";
	
			

			var className = '';
			if (nbActif > 0) className += ' marker-active';
			else if (vs.task) className += ' marker-static'
			else className += ' marker-static-empty'

			
			
			var myIcon = L.divIcon({ className: className, html : label , iconSize : [100,45] });
			
			var marker = L.marker([point[0],point[1]], {icon: myIcon}).bindPopup(taskContent).addTo(map);
			
			properties[cellAddress[0]][cellAddress[1]].markers.push(marker);

		}
	}

			
	// create a map in the "map" div, set the view to a given place and zoom
	var map = L.map('map').setView([50.6214, 3.112], 13);
		
	// add an OpenStreetMap tile layer
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
	}).addTo(map);

	$("#idPublish").click(function(){

		if (currentCell != undefined){

			Http.post(HTTPHOST+"container/publish/cell",{cell : currentCell}).onSuccess = function(mess){



			}
		}
	})
	
	var resolver = undefined;

	var properties = {};

	Http.post(HTTPHOST+"container/resolver",{}).onSuccess = function(mess){
		
		resolver = APIS.parse(mess);

		var bound = resolver.bound;
		var cellScale = resolver.cellScale;

		map.fitBounds([
    		[bound.lat1, bound.lon1],
    		[bound.lat2, bound.lon2]
		]);

		var cellHeight = Math.abs(bound.lat2 - bound.lat1) / cellScale;
		var cellWidth  = Math.abs(bound.lon2 - bound.lon1) / cellScale;
		
		for (var i=0;i<cellScale;i++){
			properties[i] = []

			for (var j=0;j<cellScale;j++){

				var northEast = new L.LatLng(bound.lat1 - (cellHeight * (j))   ,bound.lon1 + (cellWidth * (i)));
				var southWest = new L.LatLng(bound.lat1 - (cellHeight * (j+1)) ,bound.lon1 + (cellWidth * (i+1)));
				
				var cellBound = new L.LatLngBounds(southWest,northEast);
				var cell = L.rectangle(cellBound,{color: "#111", weight: 2});

				var cellAddr = [i,j];

				var onClick = function(cell,addr){

					cell.on('click',function(e){
						updateCell(addr);
						currentCell = addr;
						$("#idvsname").text("Cell-"+addr[0]+"-"+addr[1]);
					})
				}
				onClick(cell,cellAddr);

				cell.addTo(map);

				var _centerPoint = [ bound.lat1 - (cellHeight * (j)) - (cellHeight/2), bound.lon1 + (cellWidth * (i)) + (cellWidth/2) ];
				properties[i][j] = { address : [i,j], centerPoint : _centerPoint, markers : []};

			}
		}

	};
})