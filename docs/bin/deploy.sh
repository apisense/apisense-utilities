#/bin/bash

## need version number of documentation
#if [ $# -ne 1 ]
#then
#  echo "Usage: Please enter version name"
#  exit 
#fi

# define filename
BASE="../dist"

MOBILE_VERSION="2.1.0-SNAPSHOT";

MOBILE_FILE=$BASE"/"$MOBILE_VERSION

# create dist folder if exist
if [ ! -d "$BASE" ]; then echo "create folder $BASE"; mkdir $BASE; fi

# remove old documentation
if [ -d "$MOBILE_FILE" ]; then echo "remove folder $MOBILE_FILE"; rm -R $MOBILE_FILE; fi

mkdir $MOBILE_FILE

# copy docs
cp -R ../jaguar/mobile/dist/* $MOBILE_FILE

cd $BASE

# create zip archive
zip -r $MOBILE_VERSION.zip $MOBILE_VERSION

# copy archive to metronet server
scp $MOBILE_VERSION.zip metronet1:/tmp

# delete old document 
ssh metronet1 -t " sudo rm -R  /var/www/static.apisense.tld/docs/$MOBILE_VERSION"

# extract new document
ssh metronet1 -t " sudo unzip /tmp/$MOBILE_VERSION.zip -d  /var/www/static.apisense.tld/docs"

# delete tmp archive
ssh metronet1 -t "rm /tmp/$MOBILE_VERSION.zip"

echo "DONE"



