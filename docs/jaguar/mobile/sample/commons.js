/**
 * @file
 * @module engine
 */
/**
	 * @class
	 * 
	 *
	 */
EventStreamFacade = function(){ 

/**
	 * Register a callback for a particular event  
	 * @param {function(event,subscriber)} 	callback
	 * @param {object} 						callback.event Received event
	 * @param {module:pubsub~Subscriber} 	callback.subscriber Reference  to the subscription
	 * @param {String}  [filter] {@linkplain module:pubsub|See Section JFilter}
	 * @return {module:pubsub~Subscriber}
	 */

    this.subscribe = function(){};

/**
	 * Publish a new event in script event stream system
	 * @param {object} event event to publish 
	 */

    this.publish = function(){};

/**
	 * Broadcast an event for all event stream systems
	 * @param {object} event event to publish 
	 */

    this.broadcast = function(){};

/**
	 * Publish an event is a particular event stream system
	 * @param {string} name  name of the event stream system
	 * @param {object} event event to publish 
	 */

    this.publish = function(){};
 
 }; /**
	 * @class
	 * @alias $context
	 * 
	 * @summary Provide access to script context
	 * 
	 */
ContextFacade = function(){ 

/**
     * 
     * Register a callback called when script is done
     * 
     * @param {Function} callback
     */

    this.onPreFinish = function(){};

/**
     * 
     * Register a callback called when script is done
     * 
     * @param callback
     */

    this.onFinish = function(){};

/**
	 * Register a callback called when script is done
	 *
	 * @param {function} callback
	 */

    this.onExist = function(){};

/**
	 * Load external script
	 *
	 * @param {string} scriptPath Script path
	 */

    this.load = function(){};

/**
	 *
	 * Return File content
	 *
	 * @param {string} File path 
	 *
	 * @return {string} Return File content
	 *
	 */

    this.read = function(){};

/**
	 *  Terminate current running script
	 */

    this.exit = function(){};

/**
	 * Terminate current running script
	 * 
	 * @param exitCode Return code
	 */

    this.exit = function(){};

/**
	 * 
	 * Sleep current running process
	 * 
	 * @param {number} milli Time to wait in millisecond
	 */

    this.sleep = function(){};

/**
	 * Return global process name
	 * 
	 * @return {string} Return global process name
	 */

    this.globalProcess = function(){};

/**
	 * Return current process name
	 * 
	 * @return {string} Return current process name
	 */

    this.process = function(){};

/**
	 * 
	 * Return date with Local.US format
	 * 
	 * @return {string} Return date with Local.US format
	 */

    this.dateUs = function(){};

/**
	 * 
	 * Return date with Local.FR format
	 * 
	 * @return {string} Return date with Local.FR format
	 */

    this.dateFr = function(){};

/**
	 * 
	 * Return System time millisecond
	 * 
	 * @return {number} Return System time millisecond
	 */

    this.ms = function(){};
 
 }; /**
	 * @class 
	 * @alias module:commons.subscriber
	 */
Subscriber = function(){ 

/**
	 * 
	 * Cancel event subscription
	 * 
	 */

    this.cancel = function(){};
 
 }; 