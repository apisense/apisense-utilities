/**
	 * @class 
	 * 
	 * 
	 * ### Time Definition
	 * 
	 * Time syntax is defined as <time> <unit> 
	 * 
	 * Unit can have for value 
	 * 
	 *|			unit    		|		description		|
	 *|:----------------------- |:--------------------- |
	 *|	`ms`					| *millisecond* 		|
	 *|	`s`						| *second* 				|
	 *|	`h`						| *hour* 				|
	 *|	`d`						| *day* 				|
	 */
SchedulerFacade = function(){ 

/**
	 * Schedule a networking task. If device has a network connection,
	 * the task will executed directly, else the task will
	 * we executed when a networking connection will be available.
	 * 
	 * @param  {string}	 collapseKey 	CollapseKey allow to replace old task with the same collapseKey to avoid
	 * 									to sending the same task several times when network become availble.
	 * @param  {function} task			Task to schedule
	 * 
	 * @return {module:commons.subscriber}  Instance of the task subscription which can be used to cancel the task
	 */

    this.network = function(){};

/**
	 * Schedule a networking task. If device has a network connection,
	 * the task will executed on function call, else the task will
	 * we executed when a networking connection will be available.
	 * 
	 * @return {module:commons.subscriber}  Instance of the task subscription which can be used to cancel the task
	 */

    this.network = function(){};

/**
	 * Schedule a task at a specific time
	 *
	 * @param {number}   Time (in milliseconds) 
	 * @param {function} Task to execute
	 * 
	 * @return {module:commons.subscriber}  Instance of the task subscription which can be used to cancel the task
	 */

    this.at = function(){};

/**
	 *
	 * Schedule a task in a specific duration
	 *
	 * @param {number}   Time in *millisecond*  
	 * @param {function} Task to execute
	 * 
	 * @return {module:commons.subscriber}  Return task reference
	 */

    this.in = function(){};

/**
	 * 
	 * Schedule a periodic task
	 * 
	 * @param {string}   period Periodic time to schedule task, {@link module:schedule~SchedulerFacade|See Section Time Definition}.
	 * @param {function} task Task to execute
	 * 
	 * @return {module:commons.subscriber}  Return task reference
	 */

    this.every = function(){};

/**
	 * 
	 * Schedule a periodic task
	 * 
	 * @param {number}   period Period in millisecond
	 * @param {function} task Task to execute
	 * 
	 * @return {module:commons.Subscriber}  Return task reference
	 */

    this.every = function(){};
 
 }; 