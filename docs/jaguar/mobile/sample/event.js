/**
 * @namespace event
 */
/**
	 * @class event.LeafEvent
	 * @static
	 * @property {number} time Current system time in milliseconds since January 1, 1970 00:00:00 UTC.
	 * where event has been fix
	 * 
	 * @property {number} event name
	 */
BasicEvent = function(){ 


    this.toSource = function(){};
 
 }; 

/**
 * @class 
 * @alias event.signal
 * @event
 * 
 */
SignalStrengthEvent = function(){ 

		/**
		 * @property {number} event name
		 */

		this.signalStrength = "";
 		
 		/**
	 * Return task id
	 * 
	 * @return {string} Return task id
	 */

 		this.myfunc = function(){};
 }; 

 