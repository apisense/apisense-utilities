/**
	 * 
	 * @class
	 */
NetworkingTask = function(){ 

/**
	 * Return task id
	 * 
	 * @return {string} Return task id
	 */

    this.getTaskId = function(){};

/**
	 * 
	 * Cancel future execution of the task
	 * 
	 */

    this.cancel = function(){};
 
 }; 