/**
 * @fileOverview
 * 
 * *** Module Overview ***
 * 
 * @module sensors
 */
/**
	 * #### Quick Example 
	 * @example 
	 * <caption>Listen all battery events</caption>
	 * battery.onBatteryStateChange(function(event){
	 *    $log.toast(
	 *    	"battery level "+ event.level  +
	 *    	"battery state "+ event.status
	 *    )
	 * })
	 * 
	 * @example 
	 * <caption>Get battery state</caption>
	 * $log.toast(battery.info());
	 * 
	 * @class 
	 * @alias battery
	 * @summary Capture device battery state
	 */
BatteryFacade = function(){ 

/**
	 * 
	 * Listen all device battery events.
	 * 
	 * @param  {function(event,subscriber)} callback  
	 * @param  {module:sensors.battery:BatteryEvent} callback.event battery event
	 * @param  {module:commons.subscriber}  		 callback.subscriber 
	 * 		subscription instance that can be used to cancel the subscription.
	 * @param {String} [filter] 
	 * 		filter. See tutorial {@tutorial jfilter}
	 * @return {module:commons.subscriber}				
	 * 		Returns a subscription instance that can be used to cancel the subscription.
	 */

    this.onBatteryStateChange = function(){};

/**
	 * Return last battery event
	 * 
	 * @return {module:sensors.battery:BatteryEvent} Return last battery event
	 */

    this.info = function(){};
 
 }; AbstractAndroidFacade = function(){ 
 
 }; /**
	 * @class 
	 * @alias module:sensors.android
	 * @summary Provide global information about mobile device system
	 */
AndroidFacade = function(){ 

/**
	 * 
	 * Return current time millisecond
	 * 
	 * @return {number} current time millisecond
	 */

    this.time = function(){};

/**
	 * 
	 * Return SDK version 
	 * 
	 * @return {number} Return SDK version 
	 */

    this.api = function(){};

/**
	 * Return the name of the industrial design
	 * 
	 * @return {string} name of the industrial design
	 */

    this.device = function(){};

/**
	 * Return hardware name 
	 * 
	 * @return {string} Return hardware code name 
	 */

    this.hardware = function(){};


    this.id = function(){};

/**
	 * Return APISENSE version code
	 * 
	 * @return {number} 
	 */

    this.versionCode = function(){};

/**
	 * Return APISENSE version name
	 * 
	 * @return {string} APISENSE version name
	 */

    this.versionName = function(){};
 
 }; /** 
	 * @example 
	 * <caption>Quick Example</caption>
	 * 
	 * // request location update  
	 * location.onLocationChanged({provider : ["gps","network"]},function(event){
	 * 
	 * 
	 * })
	 * 
	 * @class 
	 * @alias module:sensors~location
	 * @summary Provides access to the location system of mobile device
	
	 */
LocationFacade = function(){ 

/**
	 *
	 * Register for location events.
	 * 
	 * @fires module:sensors#LocationEvent
	 * 
	 * @param {object} params 						configuration location sensor monitoring
	 * @param {array}  params.provider              location sensor provider
	 * @param {string} params.distance				distance in meter
	 * @param {function(event,subscriber)} 			callback 
	 * @param {module:sensors#location:LocationEvent} 		callback.event
	 * @param {module:pubsub~Subscriber}  			callback.subscriber
	 * 
	 *
	 * @param {String} [filter] {@link module:pubsub|See Section JFilter}
	 * @return {module:pubsub~Subscriber}
	 */

    this.onLocationChanged = function(){};

/**
	 * 
	 * Return last location fix. If location has no fix, a default
	 * location is returned with provider named "none"
	 *
	 * @return {module:location.event:LocationEvent} Return last location fix
	 */

    this.location = function(){};

/**
	 * Force location system to fix a new location
	 * @param {String[]}   [providers=["network"]]  - List of location provider to use (gps, network or passive) to fix the location
	 * @param {number}     [wait=45000]      		- Maximal time (in millisecond) to wait to fix the new location
	 * @param {number}     [accuracy=1000]  		- Acceptable accuracy (in meter) to handle location fix before duration
	 * @param {function(event)} callback  			- Callback called with the new location is fixed or wait time expire
	 */

    this.fix = function(){};


    this.fix = function(){};


    this.fix = function(){};

/**
	 * Return last location fix.
	 * If location has not been fixed before, a default
	 * location is returned with provider named "none"
	 *
	 * @return {module:sensors.location:LocationEvent} Last location fix
	 */

    this.locationCache = function(){};

/**
	 * Return  whether GPS is enable or disable
	 *
	 * @return {boolean} True if GPS is enable
	 */

    this.isGpsEnable = function(){};

/**
	 * Start location setting view. Can be used in order to propose
	 * to user to active GPS.
	 */

    this.showLocationSetting = function(){};

/**
	 * Return distance in meter from two points
	 * 
	 * @param {number} startLatitude  start latitude point
	 * @param {number} startLongitude start longitude point
	 * @param {number} endLatitude    end latitude point
	 * @param {number} andLongitude   end longitude point
	 * 
	 * @return {number} Distance in meter
	 */

    this.distance = function(){};
 
 }; /**
	 * @class 
	 * @alias module:sensors.telephony
	 * @summary Provide information about telephony system
	 */
TelephonyFacade = function(){ 

/**
	 * 
	 * Listen mobile signal strength events
	 * 
	 * @param  {function(event,subscriber)} function(event,subscriber) 	callback
	 * @param  {module:sensors.telephony:SignalStrengthEvent} 			callback.event signal strength events
	 * @param  {module:commons.subscriber}  		 					callback.subscriber 
	 * 		subscription instance that can be used to cancel the subscription.
	 * @param {String} [filter] 
	 * 		filter. See tutorial {@tutorial jfilter}
	 * @return {module:commons.subscriber}				
	 * 		Returns a subscription instance that can be used to cancel the subscription.
	 */

    this.onSignalStrengthChanged = function(){};

/**
	 * 
	 * Listen all changes of connected cell location of mobile device
	 * 
	 * @param  {function(event,subscriber)} callback  
	 * @param  {module:sensors.telephony:CellLocationEvent} callback.event cell location event
	 * @param  {module:commons.subscriber}  		 		callback.subscriber 
	 * 		subscription instance that can be used to cancel the subscription.
	 * @param {String} [filter] 
	 * 		filter. See tutorial {@tutorial jfilter}
	 * @return {module:commons.subscriber}				
	 * 		Returns a subscription instance that can be used to cancel the subscription.
	 */

    this.onCellLocationChanged = function(){};


    this.deviceId = function(){};

/**
	 * Return Last cell location event fix
	 * 
	 * @return {module:sensors.telephony:CellLocationEvent} Last cell location event
	 */

    this.cellLocation = function(){};

/**
	 * Return last signal strength event fix
	 * 
	 * @return {module:sensors.telephony:SignalStrengthEvent} Last signal strength event fix
	 */

    this.signalStrength = function(){};

/**
	 *  Returns a constant indicating the call state (cellular) on the device.
	 *  
	 *  *** Constant value ***
	 *  
	 *  0 : Device call state: No activity      																					 	           																																																								
	 *  1 : Device call state: Ringing   
	 *  2 : A new call arrived and is ringing or waiting     
	 *  3 : In the latter case, another call is already active)     
	 *  4 : Device call state: Off-hook. At least one call exists that is dialing, active, or on hold,
	 *       and no calls are ringing or waiting)   	 	
	 *																																			
	 * @return {number} A constant indicating the call state (cellular) on the device.
	 * 
	 */

    this.callState = function(){};

/**
	 * Return Mobile Country Code
	 * 
	 * @return {number}  Return Mobile Country Code
	 */

    this.mcc = function(){};

/**
	 * Return Mobile Network Code  
	 * 
	 * @return {number} Return Mobile Network Code  
	 */

    this.mnc = function(){};

/** 
	 * Returns the numeric name (MCC+MNC) of current registered operator.
	 * 
	 * @return {string} The numeric name (MCC+MNC) of current registered operator.
	 */

    this.plmn = function(){};

/**
	 * Returns the alphabetic name of current registered operator.
	 * 
	 * @return {string} The alphabetic name of current registered operator.
	 */

    this.operatorName = function(){};

/**
	 * Returns a value indicating the device phone type. 
	 * This indicates the type of radio used to transmit voice calls
	 * 
	 * *** Constant value *** 
	 * 
	 *  0 : Phone type unknown	
	 * 	1 : Phone type GSM		 
	 * 	2 : Phone type CDMA		 
	 * 	3 : Phone type SIP		
	 * 
	 * @return {number} A constant indicating the device phone type.
	 */

    this.phoneType = function(){};

/**
	 * Returns a constant indicating the state of the device SIM card.
	 * 
	 * *** Constant value ***
	 * 
	 * 0 : SIM State Unknown 
	 * 1 : SIM State Absent      	
	 * 2 : SIM State Pin required    
	 * 3 : SIM State Puk required     
	 * 4 : SIM State Networkd required
	 * 5 : SIM State Ready      		
	 * 
	 * @return {number}  A constant indicating the state of the device SIM card
	 */

    this.simState = function(){};

/**
	 * Return true if sim state is ready
	 * 
	 * @return {boolean} true if sim state is ready
	 */

    this.simReady = function(){};

/**
	 * 
	 * Returns the Service Provider Name (SPN).
	 * 
	 * @return {string} Service Provider Name (SPN).
	 */

    this.simOperatorName = function(){};


    this.neighboringCellInfo = function(){};
 
 }; /**
		 * 
		 * 
		 * @event module:sensors.telepehony:NeighboringCellInfoEvent
		 * 
		 * @property {number} cellId Cell id in GSM, 0xffff max legal value UNKNOWN_CID if in UMTS or CDMA or unknown
		 * @property {number} lac LAC in GSM, 0xffff max legal value UNKNOWN_CID if in UMTS or CMDA or unknown
		 * @property {number} networkType Radio network type while neighboring cell location is stored
		 * @property {number} psc Primary Scrambling Code in 9 bits format in UMTS, 0x1ff max value UNKNOWN_CID if in GSM or CMDA or unknown
		 * @property {number} rssi Received signal strength or UNKNOWN_RSSI if unknown. <br/>
		 * For GSM, it is in "asu" ranging from 0 to 31 (dBm = -113 + 2*asu) 0 means "-113 dBm or less"
		 * and 31 means "-51 dBm or greater" For UMTS, it is the Level index of CPICH RSCP defined 
		 * in TS 25.125
		 */
NeighboringCellInfoEvent = function(){ 
 
 }; /**
		 * Augment event {@link module:commons.LeafEvent}
		 * 
		 * @event module:sensors.telephony:CellLocationEvent
		 * 
		 */
CellLocationEvent = function(){ 
 
 }; /**
		 * Augment event {@link module:commons.LeafEvent}
		 * 
		 * @event module:sensors.telephony:GsmCellLocationEvent
		 * @augments module:sensors.telephony:CellLocationEvent
		 * @property {number} cellId     Gsm cell id
		 * @property {string} cellIdHex  Gsm cell id (Value in Hexa)
		 * @property {number} lac 		 Gsm location area code
		 * @property {string} lacHex 	 Gsm location area code, value is given in Hexa
		 * @property {string} rat 	 	 Radioaccess technology. G for GSM, W for WCDMA
		 */
GsmCellLocationEvent = function(){ 
 
 }; /**
		 * Augment event {@link module:commons.LeafEvent}
		 * 
		 * @class
		 * @alias module:sensors.telephony:CdmaCellLocationEvent
		 * @augments module:sensors.telephony:GsmCellLocationEvent
		 * @event module:sensors.telephony:CdmaCellLocationEvent
		 * 
		 * @property {number} stationId Cdma base station identification number, -1 if unknown
		 * @property {number} stationLatitude Cdma base station latitude, Integer.MAX_VALUE if unknown
		 * @property {number} stationLongitude Cdma base station longitude, Integer.MAX_VALUE if unknown
		 * @property {number} networkId Cdma network identification number, -1 if unknown
		 * @property {number} systemId Cdma system identification number, -1 if unknown
		 */
CdmaCellLocationEvent = function(){ 
 
 }; /**
		 *  
		 * Information about current battery state.
		 * 
		 * @event module:sensors.battery:BatteryEvent
		 * 
		 * @property {number} level 		- current battery level
		 * @property {number} voltage 		- current battery voltage
		 * @property {number} temperature 	- current battery temperature
		 * @property {number} maxLevel 		- maximum value of battery level
		 * @property {number} status 		- current battery status
		 * 
		 * *** Constant value ***
		 * 
		 *  1 : Unknown 		
		 *  2 : Charging		
		 *  3 : Discharging	    
		 *  4 : Not charging	
		 *  5 : Full 		    
		 * 
		 * @property {number} plugged 	- indicate whether the device is plugged in to a power source
		 * 
		 * *** Constant value ***
		 * 
		 *  0 : No Plugged  
		 *  1 : AC 		    
		 *  2 : USB	    
		 *  4 : WIRELESS  
		 */
BatteryEvent = function(){ 
 
 }; /**
		 * @event module:sensors#location:LocationEvent
		 * @type {object}
		 * 
		 * @property {string} provider - Name of the provider that generated this location
		 * @property {number} latitude  - Latitude of this location
		 * @property {number} longitude - Longitude of this location
		 * @property {number} longitude - Accuracy of this location in meters
		 * @property {number} altitude	- Altitude of this location
		 * @property {number} distance	- Distance between last location
		 * @property {number} bearing	- Direction of travel in degrees East of true North
		 * @property {number} speed		- Speed of the device over ground in meters/second
		 * @property {number} timeDelta	- Time delta from the last location
		 */
LocationEvent = function(){ 
 
 }; 