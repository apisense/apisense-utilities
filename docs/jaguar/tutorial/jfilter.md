
	
JFilter is a library to match POJO against LDAP-like or JSON-like filters available on [Github](https://github.com/rouvoy/jfilter).
Currently, the library supports the following LDAP-like filters 
	 	
| Operator    | Description       | Supported types | Filter example |
|:-----------:|:-----------------:|:----------------| :--------------|
| `=`         | *equals to*       | [String](http://docs.oracle.com/javase/6/docs/api/java/lang/String.html), [Number](http://docs.oracle.com/javase/6/docs/api/java/lang/Number.html), [Object](http://docs.oracle.com/javase/6/docs/api/java/lang/Object.html) | `(firstname = John)` |
| `~`         | *differs from*    | [String](http://docs.oracle.com/javase/6/docs/api/java/lang/String.html), [Number](http://docs.oracle.com/javase/6/docs/api/java/lang/Number.html), [Object](http://docs.oracle.com/javase/6/docs/api/java/lang/Object.html) | `(name ~ Smith)` |
| `>`         | *more than*       | [Number](http://docs.oracle.com/javase/6/docs/api/java/lang/Number.html) | `(height > 1.6)` |
| `>=`        | *more or equals*  | [Number](http://docs.oracle.com/javase/6/docs/api/java/lang/Number.html) | `(height >= 1.6)` |
| `<`         | *less than*       | [Number](http://docs.oracle.com/javase/6/docs/api/java/lang/Number.html) | `(age < 20)` |
| `<=`        | *less or equals*  | [Number](http://docs.oracle.com/javase/6/docs/api/java/lang/Number.html) | `(age <= 20)` |
| `!`         | *not*             | Filter          | `!(age<10)` |
| `&`         | *and*             | Filters         | `&(name=Doe)(firstname=John)` |
| `PIPE`      | *or*              | Filters         | `PIPE(age<10)(male=true)` |
| *wildcards* | *matches all*     | [String](http://docs.oracle.com/javase/6/docs/api/java/lang/String.html) | `&(firstname=J*)(name=Do?)` |
| *types*     | *conforms to*     | [Object](http://docs.oracle.com/javase/6/docs/api/java/lang/Object.html) |  `(objectClass=Person)` |
	  