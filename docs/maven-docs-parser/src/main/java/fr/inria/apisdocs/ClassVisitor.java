package fr.inria.apisdocs;

import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.expr.AnnotationExpr;
import japa.parser.ast.expr.MemberValuePair;
import japa.parser.ast.expr.NormalAnnotationExpr;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.util.List;

import fr.inria.apisdocs.model.JSDocModel;

public class ClassVisitor extends VoidVisitorAdapter<Object>{

	private CompilationUnit cu;
	
	private JSDocModel model;

	public ClassVisitor(JSDocModel model, CompilationUnit cu){
		this.cu = cu;
		this.model = model;
		this.visit(cu, null);
	}
	
	
	
	public void visit(ClassOrInterfaceDeclaration clazz, Object arg1) {
		super.visit(clazz, arg1);
		
		final List<AnnotationExpr> annotations = clazz.getAnnotations();
		if (annotations != null){
			for (final AnnotationExpr annotation : annotations){

				if (annotation.getName().toString().equals("JsDocs")){
					
					final NormalAnnotationExpr normalAnnotation = (NormalAnnotationExpr) annotation;
					final List<MemberValuePair> pairs = normalAnnotation.getPairs();
					for (final MemberValuePair pair : pairs){
						
						if (pair.getName().equals("module")){
							
							final String modulename = pair.getValue().toString();
							new JsDocVisitor(model, modulename, clazz);
						}
					}
				}
			}
		}
	}


}
