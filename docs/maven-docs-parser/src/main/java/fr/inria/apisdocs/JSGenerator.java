package fr.inria.apisdocs;

import java.io.File;
import java.io.FileOutputStream;

import fr.inria.apisdocs.model.JSDocModel;
import fr.inria.apisdocs.model.JSDocModel.JSDocClass;
import fr.inria.apisdocs.model.JSDocModel.JSDocField;
import fr.inria.apisdocs.model.JSDocModel.JSDocMethod;
import fr.inria.apisdocs.model.JSDocModel.JSDocModule;

public class JSGenerator {

	private String rootFolder; 

	private JSDocModel model;

	public JSGenerator(String rootFolder,JSDocModel model){

		final File folder = new File(rootFolder);
		if (!folder.exists()){
			folder.mkdirs();
		}

		this.model = model;
		this.rootFolder = folder.getAbsolutePath();
	}

	public void run(){

		for (final String moduleName :  model.mModules.keySet()){

			final JSDocModule module = model.mModules.get(moduleName);
			this.run(module);
		}

	}

	public void run(JSDocModule module){

		String moduleName = module.name.replaceAll("\"","");
		if (moduleName.contains("/")){
			moduleName = moduleName.split("/")[1];
		}

		final StringBuilder builder = new StringBuilder();
		builder.append(module.javaDocs);

		for (final JSDocClass jsclass : module.jsClass){
			run(jsclass,builder);
		}

		try {

			final String filename = rootFolder+"/"+moduleName+".js";

			final FileOutputStream outputStream = new FileOutputStream(filename);
			outputStream.write(builder.toString().getBytes());
			outputStream.flush();
			outputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	public void run(final JSDocClass jsclass, StringBuilder builder){
		
		builder.append(jsclass.javaDocs);
		builder.append(jsclass.name);
		builder.append(" = function(){ \n");
		for (final JSDocField jsfield : jsclass.fields){
			run(jsfield,builder);
		}
		for (final JSDocMethod jsmethod : jsclass.methods){
			run(jsmethod,builder);
		}
		builder.append(" \n }; ");
	}

	public void run(final JSDocField jsfield, StringBuilder builder){

		builder.append("\n");
		builder.append(jsfield.javaDocs);
		builder.append("var ");
		builder.append(jsfield.name);
		builder.append("= \"\";");
		builder.append("\n");
	}

	public void run(final JSDocMethod jsmethod, StringBuilder builder){

		builder.append("\n");
		builder.append(jsmethod.javaDocs);
		builder.append("\n");
		builder.append("    this.");
		builder.append(jsmethod.name);
		builder.append(" = function(){};");
		builder.append("\n");
	}


}
