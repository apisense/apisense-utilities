package fr.inria.apisdocs;

import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.ConstructorDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.expr.AnnotationExpr;
import japa.parser.ast.visitor.VoidVisitorAdapter;
import fr.inria.apisdocs.model.JSDocModel;
import fr.inria.apisdocs.model.JSDocModel.JSDocClass;
import fr.inria.apisdocs.model.JSDocModel.JSDocField;
import fr.inria.apisdocs.model.JSDocModel.JSDocMethod;
import fr.inria.apisdocs.model.JSDocModel.JSDocModule;
import fr.inria.asl.facade.FacadeAnnotation.ScriptMethod;

public class JsDocVisitor extends VoidVisitorAdapter<Object>{

	private String moduleName;
	
	private JSDocModel model;
	
	private JSDocClass clazz;

	public JsDocVisitor(JSDocModel model, String moduleName, ClassOrInterfaceDeclaration cu){

		this.moduleName = moduleName;
		this.model = model;
		
		this.clazz = new JSDocClass("");
		
		if (!model.mModules.containsKey(moduleName)){
			model.mModules.put(moduleName, new JSDocModule(moduleName));
		}
		
		this.visit(cu, null);
		
		model.mModules.get(moduleName).jsClass.add(this.clazz);
	}
	
	@Override
	public void visit(ConstructorDeclaration arg0, Object arg1) {
		// TODO Auto-generated method stub
		super.visit(arg0, arg1);
		
		
		if (arg0.getJavaDoc() != null){
			
			this.clazz.javaDocs = arg0.getJavaDoc().toString();
		}
		else{
			this.clazz.javaDocs = "";
		}
	}
	
	
	
	public void visit(ClassOrInterfaceDeclaration clazz, Object arg1) {
		super.visit(clazz, arg1);
		
		this.clazz.name = clazz.getName();
		
		if (clazz.getJavaDoc() != null){
		
			model.mModules.get(moduleName).javaDocs = clazz.getJavaDoc().toString();	
		}
		
		if (clazz.getMembers() != null){

			for (BodyDeclaration body : clazz.getMembers()){

				if (body instanceof FieldDeclaration){

					final FieldDeclaration field = (FieldDeclaration) body;
					
					if (
						(field.getModifiers() == 1) ||   // public 
						(field.getModifiers() == 17)){  // final
	
						
						final JSDocField jsfield = new JSDocField(field.getVariables().get(0).getId().getName());
						if (field.getJavaDoc() != null){
							jsfield.javaDocs = field.getJavaDoc().toString();
							this.clazz.fields.add(jsfield);
						}
					}
				}
			}
		}
		
	}

	
	
	public void visit(MethodDeclaration n, Object arg) {
		super.visit(n, arg);
		
		
		
		if (n == null) return;
		if (n.getAnnotations() == null) return;

		for (AnnotationExpr a : n.getAnnotations()){
			if (a.getName().getName().equals(ScriptMethod.class.getSimpleName())){
				
				
				
				final JSDocMethod method = new JSDocMethod(n.getName().toString());
				if (n.getJavaDoc() != null){
					
					String docs = n.getJavaDoc().toString();
					if(docs.contains("{@add")){
						
						final String extDoc = docs.substring(docs.indexOf("{@add"));
						
					}
					
					
					method.javaDocs = n.getJavaDoc().toString();
				}
				
				
				
				
				this.clazz.methods.add(method);
			}
		}
	}

}
