package fr.inria.apisdocs;

import fr.inria.apisdocs.model.JSDocModel;
import fr.inria.bsense.doc.DocumentGenerator.DocLibrarie;

public class Run {

	public static void main(String[] args) throws Exception {
		
		final String output = System.getProperty("generator.output","output");
		
		final String folders = System.getProperty("generator.folders","../../../apis-scripting/");
		
		final String[] paths = folders.split("@");
		
		final FileVisitor fileVisitor = new FileVisitor(paths, new DocLibrarie());
		
		
		
		final JSDocModel model = fileVisitor.visitRoot();
		
		System.out.println("Generate target: "+output);
		
		new JSGenerator(output, model).run();
	}
	
}
