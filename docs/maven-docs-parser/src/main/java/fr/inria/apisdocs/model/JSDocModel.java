package fr.inria.apisdocs.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class JSDocModel {

	public static class JSDocElem{

		public String name;
		
		public String javaDocs = "";
		
		public JSDocElem(String name){
			this.name = name;
		}
	}
	
	public static class JSDocNamespace extends JSDocElem{

		public JSDocNamespace(String name) {
			super(name);
			
		}
	}
	
	public static class JSDocModule extends JSDocElem{
		
		public List<JSDocClass> jsClass = new ArrayList<JSDocModel.JSDocClass>();
		
		
		public JSDocModule(String name) {
			super(name);
		}
	}

	public static class JSDocClass extends JSDocElem{
		
		final public List<JSDocField> fields = new ArrayList<JSDocModel.JSDocField>();

		final public List<JSDocMethod> methods = new ArrayList<JSDocMethod>();
		
		public JSDocClass(String name) {
			super(name);
		}
	}
	
	public static class JSDocField extends JSDocElem{

		public JSDocField(String name) {
			super(name);
		}
		
	}

	public static class JSDocMethod extends JSDocElem{

		public List<String> externalJavaDocs = new ArrayList<String>();
		
		public JSDocMethod(String name) {
			super(name);
		}
	}

	public Map<String,JSDocModule> mModules = new HashMap<String, JSDocModel.JSDocModule>();
	
	public JSDocModel(){
		
		
		
	}


}
