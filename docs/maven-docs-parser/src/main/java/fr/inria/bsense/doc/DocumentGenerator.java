/**
 * Copyright © 2011-2013, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * Antdroid v1.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.doc;

import java.util.ArrayList;
import java.util.List;

public class DocumentGenerator {

	final StringBuffer buffer = new StringBuffer();
	
	public interface IElementGenerator{
		public StringBuffer generate();
	}
	
	
	public static class DocElement{
		public String javaDoc = "";
		public String element = "";
	}
	
	public static class DocLibrarie extends DocElement{
		final public List<DocClass> facade = 
				new ArrayList<DocumentGenerator.DocClass>();
		final public List<DocClass> events = 
				new ArrayList<DocumentGenerator.DocClass>();
	}
	
	public static class DocClass extends DocElement{
		
		private Boolean empty = true;
		
		public void setEmpty(Boolean e){
			this.empty = e;
		}
		
		public Boolean isEmpty(){return empty;}
		
		final public List<DocField> params = 
				new ArrayList<DocField>();
		
		final public List<DocMethod> methods = 
				new ArrayList<DocumentGenerator.DocMethod>();
		public DocClass(){}
	}
	
	public static class DocMethod extends DocElement{
		final public List<DocField> params = 
				new ArrayList<DocField>();
	}
	
	public static class DocField extends DocElement{
		public String type="";
	}
	
	public DocumentGenerator(){
		
	}
	
	public void generate(){
		
	}
	
}
