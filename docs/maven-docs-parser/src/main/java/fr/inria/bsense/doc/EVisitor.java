/**
 * Copyright © 2011-2013, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * Antdroid v1.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.doc;

import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.util.List;

import fr.inria.bsense.doc.DocumentGenerator.DocClass;
import fr.inria.bsense.doc.DocumentGenerator.DocField;

public class EVisitor extends VoidVisitorAdapter<Object>{

	private List<DocClass> docs;
	
	public EVisitor(List<DocClass> docs){
		this.docs = docs;
		
	}
	
	@Override
	public void visit(ClassOrInterfaceDeclaration dec, Object arg1) {
		super.visit(dec, arg1);

		if (dec.getName().contains("Event")){


			final DocClass doc = new DocClass();
			doc.setEmpty(false);
			doc.element = dec.getName();
			if (dec.getJavaDoc() != null) doc.javaDoc = dec.getJavaDoc().getContent();

			if (dec.getMembers() != null){

				for (BodyDeclaration body : dec.getMembers()){

					if (body instanceof FieldDeclaration){

						final FieldDeclaration field = (FieldDeclaration) body;

						final DocField df = new DocField();
						df.type = field.getType().toString();
						df.element = field.getVariables().get(0).toString();
						if (field.getJavaDoc() != null) df.javaDoc = field.getJavaDoc().getContent();
						doc.params.add(df);
					}
				}
			}
			
			docs.add(doc);


		}
	}
}
