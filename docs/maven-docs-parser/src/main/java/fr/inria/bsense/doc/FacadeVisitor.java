/**
 * Copyright © 2011-2013, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * Antdroid v1.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.doc;

import japa.parser.ast.body.ConstructorDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.Parameter;
import japa.parser.ast.expr.AnnotationExpr;
import japa.parser.ast.visitor.VoidVisitorAdapter;
import fr.inria.asl.facade.FacadeAnnotation.ScriptMethod;
import fr.inria.bsense.doc.DocumentGenerator.DocClass;
import fr.inria.bsense.doc.DocumentGenerator.DocField;
import fr.inria.bsense.doc.DocumentGenerator.DocMethod;

public class FacadeVisitor extends VoidVisitorAdapter<Object>{

	private DocClass doc;
	
	public FacadeVisitor(DocClass doc){
		this.doc = doc;
		
	}
	
	
	@Override
	public void visit(ConstructorDeclaration arg0, Object arg1) {
		// TODO Auto-generated method stub
		super.visit(arg0, arg1);
		
		if (arg0.getJavaDoc() != null){
			
			this.doc.javaDoc = arg0.getJavaDoc().getContent();
		}
		else{
			this.doc.javaDoc = "";
		}
	}
	
	@Override
	public void visit(MethodDeclaration n, Object arg) {
		// here you can access the attributes of the method.
		// this method will be called for all methods in this 
		// CompilationUnit, including inner class methods
		doc.setEmpty(false);
		if (n == null) return;
		if (n.getAnnotations() == null) return;

		for (AnnotationExpr a : n.getAnnotations()){
			if (a.getName().getName().equals(ScriptMethod.class.getSimpleName())){

				final DocMethod method = new DocMethod();
				method.javaDoc = "";
				method.element = n.getName();
				
				if (n.getParameters() != null){
					for (Parameter param : n.getParameters()){
						
						final DocField f = new DocField();
						f.type = param.getType().toString();
						f.element = param.getId().toString();
						method.params.add(f);
					}
				}
				
				if (n.getJavaDoc() != null){
					method.javaDoc = n.getJavaDoc().getContent();
				}
				this.doc.methods.add(method);
			}
		}
	}



}
