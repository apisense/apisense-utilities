/**
 * Copyright © 2011-2013, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * Antdroid v1.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.doc;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.inria.bsense.doc.DocumentGenerator.DocClass;
import fr.inria.bsense.doc.DocumentGenerator.DocLibrarie;

public class FileVisitor {

	final private String[] filesRoot;
	
	final private DocLibrarie docL;
	
	public FileVisitor(final String[] root, DocLibrarie docL){
		
		this.filesRoot = root;
		this.docL = docL;
	}
	
	public void visitRoot() throws Exception{

		for (final String fileRoot : filesRoot){

			final File rootFile = new File(fileRoot);
			if (!rootFile.exists())
				new FileNotFoundException("File "+fileRoot+" not exist");

			if (rootFile.isDirectory()){ visitDirectory(rootFile); }
			else{ visitFile(rootFile); }
		}
	}
	
	public void visitDirectory(final File folder) throws Exception{
		
		for (final File file : folder.listFiles()){
			
			if (file.getName().startsWith(".")) continue;
			
			if (file.isDirectory())
				visitDirectory(file);
			else
				visitFile(file);
			
			
		}
		
	}
	
	public void visitFile(final File file) throws ParseException, IOException{
		
		if (!file.getName().endsWith(".java")) return;
		
			// creates an input stream for the file to be parsed
			final FileInputStream in = new FileInputStream(file);

			CompilationUnit cu;
			try {
				// parse the file
				cu = JavaParser.parse(in);
			} finally {
				in.close();
			}

			
			final DocClass doc = new DocClass();

			
			if (file.getName().contains("Facade") || file.getName().equals("Android.java")){
				
				doc.element = file.getName().substring(0,file.getName().lastIndexOf("."));
				
				new FacadeVisitor(doc).visit(cu, null);
				System.out.println("Add facade class "+doc.element);
				this.docL.facade.add(doc);
			
			}
			else{
				
				final List<DocClass> docs = new ArrayList<DocumentGenerator.DocClass>();
				new EVisitor(docs).visit(cu, null);
				for (DocClass cl : docs){
					System.out.println("Add event class "+cl.element);
					this.docL.events.add(cl);
				}
				
			}
		
		
		
		
		
    }
	
}
