/**
 * Copyright © 2011-2013, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * Antdroid v1.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.doc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

import fr.inria.bsense.doc.DocumentGenerator.DocClass;
import fr.inria.bsense.doc.DocumentGenerator.DocField;
import fr.inria.bsense.doc.DocumentGenerator.DocLibrarie;
import fr.inria.bsense.doc.DocumentGenerator.DocMethod;

public class SimpleTest {
	
	public static void main(String[] args) throws Exception {
		
		
		final String folders = System.getProperty("generator.folders");
		final String output = System.getProperty("generator.output");
		
		if (output == null){
			System.err.println("java property generato.output is NULL");
			System.exit(-1);
		}
		
		if (folders == null){
			System.err.println("java property generato.folders is NULL");
			System.exit(-1);
		}
		
		final String[] paths = folders.split("@");
		for (String folder : paths){
			System.out.println("parse folder "+folder);
		}
		
		final DocLibrarie libs = new DocLibrarie();
		
		final FileVisitor fv = new FileVisitor(paths,libs);
		fv.visitRoot();
		
		
		String base = SimpleTest.class.getProtectionDomain().getCodeSource().getLocation().getFile();
		base = base.substring(0,base.lastIndexOf("/"));
		base = base.substring(0,base.lastIndexOf("/"));
		base = base.substring(0,base.lastIndexOf("/"));
				
		
		System.out.println("root folder "+base);
		
		writeDoc("Facade",libs.facade,base+"/target/facade.js");
		writeDoc("Event",libs.events,base+"/target/event.js");
		
		final Process process = Runtime.getRuntime().exec(
				base+"/generateScriptingDocs "+base+" "+output);
		final InputStream is = process.getInputStream();
		final BufferedReader isErr = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		
		
		BufferedReader outSuccess = new BufferedReader(new InputStreamReader(is));
		String read = outSuccess.readLine();
		while(read != null) {
		    System.out.println("script : "+read);
		    read =outSuccess.readLine();
		}
		
		read = isErr.readLine();
		while(read != null) {
		    System.out.println("error : "+read);
		    read =isErr.readLine();
		}
	
	}
	
	
	public static void writeDoc(String superClass, List<DocClass> lClazz , String filename) throws IOException{
		
		final File file = new File(filename);
		if (!file.exists()){
			file.createNewFile();
		}
		
		final OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file));
		//writer.append("var "+superClass+" = function (){\n");
		for (final DocClass clazz : lClazz){
			
			if (clazz.isEmpty()) continue;
			
			String str = parseJV(clazz.javaDoc);
			writer.append(str.subSequence(0, str.length()));
			writer.append("function "+clazz.element+"(){");
			writer.append("\n");
			writer.append("\n");
			
			for (DocField field : clazz.params){
				
				writer.append("\t"+parseJV(field.javaDoc));
				writer.append("\tthis."+field.element+" = function(){};");
				writer.append("\n");
				writer.append("\n");
				
			}
			
			for (DocMethod method : clazz.methods){
				writer.append("\n");
				writer.append("\n");
				writer.append(parseJV(method.javaDoc));
				writer.append("this."+method.element+" = function(");
				for (DocField f : method.params){
					writer.append(f.element);
					writer.append(",");
				}
				
				writer.append("){}");
				writer.append("\n");
				
			}
			writer.append("}\n\n");
		}
		//writer.append("}");
		writer.close();
		
	}
	
	private static String parseJV(final String jv){
		return "/**"+jv.replaceAll("\n","\n")+"**/\n".replaceAll("\t", "");
	}
	
	private static String convertStreamToString(java.io.InputStream is) {
	    try {
	        return new java.util.Scanner(is).useDelimiter("\\A").next();
	    } catch (java.util.NoSuchElementException e) {
	        return "";
	    }
	}
	
}
