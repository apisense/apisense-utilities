APISENSE utilities
==================


## pysense module

Python helper which can be used to call apisense hive service

#### Command line options
```
  __   ____  __  ____  ____  __ _  ____  ____ 
 / _\ (  _ \(  )/ ___)(  __)(  ( \/ ___)(  __)
/    \ ) __/ )( \___ \ ) _) /    /\___ \ ) _) 
\_/\_/(__)  (__)(____/(____)\_)__)(____/(____)

copyright 2014
-----------------------------------------------------------------------

usage: pysense [-h] [--set_connect SET_CONNECT SET_CONNECT SET_CONNECT]
               [--connect] [--disconnect] [--host] [--set_gcm]
               [--create_organisation] [--users]

APISENSE Hive

optional arguments:
  -h, --help            show this help message and exit
  --set_connect SET_CONNECT SET_CONNECT SET_CONNECT
                        Connect to hive server (hostname, username, password)
  --connect             Connect to hive to server
  --disconnect          Disconnect to hive server
  --host                Get connected hive node url
  --set_gcm             (Admin) Set Google Cloud Messaging API Key 
                        parameters : gcmKey 
  --create_organisation
                        (Admin) Create new organisation account
  --users               (Admin) Get all connected users


```


#### Basic usage

- **Connection** : Create a new connection with hive server
  - `python pysense --connect`
- **Create new organisation** : Create new honeycomb account to hive server
  - `python pysense --create_organisation`
- **setup gcm api key** : Setup Google Cloud Messaging API KEY. More information to get a new key is available [here](http://developer.android.com/google/gcm/gs.html#create-proj)
  - `python pysense --set_gcm gcmApiKey`



## Deployment Module

Description of `pysense-deployer` which is a simple python script enable to deploy war, documentation and APK application on
a remote server.

##### global parameters 
- `-t`    set default tomcat path on remote server 
- `-ssh`  set default ssh address of remote server 
- `-html` set default html path on remote server   
