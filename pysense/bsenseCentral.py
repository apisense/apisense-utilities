import httplib
import os
import glob
import shutil
#import datetime
import time
import base64
import urllib
import urllib2
import md5
import json
import getpass
import md5

from service import *

class CentralService(Service):
  
  def __init__(self):
    Service.__init__(self)
    self.token  = None

  def _add_command(self,parser):
    parser.add_argument('--set_gcm', 
      action="store_true", 
      help="""(Admin) Set Google Cloud Messaging API Key \nparameters : gcmKey """)
    
    parser.add_argument('--create_organisation', 
      action="store_true", 
      help="""(Admin) Create new organisation account""")

    parser.add_argument('--users', 
      action="store_true", 
      help="""(Admin) Get all connected users""")

  def command(self,arguments):
    if (arguments.create_organisation): self.create_organization_interactive()
    if (arguments.set_gcm): self.set_gcm_interactive()
    if (arguments.users): self.get_users()


  def get_users(self):
    query = base64.encodestring('%s' % self.load(os.path.dirname(os.path.realpath(__file__))+'/query/users.xq'))
    #self.sendPost("/user/get/tokeself.load('query/all.xq')
    headers = { "TOKEN" : self.getToken()}
    params = {
      "query"  : query,
      "params" : json.dumps({ 'name' : 'db', 'value' : 'Users' , 'type' :"xs:string" })
    }

    requestResult = self.sendPost("/admin/query/mprocess",params,headers)
    print requestResult
    

  def disconnect(self):
    args = {"token":self.getToken()}
    requestResult =  self.sendPost("/user/update/disconnect",{},args)
    if 'success' in requestResult:
      print "disconnected from hive server"
    self.setToken(None)  
  
  def interactive_connect(self):

    index = 1
    if (len(self._hosts())):  

      print " 0) Other "

      for i in self._hosts():
        print " %s) %s " % (index,i)
        index += 1

      _select = raw_input('Select Hive Service : ')
      if (_select == '0'):
        hostname = raw_input('Hive Service URL : ')
      else: hostname = str(self._hosts()[int(_select)-1])

    else:
      hostname = raw_input('Hive Service URL : ')

    username = raw_input('Username : ')
    password = getpass.getpass()
    self.connect([hostname,username,password])
    
  def connect(self,args):
    
    hostname = args[0]
    username = args[1]
    password = md5.md5(args[2]).hexdigest()

    self.setHost(hostname)
    
    args = { "username" : username, "password" : password }
    requestResult = json.loads(self.sendPost("/user/get/token",args,{}))
    
    if 'success' in requestResult:
      token = requestResult["success"]
      self.setToken(str(token))
      self._config("username",username)
      self._config("password",password)

      hosts = self._hosts()
      if not hostname in hosts: 
        hosts.append(hostname)
        self._set_hosts(hosts)


      print "connect to host "+self.getHost()+" with username "+username  
    else:
      print "Bad connection"
      print str(requestResult)
      exit();
    

  def set_gcm_interactive(self):
    header = {"token":self.token}
    args = { "key" :raw_input('Google Cloud Message Key : ')}
    requestResult = self.sendPost("/admin/query/gcm",args,header)
    print requestResult

  def create_organization_interactive(self):
    print "Create a new orgnaization"
    orgName        = raw_input('Organization Name : ')
    orgDescript    = raw_input('Organization Description : ')
    orgEmail       = raw_input('Email : ')
    orgUsername    = raw_input('Username : ') 
    orgPassword    = getpass.getpass()
 
    result = self.create_organization(orgName,orgDescript,orgEmail,orgUsername,orgPassword)
    print result

  def create_organization(self,organization,description,email,username,password):
    args = { "organization" :organization,"description":description,"username":username,"password":password,"email":email }
    header = {"token":self.getToken()}
    return self.sendPost("/user/add/user/organization",args,header)


