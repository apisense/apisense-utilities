#!/usr/bin/python

import httplib
import os
import glob
import shutil
#import datetime
import time
import base64
import urllib
import urllib2
import md5
import json

from service import *

class HiveService(Service):
  
  def __init__(self):
    Service.__init__(self)
    self.host  = "https://antdroid.lille.inria.fr/adam"
    self.token = None

  def command(self,args):
   
    if args.process:
      self.process(args.process)
  
  def disconnect(self):
    args = {"token":self.token}
    print self.sendPost("/user-manager/update/disconnect",args)
  
  def connect(self,args):
    username = args[0]
    password = md5.md5(args[1]).hexdigest()
    
    args = { "username" : username, "password" : password }
    result = json.loads(self.sendPost("/user-manager/get/token",args))
    
    self.token = result["success"]
    print "Connection token : "+self.token
    
  def process(self,args):
    
    xpname   = args[0]
    filename = args[1]
    output   = args[2]
    
    ## run process query on server
    print "process file "+filename+" for experiment "+xpname
    args = { "token" : self.token, "filename" : filename }
    self.sendPost("/"+xpname+"/query/process/file",args)
    
    ## get process result
    result =  self.sendGetRequest("/"+xpname+"/query/get/process/file/"+filename+"/"+self.token,{})
    
    output = open(output, 'w')
    output.write(result)


