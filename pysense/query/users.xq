declare variable $db external := "";
declare option output:method 'json';

<json type="object" arrays="users" objects="user">
<users>{

for $i in db:open($db) return (
	<user>{
	$i//username,$i//email
	}</user>
)

}</users></json>