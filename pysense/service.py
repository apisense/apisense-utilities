from xml.dom.minidom import parse, parseString
from properties import Properties
import httplib
import os
import glob
import json
import shutil
#import datetime
import time
import base64
import urllib
import urllib2
import getpass
import md5

class Service:
  
  def __init__(self):
    
    self._configuration_folder     = os.getenv("HOME")+"/.apisense"
    self._configuration_properties = self._configuration_folder+"/connection.properties"
    
    if not os.path.exists(self._configuration_folder): 
      os.makedirs(self._configuration_folder)
      with file(self._configuration_properties, "w") as f:
        f.write("")
        f.close()


    self._properties = Properties()
    self._properties.load(open(self._configuration_properties))

    if not self._has_config("hosts") : self._config("hosts","[]")

    self.host = self.getHost();
    
  
  def _has_config(self,name):
    return self._properties.has_key(name)

  def _config(self,name,value = None):
    if (value == None): return self._get_config(name)
    else: self._set_config(name,value)

  def _get_config(self,name):
    return self._properties[name]

  def _set_config(self,name,value):
    self._properties[name] = value
    self._properties.store(open(self._configuration_properties,'w'))

  def _hosts(self,value = None): 
    if (value == None) : return self._get_hosts() 
    else : self._get_hosts(value)
  def _get_hosts(self):  return json.loads(self._config("hosts"))
  def _set_hosts(self,hosts):  self._config("hosts",json.dumps(hosts))

  def _store(self):
    self._properties.store(open(self._configuration_properties,'w'))


  def hasToken(self):
    return self._properties.has_key("token")

  def setToken(self,key):
    if (key == None):
      del self._properties._keyorder[self._properties._keyorder.index('token')]
    else:
      self._properties["token"] = key
    
    self._store()

  def getToken(self):
    if not self.hasToken():
      print "Apisense client is not connected"
      exit(1)
    else:
      return self._properties["token"]

  def getHost(self):
    return self._properties["host"]


  def setHost(self,host):
    self._properties["host"] = host
    self.host = host
    self._store()


  def load(self,path):
    str = ""
    for i in open(path).readlines():
      str = str + i
    return str

    
  def write(self,path,str):
    f = open(path,"w")
    f.write(str)
    f.close()

  
  def sendGetRequest(self,url,args):

   _service = self.getHost()+"/url"
   print "call "+_service

   request = urllib2.Request(_service+"?"+urllib.urlencode(args))
   #b64str =  base64.encodestring('%s:%s' % (self.user, self.user)).replace('\n', '')
   #request.add_header("Authorization", "Basic %s" % b64str)
   try:
    result = urllib2.urlopen(request)
    return result.read()
   except Exception as e:
    print "Connection failed : "+str(e)
    return e

  
  def sendPost(self,url,args, hds):
   _service = self.getHost()+url
   print "call "+_service
   
   request = urllib2.Request(_service,urllib.urlencode(args), headers= hds)
   try:
    result = urllib2.urlopen(request)
    return result.read()
   except Exception as e:
    print "Connection failed : "+str(e)
    return e
